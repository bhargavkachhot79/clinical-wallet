@extends('layouts.auth')
@section('content')
<div class="container-fluid"> 
   <div class="d-sm-flex align-items-center justify-content-between mb-4">
      <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
      <a href="{{route('student.edit-password')}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-sync fa-sm text-white-50"></i>  Change password</a> 
      <a href="#edit" data-toggle="modal" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">  Edit Profile</a> 
      <div class="modal" id="edit" tabindex="-1" role="dialog">
         <div class="modal-dialog" role="document">
            <div class="modal-content">
               <form action="{{route('student.update-profile',$id)}}" method="POST"  enctype="multipart/form-data">
                  {{ csrf_field() }}
                  <div class="modal-header">
                     <h4 class="modal-title">Update Profile</h4>
                     <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                     </button>
                  </div>
                  <div class="card-body">
                     <div class="tab-content">
                        <div class="form-group row">
                           <label for="inputName" class="col-sm-3 col-form-label">First Name</label>
                           <div class="col-sm-9">
                              <input type="text" class="form-control" id="inputName" name="first_name" value="{{ Auth::user()->first_name }}" placeholder="First name">
                           </div>
                        </div>
                        <div class="form-group row">
                           <label for="inputName" class="col-sm-3 col-form-label">Last Name</label>
                           <div class="col-sm-9">
                              <input type="text" class="form-control" id="inputName" name="last_name" placeholder="Last name" value="{{ Auth::user()->last_name }}">
                           </div>
                        </div>
                        <div class="form-group row">
                           <label for="inputName" class="col-sm-3 col-form-label">Email</label>
                           <div class="col-sm-9">
                              <input type="email" class="form-control" id="inputName" name="email" value="{{ Auth::user()->email }}" placeholder="Eamil" >
                           </div>
                        </div>
                        <div class="form-group row">
                           <label for="inputName" class="col-sm-3 col-form-label">Phone</label>
                           <div class="col-sm-9">
                              <input type="text" class="form-control" id="inputName" name="phone" value="{{ Auth::user()->phone }}" placeholder="Phone">
                           </div>
                        </div>
                        <div class="form-group row">
                           <label for="inputName" class="col-sm-3 col-form-label">Student Id</label>
                           <div class="col-sm-9">
                              <input type="text" class="form-control" id="inputName" name="student_id" value="{{ Auth::user()->student_id }}" placeholder="Student Id" disabled="disabled">
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-sm-5"> 
                              @if(Auth::user()->image == Null)
                                 <img src="http://15.206.38.116/clinicalwallet/public/profile/images/avatar.png" id="edit-profile"  >
                              @else
                                 <img id="edit-profile" src="{{ Auth::user()->image}}">
                              @endif 
                           </div>
                           <div class="col-sm-7"> 
                              <label for="inputName" class="col-form-label">Image</label> 
                              <input name="fileupload" id="fileupload" type="file" />
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="modal-footer">
                     <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Cancle</button>
                     <input type="submit" class="btn btn-primary pull-left" value="Update" />
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>
   <div class="row"> 
   </div>
   <div class="row">
      <div class="col-xl-8 col-lg-7">
         <div class="card shadow mb-4">
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
               <h6 class="m-0 font-weight-bold text-primary">Personal info</h6>
               <div class="dropdown no-arrow">
                  <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
                  </a> 
               </div>
            </div>
            <!-- Card Body -->
            <div class="card-body">
               <div class="chart-area">
                  <form class="form-horizontal">
                     <div class="card-body">
                        <div class="form-group row">
                           <label for="inputEmail3" class="col-sm-2 col-form-label">Phone</label>
                           <div class="col-sm-10">
                              <input type="text" class="form-control" disabled="disabled" id="inputEmail3" placeholder="Phone" value="{{ Auth::user()->phone }}">
                           </div>
                        </div>
                        <div class="form-group row">
                           <label for="inputPassword3" class="col-sm-2 col-form-label">Program</label>
                           <div class="col-sm-10">
                              <input type="text" class="form-control" disabled="disabled" id="inputPassword3" placeholder="Program" value="{{$program->prog['name']}} ">
                           </div>
                        </div>
                        <div class="form-group row">
                           <label for="inputPassword3" class="col-sm-2 col-form-label">School</label>
                           <div class="col-sm-10">
                              <input type="text" class="form-control" disabled="disabled" id="inputPassword3" placeholder="School" value="{{$school_name->School['name']}}">
                           </div>
                        </div>
                        <div class="form-group row">
                           <label for="inputPassword3" class="col-sm-2 col-form-label">Term</label>
                           <div class="col-sm-10">
                              <input type="text" class="form-control" disabled="disabled" id="inputPassword3" placeholder="Term" value="{{ $term->term['term_title'] }} ( {{$term->term['start_date']}} - {{$term->term['end_date']}} )">
                           </div>
                        </div>
                        <div class="form-group row">
                           <label for="inputPassword3" class="col-sm-2 col-form-label">Group</label>
                           <div class="col-sm-10">
                              <input type="text" class="form-control" disabled="disabled" id="inputPassword3" placeholder="Group" value="{{$group->group['group_name']}}">
                           </div>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
      <div class="col-xl-4 col-lg-5">
         <div class="card shadow mb-4">
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
               <h6 class="m-0 font-weight-bold text-primary">{{ Auth::user()->first_name }} {{ Auth::user()->last_name }}</h6>
               <div class="dropdown no-arrow">
                  <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
                  </a> 
               </div>
            </div>
            <div class="card-body">
               <div class="chart-pie pt-4 pb-2">
                  <div class="col-lg-12 mb-4">
                     <div class="form-group row">
                        <img class="img-profile rounded-circle" id="image-center" src="{{ Auth::user()->image }}">
                     </div>
                     <div class="form-group row">
                        <label for="inputPassword3" class="col-sm-3 col-form-label">Student Id</label>
                        <div class="col-sm-9">
                           <input type="text" class="form-control" disabled="disabled" id="inputPassword3" placeholder="Student Id" value="{{ Auth::user()->student_id }}">
                        </div>
                     </div>
                     <div class="form-group row">
                        <label for="inputPassword3" class="col-sm-3 col-form-label">Email</label>
                        <div class="col-sm-9">
                           <input type="text" class="form-control" disabled="disabled" id="inputPassword3" placeholder="Email" value="{{ Auth::user()->email }}">
                        </div>
                     </div>
                  </div>
               </div>
               <div class="mt-4 text-center small"> 
               </div>
            </div>
         </div>
      </div>
   </div> 
   <div class="col-xl-12 col-lg-6">
         <div class="card shadow mb-4">
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
               <h6 class="m-0 font-weight-bold text-primary">Payment Detail</h6>
               <div class="dropdown no-arrow">
                  <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
                  </a> 
               </div>
            </div>
            <!-- Card Body -->
            <div class="card-body">
               <div class="chart-area">
                  <form class="form-horizontal">
                     <div class="card-body">
                        <div class="form-group row">
                           <label for="inputEmail3" class="col-sm-3 col-form-label">Current Package</label>
                           <div class="col-sm-9">
                              @if($payment_details->status == 'Inactive')
                              <input type="text" class="form-control" disabled="disabled" id="inputEmail3" value="Code Redeemed">
                              @endif
                           </div>
                        </div>
                        <div class="form-group row">
                           <label for="inputPassword3" class="col-sm-3 col-form-label">Plan Expiration date</label>
                           <div class="col-sm-9">
                              <input type="text" class="form-control" disabled="disabled" id="inputPassword3"  value="{{$payment_details->expiry_date }}">
                           </div>
                        </div>
                        <div class="form-group row">
                           <label for="inputPassword3" class="col-sm-3 col-form-label">Free access code redeemed</label>
                           <div class="col-sm-9">
                              <input type="text" class="form-control" disabled="disabled" id="inputPassword3" value="{{$payment_details->code}} ({{$payment_details->access_duration}} Months)">
                           </div>
                        </div> 
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
</div>
@endsection