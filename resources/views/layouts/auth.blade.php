<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="description" content="">
      <meta name="author" content="">
      <title>Clinical Wallet</title>
      <!-- Custom fonts for this template-->
      <link href="{{ asset('user_theme/vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
      <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
      <!-- Custom styles for this template-->
      <link href="{{ asset('user_theme/css/sb-admin-2.min.css') }}" rel="stylesheet">
      <link href="{{ asset('css\theme.css') }}" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
      <link rel="stylesheet" href="{{ asset('user_theme/plugins/fontawesome-free/css/all.min.css') }}">
      <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
      <link rel="stylesheet" href="{{ asset('user_theme/plugins/daterangepicker/daterangepicker.css') }}">
      <link rel="stylesheet" href="{{ asset('user_theme/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
      <link rel="stylesheet" href="{{ asset('user_theme/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css') }}">
      <link rel="stylesheet" href="{{ asset('user_theme/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') }}">
      <link rel="stylesheet" href="{{ asset('user_theme/plugins/select2/css/select2.min.css') }}">
      <link rel="stylesheet" href="{{ asset('user_theme/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
      <link rel="stylesheet" href="{{ asset('user_theme/plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css') }}">
      <!-- <link rel="stylesheet" href="{{ asset('user_theme/dist/css/adminlte.min.css') }}"> 
         <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">  -->
   </head>
   <body id="page-top">
      <!-- Page Wrapper -->
      <div id="wrapper">
         <!-- Sidebar -->
         @include('layouts.sidebar') 
         <!-- End of Sidebar -->
         <!-- Content Wrapper -->
         <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">
               <!-- Topbar Navbar -->
               @include('layouts.header')
               <!-- End of Topbar -->
               <!-- Begin Page Content -->
               @yield('content')
               <!-- /.container-fluid -->
            </div>
            <!-- End of Main Content -->
            <!-- Footer -->
            <footer class="sticky-footer bg-white">
               <div class="container my-auto">
                  <div class="copyright text-center my-auto">
                     <span>Copyright &copy; <b>Clinical Wallet</b> 2020</span>
                  </div>
               </div>
            </footer>
            <!-- End of Footer -->
         </div>
         <!-- End of Content Wrapper -->
      </div>
      <!-- End of Page Wrapper -->
      <!-- Scroll to Top Button-->
      <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
      </a>
      <!-- Logout Modal-->
      <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
         <div class="modal-dialog" role="document">
            <div class="modal-content">
               <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                  <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                  </button>
               </div>
               <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
               <div class="modal-footer">
                  <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                  <a class="btn btn-primary" href="login.html">Logout</a>
               </div>
            </div>
         </div>
      </div>
      <!-- Bootstrap core JavaScript-->
      <script src="{{ asset('user_theme/vendor/jquery/jquery.min.js') }}"></script>
      <script src="{{ asset('user_theme/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
      <!-- Core plugin JavaScript-->
      <script src="{{ asset('user_theme/vendor/jquery-easing/jquery.easing.min.js') }}"></script>
      <!-- Custom scripts for all pages-->
      <script src="{{ asset('user_theme/js/sb-admin-2.min.js') }}"></script>
      <!-- Page level plugins -->
      <script src="{{ asset('user_theme/vendor/chart.js/Chart.min.js') }}"></script>
      <!-- Page level custom scripts -->
      <script src="{{ asset('user_theme/js/demo/chart-area-demo.js') }}"></script>
      <script src="{{ asset('user_theme/js/demo/chart-pie-demo.js') }}"></script>
      <!-- <script src="{{ asset('user_theme/plugins/jquery/jquery.min.js') }}"></script>  -->
      <!-- <script src="{{ asset('user_theme/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script> -->
      <script src="{{ asset('user_theme/plugins/select2/js/select2.full.min.js') }}"></script>
      <!-- <script src="{{ asset('user_theme/plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js') }}"></script> -->
      <script src="{{ asset('user_theme/plugins/moment/moment.min.js') }}"></script>
      <script src="{{ asset('user_theme/plugins/inputmask/min/jquery.inputmask.bundle.min.js') }}"></script>
      <script src="{{ asset('user_theme/plugins/daterangepicker/daterangepicker.js') }}"></script>
      <!-- <script src="{{ asset('user_theme/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js') }}"></script>
         <script src="{{ asset('user_theme/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script> -->
      <!-- <script src="{{ asset('user_theme/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}"></script>
         <script src="{{ asset('user_theme/dist/js/adminlte.min.js') }}"></script> -->
      <!-- <script src="{{ asset('user_theme/dist/js/demo.js') }}"></script> -->
      <script>
         var currentTab = 0; // Current tab is set to be the first tab (0)
         showTab(currentTab); // Display the current tab
         
         function showTab(n) {
             // This function will display the specified tab of the form...
             var x = document.getElementsByClassName("tab");
             x[n].style.display = "block";
             //... and fix the Previous/Next buttons:
         
             if (n == 0) {
                 document.getElementById("prevBtn").style.display = "none";
             } else {
                 document.getElementById("prevBtn").style.display = "inline";
             }
             if (n == 10) {
                 document.getElementById("draft").style.display = "inline";
             } else {
                 document.getElementById("draft").style.display = "none";
             }
             if (n == (x.length - 1)) {
                 document.getElementById("nextBtn").innerHTML = "Submit";
             } else {
                 document.getElementById("nextBtn").innerHTML = "Save and Continue";
             }
             //... and run a function that will display the correct step indicator:
             fixStepIndicator(n)
         }
         
         function nextPrev(n) {
             // This function will figure out which tab to display
             var x = document.getElementsByClassName("tab");
             // Exit the function if any field in the current tab is invalid:
             if (n == 1 && !validateForm()) return false;
             // Hide the current tab:
             x[currentTab].style.display = "none";
             // Increase or decrease the current tab by 1:
             currentTab = currentTab + n;
             // if you have reached the end of the form...
             if (currentTab >= x.length) {
                 // ... the form gets submitted:
                 document.getElementById("regForm").submit();
                 return false;
             }
             // Otherwise, display the correct tab:
             showTab(currentTab);
         }
         
         function validateForm() {
             // This function deals with validation of the form fields
             var x, y, i, valid = true;
             x = document.getElementsByClassName("tab");
             y = x[currentTab].getElementsByTagName("input");
             console.log(y);
             // A loop that checks every input field in the current tab:
             for (i = 0; i < y.length; i++) {
                 // If a field is empty...
                 if (y[i].value == "") {
                     // add an "invalid" class to the field:
                     y[i].className += " invalid";
                     // and set the current valid status to false
                     valid = false;
                 }
             }
             // If the valid status is true, mark the step as finished and valid:
             if (valid) {
                 document.getElementsByClassName("step")[currentTab].className += " finish";
             }
             return valid; // return the valid status
         }
         
         function fixStepIndicator(n) {
             // This function removes the "active" class of all steps...
             var i, x = document.getElementsByClassName("step");
             for (i = 0; i < x.length; i++) {
                 x[i].className = x[i].className.replace(" active", "");
             }
             //... and adds the "active" class on the current step:
             x[n].className += " active";
         }
      </script>
      <script>
         $(function() {
             //Initialize Select2 Elements
             $('.select2').select2()
         
             //Initialize Select2 Elements
             $('.select2bs4').select2({
                 theme: 'bootstrap4'
             })
         
             //Datemask dd/mm/yyyy
             $('#datemask').inputmask('dd/mm/yyyy', {
                     'placeholder': 'dd/mm/yyyy'
                 })
                 //Datemask2 mm/dd/yyyy
             $('#datemask2').inputmask('mm/dd/yyyy', {
                     'placeholder': 'mm/dd/yyyy'
                 })
                 //Money Euro
             $('[data-mask]').inputmask()
         
             //Date range picker
             $('#reservation').daterangepicker()
                 //Date range picker with time picker
             $('#reservationtime').daterangepicker({
                     timePicker: true,
                     timePickerIncrement: 30,
                     locale: {
                         format: 'MM/DD/YYYY hh:mm A'
                     }
                 })
                 //Date range as a button
             $('#daterange-btn').daterangepicker({
                     ranges: {
                         'Today': [moment(), moment()],
                         'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                         'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                         'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                         'This Month': [moment().startOf('month'), moment().endOf('month')],
                         'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                     },
                     startDate: moment().subtract(29, 'days'),
                     endDate: moment()
                 },
                 function(start, end) {
                     $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
                 }
             )
         
             //Timepicker
             $('#timepicker').datetimepicker({
                 format: 'LT'
             })
         
             //Bootstrap Duallistbox
             $('.duallistbox').bootstrapDualListbox()
         
             //Colorpicker
             $('.my-colorpicker1').colorpicker()
                 //color picker with addon
             $('.my-colorpicker2').colorpicker()
         
             $('.my-colorpicker2').on('colorpickerChange', function(event) {
                 $('.my-colorpicker2 .fa-square').css('color', event.color.toString());
             });
         
             $("input[data-bootstrap-switch]").each(function() {
                 $(this).bootstrapSwitch('state', $(this).prop('checked'));
             });
         
         })
      </script>
   </body>
</html>