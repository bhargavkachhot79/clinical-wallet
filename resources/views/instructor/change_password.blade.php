@extends('instructor.layouts.auth')
@section('content')   
<div id="content-wrapper" class="d-flex flex-column">
   <!-- Main Content -->
   <div id="content">
      <!-- Begin Page Content -->
      <div class="container-fluid">
         <!-- Page Heading -->
         <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Change Password</h1>
         </div>
         <div class="card card-default">
            <div class="card-header">
               <h3 class="card-title">Change Password</h3>
            </div>
            <!-- /.card-header -->
            <form action="{{ route('instructor.update-password',$id) }}" method="POST" class="form-horizontal">
                     {{ csrf_field() }}
            <div class="card-body">
               <div class="row">
                  <div class="col-md-5">
                     <div class="form-group">
                        <label>New Password</label>
                        <input type="password" class="form-control" name="password" placeholder="Please Enter New Password ...">
                     </div>
                     <div class="form-group">
                        <label>Confirm Password</label>
                        <input type="password" class="form-control" name="confirm_password" placeholder="Please Enter Confirm Password ...">
                     </div>
                  </div>
               </div>
               <!-- <p>Password should contain at least 1 Alphabet, 1 Number and 1 Special Character</p> -->
            </div>
            <div class="card-footer">  
               <input type="submit" name="submit" value="Change Password" class="btn btn-primary">
            </div>
         </form>
         </div>
      </div>
   </div>
   <!-- End of Main Content -->
</div>
'
<!-- End of Content Wrapper -->
@endsection