@extends('instructor.layouts.auth')
@section('content')   
<div id="content-wrapper" class="d-flex flex-column">
   <!-- Main Content -->
   <div id="content">
      <!-- Begin Page Content -->
      <div class="container-fluid">
         <!-- Page Heading -->
         <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Home </h1>
         </div>
         <!-- Content Row -->
         <div class="row">
            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-4 col-md-6 mb-4">
               <div class="card border-left-primary shadow h-100 py-2">
                  <div class="card-body">
                     <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                           <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">  Activity Log </div>
                           <div class="h5 mb-0 font-weight-bold text-gray-800">$40,000</div>
                        </div>
                        <div class="col-auto">
                           <i class="fas fa fa-graduation-cap fa-2x text-gray-300"></i> 
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-4 col-md-6 mb-4">
               <div class="card border-left-success shadow h-100 py-2">
                  <div class="card-body">
                     <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                           <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Total students</div>
                           <div class="h5 mb-0 font-weight-bold text-gray-800">$215,000</div>
                        </div>
                        <div class="col-auto">
                           <i class="fas fa-user-graduate fa-2x text-gray-300"></i>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-4 col-md-6 mb-4">
               <div class="card border-left-info shadow h-100 py-2">
                  <div class="card-body">
                     <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                           <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Total Clinical Absence</div>
                           <div class="row no-gutters align-items-center">
                              <div class="col-auto">
                                 <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">50%</div>
                              </div>
                              <div class="col">
                                 <div class="progress progress-sm mr-2">
                                    <div class="progress-bar bg-info" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-auto">
                           <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="card shadow mb-4">
            <div class="card-header py-3">
               <h6 class="m-0 font-weight-bold text-primary">Filter: </h6>
            </div>
            <div class="card-body">
               <div class="tab">
                  <div class="row">
                     <div class="col-md-3">
                        <div class="form-group">
                           <label>Hospital</label>
                           <select class="form-control select2" style="width: 100%;">
                              <option selected="selected">Alabama</option>
                              <option>Alaska</option>
                              <option>California</option>
                              <option>Delaware</option>
                              <option>Tennessee</option>
                              <option>Texas</option>
                              <option>Washington</option>
                           </select>
                        </div>
                     </div>
                     <div class="col-md-3">
                        <div class="form-group">
                           <label>Sessions</label>
                           <select class="form-control select2" style="width: 100%;">
                              <option selected="selected">My Session</option>
                              <option>Group session</option>
                           </select>
                        </div>
                     </div>
                     <div class="col-md-3">
                        <div class="form-group">
                           <label>Date range:</label> 
                           <div class="input-group">
                              <div class="input-group-prepend">
                                 <span class="input-group-text">
                                 <i class="far fa-calendar-alt"></i>
                                 </span>
                              </div>
                              <input type="text" class="form-control float-right" id="reservation">
                           </div>
                        </div>
                     </div>
                     <div class="col-md-3">
                        <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"> Submit</a>
                        <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"> Reset</a>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-sm-3">
            </div>
         </div>
         <div class="card shadow mb-4">
            <div class="card-header py-3">
               <h6 class="m-0 font-weight-bold text-primary">Activity Log</h6>
            </div>
            <div class="card-body">
               <div class="table-responsive">
                  <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                     <thead>
                        <tr>
                           <th>Date & Duration</th>
                           <th>Student Name</th>
                           <th>Location</th>
                           <th>Start date Time</th>
                           <th>End Time Edited by</th>
                           <th>Verified By</th>
                           <th>Digital Signature</th>
                           <th>Action</th>
                        </tr>
                     </thead>
                     <tfoot>
                        <tr>
                           <th>Date & Duration</th>
                           <th>Student Name</th>
                           <th>Location</th>
                           <th>Start date Time</th>
                           <th>End Time Edited by</th>
                           <th>Verified By</th>
                           <th>Digital Signature</th>
                           <th>Action</th>
                        </tr>
                     </tfoot>
                     <tbody>
                        <tr>
                           <td>Tiger Nixon</td>
                           <td>System Architect</td>
                           <td>Edinburgh</td>
                           <td>61</td>
                           <td>2011/04/25</td>
                           <td>$320,800</td>
                           <td>2011/04/25</td>
                           <td>$320,800</td>
                        </tr>
                        <tr>
                           <td>Garrett Winters</td>
                           <td>Accountant</td>
                           <td>Tokyo</td>
                           <td>63</td>
                           <td>2011/07/25</td>
                           <td>$170,750</td>
                           <td>2011/04/25</td>
                           <td>$320,800</td>
                        </tr>
                        <tr>
                           <td>Ashton Cox</td>
                           <td>Junior Technical Author</td>
                           <td>San Francisco</td>
                           <td>66</td>
                           <td>2009/01/12</td>
                           <td>$86,000</td>
                           <td>2011/04/25</td>
                           <td>$320,800</td>
                        </tr>
                        <tr>
                           <td>Cedric Kelly</td>
                           <td>Senior Javascript Developer</td>
                           <td>Edinburgh</td>
                           <td>22</td>
                           <td>2012/03/29</td>
                           <td>$433,060</td>
                           <td>2011/04/25</td>
                           <td>$320,800</td>
                        </tr>
                        <tr>
                           <td>Airi Satou</td>
                           <td>Accountant</td>
                           <td>Tokyo</td>
                           <td>33</td>
                           <td>2008/11/28</td>
                           <td>$162,700</td>
                           <td>2011/04/25</td>
                           <td>$320,800</td>
                        </tr>
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
      <!-- /.container-fluid -->
   </div>
   <!-- End of Main Content -->
</div>
<!-- End of Content Wrapper -->
@endsection