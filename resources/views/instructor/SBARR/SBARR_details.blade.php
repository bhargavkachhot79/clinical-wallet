@extends('instructor.layouts.auth')
@section('content')
<section class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-md-1">
         </div>
         <div class="col-md-10">
            <div class="card card-primary">
               <div class="card-header">
                <div class="row">
                  <div class="col-sm-10">
                     <label>Your lab/classroom</label>
                     <p>Ashley Parks</p>
                     <p>Jun 12, 2017</p>
                  </div>
                  <div class="col-sm-2">
                   <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Export to PDF</a>
                   </div>
                 </div>
               </div>
               <div class="card-body">
                  <div class="col-sm-6">
                     <h4>Situation</h4>
                     <p>sit</p>
                  </div>
                  <hr>
                  <div class="col-sm-6">
                     <h4>Background</h4>
                     <p>back</p>
                  </div>
                  <hr>
                  <div class="col-sm-6">
                     <h4>Assessment</h4>
                     <p>Assess</p>
                  </div>
                  <hr>
                  <div class="col-sm-6">
                     <h4>Recommendation</h4>
                     <p>Recommend</p>
                  </div>
                  <hr>
                  <div class="col-sm-6">
                     <h4>Response</h4>
                     <p>Response.se</p>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-md-1">
         </div>
      </div>
   </div>
</section>
@endsection