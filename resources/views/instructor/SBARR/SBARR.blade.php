@extends('instructor.layouts.auth')
@section('content')   
<div id="content-wrapper" class="d-flex flex-column">
   <!-- Main Content -->
   <div id="content">
      <!-- Begin Page Content -->
      <div class="container-fluid">
         <!-- Page Heading -->
         <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Rubric</h1>
         </div>
         <div class="card shadow mb-4">
            <div class="card-header py-3">
               <h6 class="m-0 font-weight-bold text-primary">Rubric</h6>
            </div>
            <div class="card-body">
               <div class="table-responsive">
                  <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                     <thead>
                        <tr>
                           <th>
                              <div class="row">
                                 <div class="col-sm-9">
                                    <h5>Summative Clinical Evaluation Tool</h5>
                                    <p>School</p>
                                 </div>
                                 <div class="col-sm-2"> 
                                    <p>Apr 03, 2018</p>
                                 </div>
                                 <div class="col-sm-1">
                                    <a href="{{route('instructor.SBARR.details')}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-arrow-right"></i></a>
                                 </div>
                              </div>
                           </th>
                        </tr>
                        <tr>
                           <th>
                              <div class="row">
                                 <div class="col-sm-9">
                                    <h5>Summative Clinical Evaluation Tool</h5>
                                    <p>School</p>
                                 </div>
                                 <div class="col-sm-2"> 
                                    <p>Apr 03, 2018</p>
                                 </div>
                                 <div class="col-sm-1">
                                    <a href="{{route('instructor.SBARR.details')}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-arrow-right"></i></a>
                                 </div>
                              </div>
                           </th>
                        </tr>
                        <tr>
                           <th>
                              <div class="row">
                                 <div class="col-sm-9">
                                    <h5>Summative Clinical Evaluation Tool</h5>
                                    <p>School</p>
                                 </div>
                                 <div class="col-sm-2"> 
                                    <p>Apr 03, 2018</p>
                                 </div>
                                 <div class="col-sm-1">
                                    <a href="{{route('instructor.SBARR.details')}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-arrow-right"></i></a>
                                 </div>
                              </div>
                           </th>
                        </tr>
                     </thead>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- End of Main Content -->
</div>
<!-- End of Content Wrapper -->
@endsection