<nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
   <!-- Sidebar Toggle (Topbar) -->
   <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
   <i class="fa fa-bars"></i>
   </button>
   <!-- Topbar Search -->
   <form class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
      <div class="input-group">
         <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
         <div class="input-group-append">
            <button class="btn btn-primary" type="button">
            <i class="fas fa-search fa-sm"></i>
            </button>
         </div>
      </div>
   </form>
   <div class="col-sm-5">
      <a href="#" class="btn btn-info btn-icon-split">
      <span class="icon text-white-50">
      <i class="fas fa-school"></i> School:
      </span>
      <span class="text"> {{ Auth::user()->school }}</span>
      </a>
   </div>
   <div class="col-sm-3">
      <a href="#" class="btn btn-info btn-icon-split">
      <span class="icon text-white-50">
      <i class="fa fa-object-ungroup" aria-hidden="true"></i> Program:
      </span>
      <span class="text"> {{ Auth::user()->program }}</span>
      </a>
   </div>
   <ul class="navbar-nav ml-auto">
      <!-- Nav Item - Search Dropdown (Visible Only XS) -->
      <li class="nav-item dropdown no-arrow d-sm-none">
         <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
         <i class="fas fa-search fa-fw"></i>
         </a>
         <!-- Dropdown - Messages -->
         <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
            <form class="form-inline mr-auto w-100 navbar-search">
               <div class="input-group">
                  <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                  <div class="input-group-append">
                     <button class="btn btn-primary" type="button">
                     <i class="fas fa-search fa-sm"></i>
                     </button>
                  </div>
               </div>
            </form>
         </div>
      </li>
      <!-- Nav Item - User Information -->
      <li class="nav-item dropdown no-arrow">
         <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
         <span class="mr-2 d-none d-lg-inline text-gray-600 small">{{ Auth::user()->first_name }} {{ Auth::user()->last_name }}</span>
         <img class="img-profile rounded-circle" src="{{ Auth::user()->image }}">
         </a>
         <!-- Dropdown - User Information -->
         <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
            <a class="dropdown-item" href="{{route('instructor.profile')}}">
            <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
            My Account
            </a>
            <a class="dropdown-item" href="{{route('instructor.change.password',Auth::user()->id)}}">
            <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
             Change Password
            </a> 
            <a class="dropdown-item" href="#">
            <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
              Learning Center
            </a>
            <div class="dropdown-divider"></div> 
            <a href="{{ route('logout') }}" class="dropdown-item"    onclick="event.preventDefault();
         document.getElementById('logout-form').submit();">
         <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
         <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
         </form> 
         Logout                  
       
            </a>
         </div>
      </li>
   </ul>
</nav>