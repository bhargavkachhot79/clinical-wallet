<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
   <!-- Sidebar - Brand -->
   <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
      <div class="sidebar-brand-icon rotate-n-15">
         <i class="fas fa-laugh-wink"></i>
      </div>
      <div class="sidebar-brand-text mx-3">Clinical Wallet<sup>2</sup></div>
   </a>
   <!-- Divider -->
   <hr class="sidebar-divider my-0">
   <!-- Nav Item - Dashboard -->
   <li class="nav-item active">
      <a class="nav-link" href="{{route('instructor.home')}}">
      <i class="fas fa-fw fa-tachometer-alt"></i>
      <span>Home</span></a>
   </li>
   <!-- Divider -->
   <!-- <hr class="sidebar-divider"> -->
   <!-- Heading -->
   <!-- <div class="sidebar-heading">
      Interface
      </div> -->
   <!-- Nav Item - Pages Collapse Menu -->
   <!-- <li class="nav-item">
      <a class="nav-link collapsed" href="{{route('learningcontractform')}}" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
        <i class="fas fa-fw fa-cog"></i>  
        <span>Student Learning Contract</span>
      </a>
      <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
          <h6 class="collapse-header">Custom Components:</h6>
          <a class="collapse-item" href="buttons.html">Buttons</a>
          <a class="collapse-item" href="cards.html">Cards</a>
        </div>
      </div>
      </li> -->
   <li class="nav-item">
      <a class="nav-link" href="{{route('competency.list')}}">
      <i class="fas fa-fw fa-chart-area"></i>
      <span>Competency List</span>
      </a>
   </li> 
   <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#Evaluation" aria-expanded="true" aria-controls="collapseUtilities">
          <i class="fas fa-fw fa-wrench"></i>
          <span>Evaluation</span>
        </a>
        <div id="Evaluation" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded"> 
            <a class="collapse-item" href="{{route('clinical_site_evaluation')}}">Clinical Site Evaluation</a>
            <a class="collapse-item" href="{{route('assigned.clinical.remediation')}}">Assigned Clinical Remediation</a>
            <a class="collapse-item" href="{{route('journal')}}">Journal</a>
            <a class="collapse-item" href="{{route('summative_evaluation')}}">Summative Evalution</a>
            <a class="collapse-item" href="{{route('GBC_form')}}">GBC Form</a>
            <a class="collapse-item" href="{{route('Learning.Contract.Form')}}">Student Learning Contract</a>
          </div>
        </div>
      </li>
   <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#Reports" aria-expanded="true" aria-controls="collapseUtilities">
          <i class="fas fa-fw fa-wrench"></i>
          <span>Reports</span>
        </a>
        <div id="Reports" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded"> 
            <a class="collapse-item" href="{{route('instructor.student_activity_log')}}">Reports</a>
            <a class="collapse-item" href="{{route('instructor.attendence')}}">Attendence</a> 
          </div>
        </div>
      </li>
   <li class="nav-item">
      <a class="nav-link" href="{{route('instructor.SBARR')}}">
      <i class="fas fa-fw fa-chart-area"></i>
      <span>SBARR</span>
      </a>
   </li>
   <li class="nav-item">
      <a class="nav-link" href="{{route('instructor.history')}}">
      <i class="fas fa-fw fa-chart-area"></i>
      <span>History</span>
      </a>
   </li>
   <li class="nav-item">
      <a class="nav-link" href="{{route('instructor.students')}}">
      <i class="fas fa-fw fa-chart-area"></i>
      <span>Students</span>
      </a>
   </li>
   <li class="nav-item">
      <a class="nav-link" href="{{route('instructor.rubric')}}">
      <i class="fas fa-fw fa-chart-area"></i>
      <span>Rubric</span>
      </a>
    </li>
   
   <!-- Divider -->
   <hr class="sidebar-divider d-none d-md-block">
   <!-- Sidebar Toggler (Sidebar) -->
   <div class="text-center d-none d-md-inline">
      <button class="rounded-circle border-0" id="sidebarToggle"></button>
   </div>
</ul>