@extends('instructor.layouts.auth')
@section('content')   
<div id="content-wrapper" class="d-flex flex-column">
   <!-- Main Content -->
   <div id="content">
      <!-- Begin Page Content -->
      <div class="container-fluid">
         <!-- Page Heading -->
         <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Evalution</h1>
         </div>
         <div class="card shadow mb-4">
            <div class="card-header py-3">
               <h6 class="m-0 font-weight-bold text-primary">Assigned Clinical Remediation</h6>
               <div class="row">
                  <div class="col-md-3">
                     <div class="form-group">
                        <select class="form-control select2" style="width: 100%;">
                           <option selected="selected">All students</option>
                           <option>Carrie  Rouchka</option>
                           <option>Henry Hayes</option>
                        </select>
                     </div>
                  </div>
                  <div class="col-md-3">
                     <div class="form-group">
                        <select class="form-control select2" style="width: 100%;">
                           <option selected="selected">Export to PDF</option>
                           <option>Export to Excel</option>
                        </select>
                     </div>
                  </div>
                  <div class="col-md-3">
                     <a href="#delete" data-toggle="modal" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"> Assign Clinical Remediation</a> 
                     <div class="modal" id="delete" tabindex="-1" role="dialog">
                        <div class="modal-dialog" role="document">
                           <div class="modal-content">
                              <form action="" method="POST">
                                 {{ csrf_field() }}
                                 <div class="modal-header">
                                    <h4 class="modal-title">Assign Clinical Remediation</h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                 </div>
                                 <div class="modal-body">
                                    <div class="form-group">
                                       <select class="form-control select2" style="width: 100%;">
                                          <option selected="selected">Select student</option>
                                          <option>Carrie  Rouchka</option>
                                          <option>Henry Hayes</option>
                                       </select>
                                    </div>
                                    <div class="row">
                                       <div class="col-sm-6">
                                          <!-- checkbox -->
                                          <div class="form-group">
                                             <div class="form-check">
                                                <input class="form-check-input" type="checkbox">
                                                <label class="form-check-label"> Occurrence/Incident</label>
                                             </div>
                                             <div class="form-check">
                                                <input class="form-check-input" type="checkbox" checked>
                                                <label class="form-check-label">Clinical Warning</label>
                                             </div>
                                             <div class="form-check">
                                                <input class="form-check-input" type="checkbox">
                                                <label class="form-check-label"> Clinical Probation</label>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="col-sm-6">
                                          <!-- checkbox -->
                                          <div class="form-group">
                                             <div class="form-check">
                                                <input class="form-check-input" type="checkbox">
                                                <label class="form-check-label">Missing Documents</label>
                                             </div>
                                             <div class="form-check">
                                                <input class="form-check-input" type="checkbox" >
                                                <label class="form-check-label"> Alternate Assignments</label>
                                             </div>
                                             <div class="form-check">
                                                <input class="form-check-input" type="checkbox">
                                                <label class="form-check-label">Clinical Demerit</label>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div class="col-sm-12">
                                          <!-- textarea -->
                                          <div class="form-group"> 
                                             <textarea class="form-control" rows="3" placeholder="Improvement ..."></textarea>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="modal-footer">
                                    <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Cancle</button>
                                    <input type="submit" class="btn btn-primary pull-left" value="Submit" />
                                  </div>
                              </form> 
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="card-body">
               <div class="table-responsive">
                  <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                     <thead>
                        <tr>
                           <th>Student</th>
                           <th>Category</th>
                           <th>Faculty Comments</th>
                           <th>Student Acknowledge</th>
                           <th>Date</th>
                           <th>Action</th>
                        </tr>
                     </thead>
                     <tfoot>
                        <tr>
                           <th>Student</th>
                           <th>Category</th>
                           <th>Faculty Comments</th>
                           <th>Student Acknowledge</th>
                           <th>Date</th>
                           <th>Action</th>
                        </tr>
                     </tfoot>
                     <tbody>
                        <tr>
                           <td>Tiger Nixon</td>
                           <td>System Architect</td>
                           <td>Edinburgh</td>
                           <td>61</td>
                           <td>Edinburgh</td>
                           <td>61</td>
                        </tr>
                        <tr>
                           <td>Garrett Winters</td>
                           <td>Accountant</td>
                           <td>Tokyo</td>
                           <td>63</td>
                           <td>Edinburgh</td>
                           <td>61</td>
                        </tr>
                        <tr>
                           <td>Ashton Cox</td>
                           <td>Junior Technical Author</td>
                           <td>San Francisco</td>
                           <td>66</td>
                           <td>San Francisco</td>
                           <td>66</td>
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- End of Main Content -->
</div>
<!-- End of Content Wrapper -->
@endsection