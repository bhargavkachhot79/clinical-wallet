@extends('instructor.layouts.auth')
@section('content')   
<div id="content-wrapper" class="d-flex flex-column">
   <!-- Main Content -->
   <div id="content">
      <!-- Begin Page Content -->
      <div class="container-fluid">
         <!-- Page Heading -->
         <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Evalution</h1>
         </div> 
         <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">GBC Form</h6>
              <div class="row">
                  <div class="col-md-4">
                     
                  </div>
                  <div class="col-md-4">
                      
                  </div>
                  <div class="col-md-4">
                     <a href="{{route('add.GBC_location')}}"  class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">Create new GBC form</a> 
                  </div>
               </div>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>#Id</th>
                      <th>Term</th>
                      <th>Clinical Site & Unit</th>
                      <th>Letter Of Concern</th> 
                      <th>Status</th>
                      <th>Action</th> 
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>#Id</th>
                      <th>Term</th>
                      <th>Clinical Site & Unit</th>
                      <th>Letter Of Concern</th> 
                      <th>Status</th>
                      <th>Action</th> 
                    </tr>
                  </tfoot>
                  <tbody>
                    <tr>
                      <td>Tiger Nixon</td>
                      <td>System Architect</td>
                      <td>Edinburgh</td>
                      <td>61</td> 
                      <td>Edinburgh</td>
                      <td><a href="#" class="btn btn-info btn-icon-split">
                    <span class="icon text-white-50">
                      <i class="fas fa-info-circle"></i>
                    </span>
                    <span class="text">View Detail</span>
                  </a></td> 
                    </tr>
                    <tr>
                      <td>Garrett Winters</td>
                      <td>Accountant</td>
                      <td>Tokyo</td>
                      <td>63</td> 
                      <td>Edinburgh</td>
                      <td><a href="#" class="btn btn-info btn-icon-split">
                    <span class="icon text-white-50">
                      <i class="fas fa-info-circle"></i>
                    </span>
                    <span class="text">View Detail</span>
                  </a></td> 
                    </tr>
                    <tr>
                      <td>Ashton Cox</td>
                      <td>Junior Technical Author</td>
                      <td>San Francisco</td>
                      <td>66</td> 
                      <td>Edinburgh</td>
                      <td><a href="#" class="btn btn-info btn-icon-split">
                    <span class="icon text-white-50">
                      <i class="fas fa-info-circle"></i>
                    </span>
                    <span class="text">View Detail</span>
                  </a></td> 
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
   </div>
   <!-- End of Main Content -->
</div>
<!-- End of Content Wrapper -->
@endsection