@extends('instructor.layouts.auth')
@section('content')   
<div id="content-wrapper" class="d-flex flex-column">
   <!-- Main Content -->
   <div id="content">
      <!-- Begin Page Content -->
      <div class="container-fluid">
         <!-- Page Heading -->
         <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Summative Form</h1>
         </div> 
        
          <div class="card card-default">
          <div class="card-header">
            <h3 class="card-title">Add details</h3>

           
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <label>Student</label>
                  <select class="form-control select2" style="width: 100%;">
                    <option selected="selected">Alabama</option>
                    <option>Alaska</option>
                    <option>California</option>
                    <option>Delaware</option>
                    <option>Tennessee</option>
                    <option>Texas</option>
                    <option>Washington</option>
                  </select>
                </div>
                <!-- /.form-group -->
                <div class="form-group">
                  <label>Location</label>
                  <select class="form-control select2" style="width: 100%;">
                    <option selected="selected">Select Location</option>
                    <option>Alaska</option>
                    <option>California</option>
                    <option>Delaware</option>
                    <option>Tennessee</option>
                    <option>Texas</option>
                    <option>Washington</option>
                  </select>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
              <div class="col-md-4">
                <div class="form-group">
                        <label>Grade</label>
                        <input type="text" class="form-control" placeholder="Grade">
                      </div>
                <!-- /.form-group -->
                <div class="form-group">
                  <label>Term</label>
                  <select class="form-control select2" style="width: 100%;">
                    <option selected="selected">Mid Term</option>
                    <option>Alaska</option>
                    <option disabled="disabled">California (disabled)</option>
                    <option>Delaware</option>
                    <option>Tennessee</option>
                    <option>Texas</option>
                    <option>Washington</option>
                  </select>
                </div>
                <!-- /.form-group -->
              </div>
               <div class="col-md-4">
                <div class="form-group">
                        <label>Date</label>
                        <input type="text" value="<?php echo date('Y-m-d'); ?>" class="form-control" placeholder="Date"> 
                      </div>
               
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row --> 
            <div class="row">
              <div class="col-12 col-sm-4">
                <div class="form-group">
                  <label>Objective</label>
                  <select class="form-control select2 select2-danger" data-dropdown-css-class="select2-danger" style="width: 100%;">
                    <option selected="selected">Alabama</option>
                    <option>Alaska</option>
                    <option>California</option>
                    <option>Delaware</option>
                    <option>Tennessee</option>
                    <option>Texas</option>
                    <option>Washington</option>
                  </select>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
              <div class="col-12 col-sm-4">
                <div class="form-group">
                  <label>Rating</label>
                   <select class="form-control select2 select2-danger" data-dropdown-css-class="select2-danger" style="width: 100%;">
                    <option selected="selected">Alabama</option>
                    <option>Alaska</option>
                    <option>California</option>
                    <option>Delaware</option>
                    <option>Tennessee</option>
                    <option>Texas</option>
                    <option>Washington</option>
                  </select>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
            </div>
            <div class="row">
                    <div class="col-sm-12">
                      <!-- textarea -->
                      <div class="form-group"> 
                        <textarea class="form-control" rows="4" placeholder="Summative Comment ..."></textarea>
                      </div>
                    </div> 
            </div>
            <!-- /.row -->
          </div>
          <div id="container">
          </div> 

          <!-- /.card-body -->
          <div class="card-footer"> 
            <a href="#" class="btn btn-primary btn-icon-split">
                    <span class="icon text-white-50">
                     <i class="fa fa-check" aria-hidden="true"></i>
                    </span>
                    <span class="text">Submit</span>
                  </a>
                  <button class="btn btn-primary btn-icon-split" type="button"> 
                    
                    <span class="icon text-white-50">
                      <i class="fa fa-plus" aria-hidden="true"></i>
                    </span>
                    <span class="text">Add more</span>
                  </button>
          </div>
        </div>
        </div>
   </div>
   <!-- End of Main Content -->
</div>'

<!-- End of Content Wrapper -->
@endsection