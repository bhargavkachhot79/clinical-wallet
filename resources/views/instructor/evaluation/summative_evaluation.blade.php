@extends('instructor.layouts.auth')
@section('content')   
<div id="content-wrapper" class="d-flex flex-column">
   <!-- Main Content -->
   <div id="content">
      <!-- Begin Page Content -->
      <div class="container-fluid">
         <!-- Page Heading -->
         <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Summative Evaluation</h1>
         </div>
         <div class="card shadow mb-4">
            <div class="card-header py-3">
               <h6 class="m-0 font-weight-bold text-primary">Assigned Clinical Remediation</h6>
               <div class="row">
                  <div class="col-md-3">
                     <div class="form-group">
                        <select class="form-control select2" style="width: 100%;">
                           <option selected="selected">All students</option>
                           <option>Carrie  Rouchka</option>
                           <option>Henry Hayes</option>
                        </select>
                     </div>
                  </div>
                  <div class="col-md-3">
                     <div class="form-group">
                        <select class="form-control select2" style="width: 100%;" disabled="disabled">
                           <option selected="selected">Export to PDF</option>
                           <option>Export to Excel</option>
                        </select>
                     </div>
                  </div>
                  <div class="col-md-3">
                     <a href="{{route('add.summative_evaluation')}}"  class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">Add Summative Evaluation</a> 
                  </div>
               </div>
            </div>
            <div class="card-body">
               <div class="table-responsive">
                  <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                     <thead>
                        <tr>
                           <th>Student</th>
                           <th>Grade</th>
                           <th>Date</th>
                           <th>Location</th>
                           <th>Term</th>
                           <th>Acknowledge</th>
                           <th>Action</th>
                        </tr>
                     </thead>
                     <tfoot>
                        <tr>
                           <th>Student</th>
                           <th>Grade</th>
                           <th>Date</th>
                           <th>Location</th>
                           <th>Term</th>
                           <th>Acknowledge</th>
                           <th>Action</th>
                        </tr>
                     </tfoot>
                     <tbody>
                        <tr>
                           <td>Tiger Nixon</td>
                           <td>System Architect</td>
                           <td>Edinburgh</td>
                           <td>61</td>
                        </tr>
                        <tr>
                           <td>Garrett Winters</td>
                           <td>Accountant</td>
                           <td>Tokyo</td>
                           <td>63</td>
                        </tr>
                        <tr>
                           <td>Ashton Cox</td>
                           <td>Junior Technical Author</td>
                           <td>San Francisco</td>
                           <td>66</td>
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- End of Main Content -->
</div>
<!-- End of Content Wrapper -->
@endsection