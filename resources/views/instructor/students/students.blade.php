@extends('instructor.layouts.auth')
@section('content')   
<div id="content-wrapper" class="d-flex flex-column">
   <!-- Main Content -->
   <div id="content">
      <!-- Begin Page Content -->
      <div class="container-fluid">
         <!-- Page Heading -->
         <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Students</h1>
         </div>
         <div class="card shadow mb-4">
            <div class="card-header py-3">
               <h6 class="m-0 font-weight-bold text-primary">Students</h6>
               <div class="row">
                  <div class="col-md-3">
                     <div class="form-group">
                        <select class="form-control select2" style="width: 100%;">
                           <option selected="selected">Ascending</option>
                           <option>Descending</option>
                        </select>
                     </div>
                  </div>
               </div>
            </div>
            @if (session('status'))
            <div class="alert alert-success" role="alert">
               {{ session('status') }}
            </div>
            @endif
            <section class="content">
               <!-- Default box -->
               <div class="card card-solid">
                  <div class="card-body pb-0">
                     <div class="row d-flex align-items-stretch">
                        @if(count($students))
                        <?php $i=1; ?>                   
                        @foreach($students as $student) 
                        <div class="col-12 col-sm-6 col-md-4 d-flex align-items-stretch">
                           <div class="card bg-light">
                              <div class="card-header text-muted border-bottom-0"> 
                              </div>
                              <div class="card-body pt-0">
                                 <div class="row">
                                    <div class="col-7">
                                       <h2 class="lead"><b>{{$student->first_name}} {{$student->last_name}}</b></h2>
                                       <p class="text-muted text-sm"><b>Student Id: </b> {{$student->student_id  }}  </p>
                                       <ul class="ml-4 mb-0 fa-ul text-muted">
                                          <li class="small"><span class="fa-li"><i class="fas fa-lg fa-building"></i></span> Email : {{$student->email}}</li>
                                       </ul>
                                    </div>
                                    <div class="col-5 text-center">
                                       @if($student->image == Null)
                                       <img src="http://15.206.38.116/clinicalwallet/public/profile/images/avatar.png" id="edit-profile"  >
                                       @else
                                       <img src="{{$student->image}}" alt="" class="img-circle img-fluid">
                                       @endif
                                    </div>
                                 </div>
                              </div>
                              <div class="card-footer">
                                 <div class="text-right"> 
                                    <a href="{{route('instructor.students.detail_profile',$student->id)}}" class="btn btn-sm btn-primary">
                                    <i class="fas fa-user"></i> View Profile
                                    </a>
                                 </div>
                              </div>
                           </div>
                        </div>
                        @endforeach
                        @else 
                        <tr>
                           <td colspan="6">No Students found</td>
                        </tr>
                        @endif
                     </div>
                  </div>
                  <!-- /.card-body -->
                  <div class="card-footer">
                     <nav aria-label="Contacts Page Navigation">
                        <ul class="pagination justify-content-center m-0">
                           <li class="page-item active"><a class="page-link" href="#">1</a></li>
                           <li class="page-item"><a class="page-link" href="#">2</a></li>
                           <li class="page-item"><a class="page-link" href="#">3</a></li>
                           <li class="page-item"><a class="page-link" href="#">4</a></li>
                           <li class="page-item"><a class="page-link" href="#">5</a></li>
                           <li class="page-item"><a class="page-link" href="#">6</a></li>
                           <li class="page-item"><a class="page-link" href="#">7</a></li>
                           <li class="page-item"><a class="page-link" href="#">8</a></li>
                        </ul>
                     </nav>
                  </div>
                  <!-- /.card-footer -->
               </div>
               <!-- /.card -->
            </section>
         </div>
      </div>
   </div>
   <!-- End of Main Content -->
</div>
<!-- End of Content Wrapper -->
@endsection