@extends('instructor.layouts.auth')
@section('content')   
<section class="content"> 
   <div class="container-fluid">
      <div class="row">
         <div class="col-md-4">
            <!-- Profile Image -->
            <div class="card card-primary card-outline">
               <div class="card-body box-profile">
                  <div class="text-center"> 
                     @if($student->image == Null)
                           <img src="http://15.206.38.116/clinicalwallet/public/profile/images/avatar.png" class="profile-user-img img-fluid img-circle" id="edit-profile"  >
                     @else
                           <img src="{{$student->image}}" alt="" class="profile-user-img img-fluid img-circle">
                     @endif   
                  </div>
                  <h3 class="profile-username text-center">{{$student->first_name}} {{$student->last_name}}</h3>
                  <p class="text-muted text-center">Software Engineer</p>
                  <ul class="list-group list-group-unbordered mb-3">
                     <li class="list-group-item">
                        <b>Email:</b><br>{{$student->email}} 
                     </li>
                  </ul>
                  <div class="row">
                     <div class="col-sm-6">
                        <a href="#edit" data-toggle="modal" class="btn btn-primary btn-block">Edit Student</a> 
                        <div class="modal" id="edit" tabindex="-1" role="dialog">
                           <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                 <form action="{{route('update.student_from_instructor_side',$student->id)}}" method="POST" enctype="multipart/form-data">
                                             {{ csrf_field() }}
                                 <div class="card-header p-2">
                                    <h6 class="m-0 font-weight-bold text-primary">Update Student</h6>
                                 </div>
                                 <div class="card-body">
                                    <div class="tab-content">
                                       <div class="form-group row">
                                          <label for="inputName" class="col-sm-3 col-form-label">First Name</label>
                                          <div class="col-sm-9">
                                             <input type="text" class="form-control" id="inputName" name="first_name" value="{{$student->first_name}}" placeholder="First Name">
                                          </div>
                                       </div>
                                       <div class="form-group row">
                                          <label for="inputName" class="col-sm-3 col-form-label">Last Name</label>
                                          <div class="col-sm-9">
                                             <input type="text" class="form-control" id="inputName" name="last_name" value="{{$student->last_name}}" placeholder="Last Name">
                                          </div>
                                       </div>
                                       <div class="form-group row">
                                          <label for="inputName" class="col-sm-3 col-form-label">Email</label>
                                          <div class="col-sm-9">
                                             <input type="email" class="form-control" value="{{$student->email}}" name="email" id="inputName" placeholder="Email" disabled="disabled">
                                          </div>
                                       </div>
                                       <div class="form-group row">
                                          <label for="inputName" class="col-sm-3 col-form-label">Phone</label>
                                          <div class="col-sm-9">
                                             <input type="text" class="form-control" id="inputName" name="phone" value="{{$student->phone}}" placeholder="Phone">
                                          </div>
                                       </div>
                                       <div class="form-group row">
                                          <label for="inputName" class="col-sm-3 col-form-label">Student Id</label>
                                          <div class="col-sm-9">
                                             <input type="text" class="form-control" id="inputName" name="student_id" value="{{$student->student_id}}" placeholder="Student Id">
                                          </div>
                                       </div>
                                       <div class="form-group row">
                                          <label for="inputName" class="col-sm-3 col-form-label">Image</label>
                                          <div class="col-sm-9">
                                             <input type="file" class="form-control" id="inputName" name="image">
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="modal-footer">
                                    <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Cancle</button>
                                    <input type="submit" class="btn btn-primary pull-left" value="Update" />
                                 </div>
                              </form>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-6">
                        <a href="#delete-{{$student->id}}" data-toggle="modal" class="btn btn-primary btn-block">Delete Student</a> 
                        <div class="modal" id="delete-{{$student->id}}" tabindex="-1" role="dialog">
                           <div class="modal-dialog" role="document">
                              <form action="{{route('delete.student_from_instructor_side',$student->id)}}" method="POST">
                                             {{ csrf_field() }}
                              <div class="modal-content">
                                 <div class="card-header p-2">
                                    <h6 class="m-0 font-weight-bold text-primary">Delete Student</h6>
                                 </div>
                                 <div class="modal-body">
                                    <label>Are you sure you want to delete this student?</label>
                                 </div>
                                 <div class="modal-footer">
                                    <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Cancle</button>
                                    <input type="submit" class="btn btn-primary pull-left" value="Delete" />
                                 </div>
                              </div>
                           </form>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- /.card-body -->
            </div>
            <!-- /.card -->
         </div>
         <!-- /.col -->
         <div class="col-md-8">
            <div class="card">
               <div class="card-header p-2">
                  <ul class="nav nav-pills">
                     <li class="nav-item"><a class="nav-link active" href="#activity" data-toggle="tab">General Informaion</a></li>
                     <li class="nav-item"><a class="nav-link" href="#timeline" data-toggle="tab">Program</a></li>
                     <li class="nav-item"><a class="nav-link" href="#settings" data-toggle="tab">School</a></li>
                     <li class="nav-item"><a class="nav-link" href="#term" data-toggle="tab">Term</a></li>
                  </ul>
               </div>
               <!-- /.card-header -->
               <div class="card-body">
                  <div class="tab-content">
                     <div class="active tab-pane" id="activity">
                        <div class="form-group row">
                           <label for="inputName" class="col-sm-3 col-form-label">Phone</label>
                           <div class="col-sm-9">
                              <input type="text" class="form-control" id="inputName" value="{{$student->phone}}" disabled="disabled">
                           </div>
                        </div>
                        <div class="form-group row">
                           <label for="inputEmail" class="col-sm-3 col-form-label">Student Id</label>
                           <div class="col-sm-9">
                              <input type="text" class="form-control" id="inputEmail" value="{{$student->student_id}}" disabled="disabled">
                           </div>
                        </div>
                        <div class="form-group row">
                           <label for="inputName2" class="col-sm-3 col-form-label">Payment Status</label>
                           <div class="col-sm-9">
                              <input type="text" class="form-control" id="inputName2" value="{{$student->payment}}" disabled="disabled">
                           </div>
                        </div>
                        <div class="form-group row">
                           <label for="inputExperience" class="col-sm-3 col-form-label"> Instructor  </label>
                           <div class="col-sm-9">
                              <input type="text" class="form-control" id="inputName2" value="{{$student->instructor}}" disabled="disabled">
                           </div>
                        </div>
                     </div>
                     <div class="tab-pane" id="timeline">
                        <div class="active tab-pane" id="activity">
                           <div class="form-group row">
                              <label for="inputName" class="col-sm-3 col-form-label">Program</label>
                              <div class="col-sm-9">
                                 <input type="text" class="form-control" id="inputName" value="{{$student->program}}" disabled="disabled">
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- /.tab-pane -->
                     <div class="tab-pane" id="settings">
                        <div class="active tab-pane" id="activity">
                           <div class="form-group row">
                              <label for="inputName" class="col-sm-3 col-form-label">School</label>
                              <div class="col-sm-9">
                                 <input type="text" class="form-control" id="inputName" value="{{$student->school}}" disabled="disabled">
                              </div>
                           </div>
                           <div class="form-group row">
                              <label for="inputName" class="col-sm-3 col-form-label">Zipcode</label>
                              <div class="col-sm-9">
                                 <input type="text" class="form-control" id="inputName" value="{{$student->zipcode}}" disabled="disabled">
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="tab-pane" id="term">
                        <div class="active tab-pane" id="activity">
                           <div class="form-group row">
                              <label for="inputName" class="col-sm-3 col-form-label">Term</label>
                              <div class="col-sm-9">
                                 <input type="text" class="form-control" id="inputName" value="{{$student->term}}" disabled="disabled">
                              </div>
                           </div>
                           <div class="form-group row">
                              <label for="inputName" class="col-sm-3 col-form-label">Group</label>
                              <div class="col-sm-9">
                                 <input type="text" class="form-control" id="inputName" value="{{$student->group}}" disabled="disabled">
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- /.tab-pane -->
                  </div>
                  <!-- /.tab-content -->
               </div>
               <!-- /.card-body -->
            </div>
            <!-- /.nav-tabs-custom -->
         </div>
         <!-- /.col -->
      </div>
      <!-- /.row -->
   </div>
   <!-- /.container-fluid -->
</section>
@endsection