@extends('instructor.layouts.auth')
@section('content')   
<div id="content-wrapper" class="d-flex flex-column">
   <!-- Main Content -->
   <div id="content">
      <!-- Begin Page Content -->
      <div class="container-fluid">
         <!-- Page Heading -->
         <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Reports</h1>
         </div>
         <div class="card shadow mb-4">
            <div class="card-header py-3">
               <h6 class="m-0 font-weight-bold text-primary">Weekly Attendence</h6>
               <div class="col-sm-3">
                  <div class="form-group">
                     <label>Term: </label>
                     <select class="form-control select2" style="width: 100%;">
                        <option selected="selected">Select Term</option>
                        <option>Accelerated Summer 2017</option>
                        <option>Traditional Fall 2017</option>
                        <option>LPN-BSN</option>
                        <option>Fall OB 2018</option>
                     </select>
                  </div>
               </div>
            </div>
            <div class="card-body">
               <div class="table-responsive">
                  <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                     <thead>
                        <tr>
                           <th>Date</th>
                           <th>Student Name</th>
                           <th>Duration</th>
                           <th>Location</th>
                           <th>Digital Signature</th>
                        </tr>
                     </thead>
                     <tfoot>
                        <tr>
                           <th>Date</th>
                           <th>Student Name</th>
                           <th>Duration</th>
                           <th>Location</th>
                           <th>Digital Signature</th>
                        </tr>
                     </tfoot>
                     <tbody>
                        <tr>
                           <td>Tiger Nixon</td>
                           <td>System Architect</td>
                           <td>Edinburgh</td>
                           <td>61</td>
                        </tr>
                        <tr>
                           <td>Garrett Winters</td>
                           <td>Accountant</td>
                           <td>Tokyo</td>
                           <td>63</td>
                        </tr>
                        <tr>
                           <td>Ashton Cox</td>
                           <td>Junior Technical Author</td>
                           <td>San Francisco</td>
                           <td>66</td>
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- End of Main Content -->
</div>
<!-- End of Content Wrapper -->
@endsection