@extends('instructor.layouts.auth')
@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-1">
            </div>
            <div class="col-md-10">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Student activity log</h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Select Student</label>
                                    <select class="form-control select2" style="width: 100%;"> 
                                        <option>Alaska</option>
                                        <option>California</option>
                                        <option>Delaware</option>
                                        <option>Tennessee</option>
                                        <option>Texas</option>
                                        <option>Washington</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Select Term</label>
                                    <select class="form-control select2" style="width: 100%;">
                                        <option selected="selected">Alabama</option>
                                        <option>Alaska</option>
                                        <option>California</option>
                                        <option>Delaware</option>
                                        <option>Tennessee</option>
                                        <option>Texas</option>
                                        <option>Washington</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Select Group</label>
                                    <select class="form-control select2" style="width: 100%;">
                                        <option selected="selected">Alabama</option>
                                        <option>Alaska</option>
                                        <option>California</option>
                                        <option>Delaware</option>
                                        <option>Tennessee</option>
                                        <option>Texas</option>
                                        <option>Washington</option>
                                    </select>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Date range:</label> 
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                        							<i class="far fa-calendar-alt"></i>
                      						</span>
                                        </div>
                                        <input type="text" class="form-control float-right" id="reservation">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Skills:</label> 
                                    <select class="form-control select2" style="width: 100%;">
                                        <option selected="selected">Select Skills</option>
                                        <option>Alaska</option>
                                        <option>California</option>
                                        <option>Delaware</option>
                                        <option>Tennessee</option>
                                        <option>Texas</option>
                                        <option>Washington</option>
                                    </select>
                                </div>
                            </div>
                            </div>


                                 <button type="button" class="btn btn-block bg-gradient-primary btn-lg">Submit</button>
                                
                           <br> 
                        <h5>NOTE :</h5>
                        <p>1. Please Select Term or Date Range</p>
                        <p>2. Please select ‘All students’ to choose a Group</p>
                    </div>
                    <div class="col-md-3">
                        <a href="#delete" data-toggle="modal" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"> Filter by Skill</a> 
                           <div class="modal" id="delete" tabindex="-1" role="dialog">
                              <div class="modal-dialog" role="document">
                                 <div class="modal-content">
                                     
                                    <form action="" method="POST">
                                       {{ csrf_field() }}
                                    <div class="modal-header">
                                       <h4 class="modal-title">Assign Clinical Remediation</h4>
                                       <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                       <span aria-hidden="true">&times;</span>
                                       </button>
                                    </div>
                                    <div class="modal-body"> 
                           <div class="form-group">
                        <label>Select Skills</label>
                        <input type="text" class="form-control" placeholder="Search Skills">
                      </div>
                      <div class="row">
                    <div class="col-sm-12">
                      <!-- checkbox -->
                      <div class="form-group">
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox">
                          <label class="form-check-label">Select All</label>
                        </div>  
                      </div>
                    </div>  
                    </div> 
                        <div class="row">
                    <div class="col-sm-6">
                      <!-- checkbox -->
                      <div class="form-group">
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox">
                          <label class="form-check-label"> Occurrence/Incident</label>
                        </div>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" checked>
                          <label class="form-check-label">Clinical Warning</label>
                        </div>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox">
                          <label class="form-check-label"> Clinical Probation</label>
                        </div> 
                      </div>
                    </div> 
                    <div class="col-sm-6">
                      <!-- checkbox -->
                      <div class="form-group">
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox">
                          <label class="form-check-label">Missing Documents</label>
                        </div>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" >
                          <label class="form-check-label"> Alternate Assignments</label>
                        </div>  
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox">
                          <label class="form-check-label">Clinical Demerit</label>
                        </div> 
                        </div>  
                      </div>
                    </div> 
                                    </div>
                                    <div class="modal-footer">
                                       <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Cancle</button>
                                       <input type="submit" class="btn btn-primary pull-left" value="Filter" />
                                      
                                       </form>
                                    </div>
                                    
                                 </div>
                              </div>

                </div>
            </div>
            <div class="col-md-1">
            </div>
        </div>
    </div>
</section>
@endsection