@extends('instructor.layouts.auth')
@section('content')   
<div id="content-wrapper" class="d-flex flex-column">
   <!-- Main Content -->
   <div id="content">
      <!-- Begin Page Content -->
      <div class="container-fluid">
         <!-- Page Heading -->
         <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Home</h1>
         </div> 
         <div class="card shadow mb-4">
            <div class="card-header py-3">
               <h6 class="m-0 font-weight-bold text-primary">Activity Log</h6>
            </div>
             

             <!-- <div id="myGroup" class="container">
  <div class="panel">

    <div class="row mint" role="button" data-toggle="collapse" data-parent="#myGroup" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
      <div class="col-xs-4">Block 3-Assessment & Diagnostic Nursing Care</div> 
    </div>
    <div id="collapseOne" class="row collapse">
      <div class="text mint">(Focus: Adult) Abdomen</div>  
    </div>
    <div id="collapseOne" class="row collapse">
      <div class="text mint">(Focus: Adult) Appearance/Skin</div> 
    </div>
    <div id="collapseOne" class="row collapse">
      <div class="text mint">(Focus: Adult) Cardiovascular</div> 
    </div>
    <div id="collapseOne" class="row collapse">
      <div class="text mint">(Focus: Adult) Genitourinary</div> 
    </div>
    <div id="collapseOne" class="row collapse">
      <div class="text mint">(Focus: Adult) Head, Neck</div> 
    </div>
    <div id="collapseOne" class="row collapse">
      <div class="text mint">(Focus: Adult) Musculoskeletal</div> 
    </div>
    <div id="collapseOne" class="row collapse">
      <div class="text mint">(Focus: Adult) Neurological</div> 
    </div> 


    <div class="row mauve" role="button" data-toggle="collapse" data-parent="#myGroup" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
      <div class="col-xs-4">General</div> 
    </div>
    <div id="collapseTwo" class="row collapse">
     <div class="text mauve">text</div>
    </div>
    
    <div class="row peach" role="button" data-toggle="collapse" data-parent="#myGroup" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
      <div class="col-xs-4">IV</div> 
    </div>
    <div id="collapseThree" class="row collapse">
      <div class="text peach">text</div>
    </div>

    <div class="row peach" role="button" data-toggle="collapse" data-parent="#myGroup" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
      <div class="col-xs-4">NUR 326: Level III-Childbearing</div> 
    </div>
    <div id="collapseFour" class="row collapse">
      <div class="text peach">text</div>
    </div>

    <div class="row peach" role="button" data-toggle="collapse" data-parent="#myGroup" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
      <div class="col-xs-4">NUR 336-Medication Administration</div> 
    </div>
    <div id="collapseFive" class="row collapse">
      <div class="text peach">text</div>
    </div>

     <div class="row peach" role="button" data-toggle="collapse" data-parent="#myGroup" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
      <div class="col-xs-4">NUR 336: Level III-Childrearing</div> 
    </div>
    <div id="collapseSix" class="row collapse">
      <div class="text peach">text</div>
    </div>

     <div class="row peach" role="button" data-toggle="collapse" data-parent="#myGroup" href="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
      <div class="col-xs-4">Oxygen</div> 
    </div>
    <div id="collapseSeven" class="row collapse">
      <div class="text peach">text</div>
    </div>

     <div class="row peach" role="button" data-toggle="collapse" data-parent="#myGroup" href="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
      <div class="col-xs-4">Vital Signs</div> 
    </div>
    <div id="collapseEight" class="row collapse">
      <div class="text peach">text</div>
    </div>
    
  </div>
</div> -->

<!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary   sidebar-dark accordion" id="accordionSidebar">
 

       
      <!-- Nav Item - Utilities Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#one" aria-expanded="true" aria-controls="collapseUtilities">
          <i class="fas fa-fw fa-wrench"></i>
          <span>Block 3-Assessment & Diagnostic Nursing Care</span>
        </a>
        <div id="one" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded"> 
            <h6 class="collapse-header">Login Screens:</h6>  
          </div>
        </div>
        <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded"> 
            <h6 class="collapse-header">Login Screens:</h6> 
          </div>
        </div>
      </li> 
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#two" aria-expanded="true" aria-controls="collapsePages">
          <i class="fas fa-fw fa-folder"></i>
          <span>General</span>
        </a>
        <div id="two" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Login Screens:</h6> 
          </div>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#three" aria-expanded="true" aria-controls="collapsePages">
          <i class="fas fa-fw fa-folder"></i>
          <span>IV</span>
        </a>
        <div id="three" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Login Screens:</h6> 
          </div>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#four" aria-expanded="true" aria-controls="collapsePages">
          <i class="fas fa-fw fa-folder"></i>
          <span>NUR 326: Level III-Childbearing</span>
        </a>
        <div id="four" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Login Screens:</h6> 
          </div>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#five" aria-expanded="true" aria-controls="collapsePages">
          <i class="fas fa-fw fa-folder"></i>
          <span>NUR 336-Medication Administration</span>
        </a>
        <div id="five" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Login Screens:</h6> 
          </div>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#six" aria-expanded="true" aria-controls="collapsePages">
          <i class="fas fa-fw fa-folder"></i>
          <span>NUR 336: Level III-Childrearing</span>
        </a>
        <div id="six" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Login Screens:</h6> 
          </div>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#seven" aria-expanded="true" aria-controls="collapsePages">
          <i class="fas fa-fw fa-folder"></i>
          <span>Oxygen</span>
        </a>
        <div id="seven" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Login Screens:</h6> 
          </div>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#eight" aria-expanded="true" aria-controls="collapsePages">
          <i class="fas fa-fw fa-folder"></i>
          <span>Vital Signs</span>
        </a>
        <div id="eight" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Login Screens:</h6> 
          </div>
        </div>
      </li>

       

    </ul>
    <!-- End of Sidebar -->


         </div>
      </div>
      <!-- /.container-fluid -->
   </div>
   <!-- End of Main Content -->
</div>
<!-- End of Content Wrapper -->
@endsection