@extends('instructor.layouts.auth')
@section('content')   
<div id="content-wrapper" class="d-flex flex-column">
   <!-- Main Content -->

   <div id="content">
     <div class="container-fluid">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Profile</h1>
         </div> 
    <div class="card card-primary card-outline">

              <div class="card-body box-profile">
                <form action="{{route('update.instructor.profile',Auth::user()->id)}}" method="POST" enctype="multipart/form-data">
                  {{ csrf_field() }}
                <div class="text-center">
                  <img class="profile-user-img img-fluid img-circle"
                       src="{{ Auth::user()->image }}"
                       alt="User profile picture">
                       <div class="col-sm-3"> 
                         <a href="#" id="select_logo" class="btn btn-primary btn-block float-right"><b>Upload File</b></a>
                           <input type="file" name="image" id="logo"> 
                       </div> 
                </div>

                <h3 class="profile-username text-center">{{ Auth::user()->first_name }}  {{ Auth::user()->last_name   }}</h3>

                <p class="text-muted text-center">Software Engineer</p>

                <ul class="list-group list-group-unbordered ">
                  <li class="list-group-item">
                    <b>First Name</b> <a class="float-right"> <input type="text" class="form-control" placeholder="Fisrt Name" name="first_name" value="{{ Auth::user()->first_name }}"></a>
                  </li>
                   <li class="list-group-item">
                    <b>Last Name</b> <a class="float-right"> <input type="text" class="form-control" placeholder="Last Name" name="last_name" value="{{ Auth::user()->last_name }}"></a>
                  </li>
                   <li class="list-group-item">
                    <b>E-mail Address</b> <a class="float-right"> <input type="text" class="form-control" placeholder="E-mail Address" name="email" value="{{ Auth::user()->email }}" disabled="disabled"></a>
                  </li>
                   <li class="list-group-item">
                    <b>Phone</b> <a class="float-right"> <input type="text" class="form-control" placeholder="Phone" name="phone" value="{{ Auth::user()->phone }}"></a>
                  </li>
                  <li class="list-group-item">
                    <b>Zipcode</b> <a class="float-right"> <input type="text" class="form-control" placeholder="Zipcode" name="zipcode" value="{{ Auth::user()->zipcode }}"></a>
                  </li>
                   <li class="list-group-item"> 
                    <b>Credentials</b> <a class="float-right"> <input type="text" class="form-control" name="credentials" placeholder="Credentials"></a>
                  </li>
                   <li class="list-group-item">
                    <b>Instructor Code</b> <a class="float-right"> <input type="text" name="student_id" value="{{ Auth::user()->student_id }}" class="form-control" placeholder="Instructor Code"></a>
                  </li>
                   <li class="list-group-item">
                    <b>School</b> <a class="float-right"> <input type="text" class="form-control" name="school" value="{{ Auth::user()->school }}" placeholder="School"></a>
                  </li>
                   <li class="list-group-item">
                    <b>Program</b> <a class="float-right"> <input type="text" class="form-control" name="program" value="{{ Auth::user()->program }}" placeholder="Program"></a>
                  </li>
                   <li class="list-group-item">
                    <b>Grade</b> <a class="float-right"> <input type="text" class="form-control" value="{{ Auth::user()->grade }}" placeholder="Grade" disabled="disabled"></a>
                  </li>
                   <li class="list-group-item">
                    <b>Term</b> <a class="float-right"> <input type="text" class="form-control" value="{{ Auth::user()->term }}" placeholder="Term" disabled="disabled"></a>
                  </li> <li class="list-group-item">
                    <b>Group</b> <a class="float-right"> <input type="text" class="form-control" value="{{ Auth::user()->group }}" placeholder="Group" disabled="disabled"></a>
                  </li> 
                </ul>  
                <input type="submit" class="btn btn-primary btn-block" value="Save Changes">
              </form>
              </div>
              <!-- /.card-body -->
            </div>
          </div>
   </div>
   <!-- End of Main Content -->
</div>
<!-- End of Content Wrapper -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script type="text/javascript">
   $("#logo").css('opacity','0'); 
    $("#select_logo").click(function(e){ 
       e.preventDefault();
       $("#logo").trigger('click');
    });
</script>
@endsection