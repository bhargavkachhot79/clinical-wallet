@extends('layouts.auth')
@section('content')
<div class="container-fluid">
   <!-- Page Heading -->
   <div class="d-sm-flex align-items-center justify-content-between mb-4">
      <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
       @if (session('status'))  
                      <div class="alert alert-success" style="text-align: center;">
                      {{ session('status') }} 
                      <a href="" class="close" data-dismiss="alert" aria-label="close" style="padding-left: 5px">&times;</a>
                      </div>
                 @endif 
   </div>
   <div class="row"> 
   </div>
   <div class="row">
      <div class="col-xl-12 col-lg-7">
         <div class="card shadow mb-4">
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
               <h6 class="m-0 font-weight-bold text-primary">Change Password</h6>
               <div class="dropdown no-arrow">
                  <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                  </a>
                  <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                     <div class="dropdown-header">Dropdown Header:</div>
                     <a class="dropdown-item" href="#">Action</a>
                     <a class="dropdown-item" href="#">Another action</a>
                     <div class="dropdown-divider"></div>
                     <a class="dropdown-item" href="#">Something else here</a>
                  </div>
               </div>
            </div>
            <!-- Card Body -->
            <div class="card-body"> 
                  <form action="{{ route('student.update-password',$id) }}" method="POST" class="form-horizontal">
                  	{{ csrf_field() }}
                     <div class="card-body">
                        <div class="form-group row">
                           <label for="inputEmail3" class="col-sm-2 col-form-label">Password</label>
                           <div class="col-sm-10">
                              <input type="password" class="form-control" name="password" id="inputEmail3" placeholder="Password" >
                           </div>
                        </div>
                        <div class="form-group row">
                           <label for="inputPassword3" class="col-sm-2 col-form-label">Confirm Password</label>
                           <div class="col-sm-10">
                              <input type="password" class="form-control" name="confirm_password" id="inputPassword3" placeholder="Confirm Password" >
                           </div>
                        </div> 

                     </div><a href="{{route('home')}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm left">Cancel </a>
                     <input type="Submit" name="Submit" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm right" value="Submit"> 
                  </form>
               
            </div>
         </div>
      </div>
      
   </div>
</div>
@endsection