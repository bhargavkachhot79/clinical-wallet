@extends('layouts.auth')
@section('content')
<div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Learning Contract Form</h1>

 <div class="card shadow mb-4"> 
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Student</th>
                      <th>Date Initiated</th>
                      <th>Course</th>
                      <th>Status</th> 
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>Student</th>
                      <th>Date Initiated</th>
                      <th>Course</th>
                      <th>Status</th> 
                      <th>Action</th>
                    </tr>
                  </tfoot>
                  <tbody>
                    <tr>
                      <td>Tiger Nixon</td>
                      <td>System Architect</td>
                      <td>Edinburgh</td>
                      <td>61</td>
                      <td>2011/04/25</td> 
                    </tr> 
                    <tr>
                      <td>Donna Snider</td>
                      <td>Customer Support</td>
                      <td>New York</td>
                      <td>27</td>
                      <td>2011/01/25</td> 
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
@endsection