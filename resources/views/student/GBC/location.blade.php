@extends('layouts.auth') @section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12 mb-4">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Projects</h6>
                    <div class="card-header">
                        <div class="card-tools">
                            <ul class="nav nav-pills ml-auto">
                                <li class="nav-item">
                                    <a class="nav-link active" href="#revenue-chart" data-toggle="tab">Location</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link " href="#sales-chart" data-toggle="tab">Guidelines</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#sales-chart" data-toggle="tab">MANDATORY COMPETENCIES AND INDICATORS</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#sales-chart" data-toggle="tab">ACCOUNTABILITY INDICATORS</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#sales-chart" data-toggle="tab">KNOWLEDGE INDICATORS</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#sales-chart" data-toggle="tab">KNOWLEDGE APPLICATION INDICATORS</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#sales-chart" data-toggle="tab">CONTINUING COMPETENCE INDICATORS</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#sales-chart" data-toggle="tab">ETHICS INDICATORS</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#sales-chart" data-toggle="tab">RELATIONSHIPS INDICATORS</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#sales-chart" data-toggle="tab">LEADERSHIP INDICATORS</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#sales-chart" data-toggle="tab">LEADERSHIP INDICATORS</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="card card-default">
                        <div class="card-body">
                            <form id="regForm" action="/action_page.php">
                                <div class="tab">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Instructor</label>
                                                <select class="form-control select2" style="width: 100%;">
                                                    <option selected="selected">Alabama</option>
                                                    <option>Alaska</option>
                                                    <option>California</option>
                                                    <option>Delaware</option>
                                                    <option>Tennessee</option>
                                                    <option>Texas</option>
                                                    <option>Washington</option>
                                                </select>
                                            </div>

                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Term</label>
                                                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Term">
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">ID#</label>
                                                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Id">
                                            </div>

                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Clinical Site and Unit</label>
                                                <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Clinical Site and Unit">
                                            </div>

                                        </div>

                                    </div>
                                </div>
                                <div class="tab">
                                    <div class="card-body">
                                        <ul>
                                            <li>
                                                <p class="mb-0 small">The student is expected to meet the minimum standards for safe clinical practice on a consistent basis (>60% of the time) in ALL areas. With the exception of the Mandatory Competency and indicators, failure to meet minimal standards consistently (at least 60% of the time) is deemed to be performing below minimum standards for safe clinical practice and as such places a patient at risk for an adverse event to occur.</p>
                                            </li>
                                            <li>
                                                <p class="mb-0 small">Mandatory competency and indicators must be met at least 70% of the time to ensure safe clinical practice.</p>
                                            </li>
                                            <li>
                                                <p class="mb-0 small">Please note: An Adverse Event is "an incident that occurs during the process of providing health care and results in patient injury or death" (Canadian Patient Safety Dictionary as cited in White, 2009, p. 3).</p>
                                            </li>
                                        </ul>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <p class="mb-0 small">Guidelines for Clinical Evaluation</p>
                                                <ul>
                                                    <li>
                                                        <p class="mb-0 small">The clinical component is evaluated with a Pass or Non-Pass</p>
                                                    </li>
                                                    <li>
                                                        <p class="mb-0 small">The final rating for the course consists of two key components:</p>
                                                    </li>
                                                    <ul>
                                                        <li>
                                                            <p class="mb-0 small">The clinical component is evaluated with a Pass or Non-Pass</p>
                                                        </li>
                                                        <li>
                                                            <p class="mb-0 small">The final rating for the course consists of two key components:</p>
                                                        </li>
                                                    </ul>
                                                    <li>
                                                        <p class="mb-0 small">The student will be assessed according to the indicators for each Professional Standard Statement Competencies.</p>
                                                    </li>
                                                    <li>
                                                        <p class="mb-0 small">To indicate whether the student met each indicator, the Clinical Instructor (CI) will mark the appropriate box as “Satisfactory, Needs Improvement, or Unsatisfactory”.</p>
                                                    </li>
                                                    <li>
                                                        <p class="mb-0 small">The CI will provide ongoing objective feedback to the student throughout the semester.</p>
                                                    </li>
                                                    <li>
                                                        <p class="mb-0 small">The CI is required to document a summarized comment for each standard statement section at midterm and final, including a specific example of the student’s behaviour to support the comment. Ratings between midterm and final evaluation can go up, stay the same or go down depending on evaluated performance.</p>
                                                    </li>
                                                    <li>
                                                        <p class="mb-0 small">To be successful, the student is expected to meet the minimum standards for safe clinical practice (see Table 1) on a consistent basis in ALL areas.</p>
                                                    </li>
                                                    <li>
                                                        <p class="mb-0 small">The student can be rated as “Needs Improvement” ONLY for unshaded Standard Statement indicators on the FINAL evaluation to PASS. Shaded areas are indicators that must be met by the end of the term, which reflects meeting the competencies.</p>
                                                    </li>
                                                    <li>
                                                        <p class="mb-0 small">A student that does not meet Professional Standard Statement on ANY indicator in the FINAL evaluation will automatically receive a NON-PASS.</p>
                                                    </li>
                                                    <ul>
                                                        <li>
                                                            <p class="mb-0 small">Letters of concern and learning plans that are given and discussed with students will be documented on the cover page. A copy of these documents will be attached to the final evaluation, with all other supporting documentation that demonstrates the student’s success or failure to meet the strategies agreed upon by the CI and student.</p>
                                                        </li>
                                                        <li>
                                                            <p class="mb-0 small">As students’ progress through the Practical Nursing Program, the clinical practice performance rating will progress from “Needs Improvement to Satisfactory” and reflect the student’s ability to meet the Entry to Practice Competencies for Ontario Registered Practical Nurses (CNO, 2018).</p>
                                                        </li>
                                                    </ul>
                                                </ul>
                                            </div>
                                            <div class="col-lg-6">
                                                <p class="mb-0 small">Table 1: Examples of student performance that are consistent with Meeting Practice Standards Statement Sections & Minimum Standards of Safe Practice The student nurse:
                                                </p>
                                                <ul>
                                                    <li>
                                                        <p class="mb-0 small">Prior to giving care, assesses client(s) and review their data from a variety of sources to plan and organize client care.</p>
                                                    </li>
                                                    <li>
                                                        <p class="mb-0 small">Discusses assigned client’s pathophysiology and related symptoms in his/her own words.</p>
                                                    </li>
                                                    <li>
                                                        <p class="mb-0 small">Discusses client’s medical treatment(s) and rationale.</p>
                                                    </li>
                                                    <li>
                                                        <p class="mb-0 small">Correctly selects a priority nursing diagnosis and discusses at least two interventions that are supported by evidence-based practice.</p>
                                                    </li>
                                                    <li>
                                                        <p class="mb-0 small">Demonstrates correct medication and intravenous administration, including calculations and safe dosages, and can communicate actions, side effects, and nursing implications.</p>
                                                    </li>
                                                    <li>
                                                        <p class="mb-0 small">Demonstrates psychomotor skills at a beginner level that is appropriate to the practice setting.</p>
                                                    </li>
                                                    <li>
                                                        <p class="mb-0 small">Maintains asepsis, routine precautions and safe practice.</p>
                                                    </li>
                                                    <li>
                                                        <p class="mb-0 small">Reports to appropriate parties (i.e., RN, RPN, instructor) regarding all client care and changes in a client’s condition including breaks and end of shift report.</p>
                                                    </li>
                                                    <li>
                                                        <p class="mb-0 small">Documents in a timely manner and according to current nursing practice within the setting and specialty area.</p>
                                                    </li>
                                                    <li>
                                                        <p class="mb-0 small">Maintains confidentiality and client safety.</p>
                                                    </li>
                                                    <li>
                                                        <p class="mb-0 small">Communicates in a clear and professional manner.</p>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="form-group row">
                                                <div class="offset-sm-12 col-sm-12">
                                                    <div class="form-check">
                                                        <input type="checkbox" class="form-check-input" id="exampleCheck2">
                                                        <label class="form-check-label" for="exampleCheck2">I have read and accept guidelines.</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab">
                                    <div class="card card-primary">
                                        <div class="card-header">
                                            <h3 class="card-title">MANDATORY COMPETENCIES AND INDICATORS</h3>
                                        </div>

                                        <div class="card-body">
                                            <div class="tab-pane" id="settings">
                                                <div class="form-group row">
                                                    <label for="inputName" class="col-sm-6 col-form-label"></label>
                                                    <div class="col-sm-3">
                                                        <label>Mid-Term</label>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <label>Final</label>
                                                    </div>
                                                </div>
                                                <label>Accountability</label>
                                                <div class="form-group row">
                                                    <label for="inputName" class="col-sm-6 col-form-label">1. Maintains client safety.</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputEmail" class="col-sm-6 col-form-label">2. Maintains infection prevention and control measures.</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputName2" class="col-sm-6 col-form-label">3. Refrains from performing activities beyond their expected scope and skill level.</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <hr>
                                                <label>Ethics</label>
                                                <div class="form-group row">
                                                    <label for="inputName" class="col-sm-6 col-form-label">4. Maintains client confidentiality within legal and regulatory parameters. (E.g. RHPA, PHIPA)</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputEmail" class="col-sm-6 col-form-label">5. Obtains client consent prior to initiating nursing care. measures.</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <hr>
                                                <label>Knowledge Application</label>
                                                <div class="form-group row">
                                                    <label for="inputName" class="col-sm-6 col-form-label">6. Identifies/recognizes client responses and:
                                                        <br>a. interprets data</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputEmail" class="col-sm-6 col-form-label">b. takes appropriate action</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputEmail" class="col-sm-6 col-form-label">c. reports in a timely fashion</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>

                                            <h3 class="card-title">Clinical Student Comments</h3>
                                            <p>ACCOUNTABILITY INDICATORS: Specific events/evidence/examples to support rating by Instructor:</p>

                                            <label>Midterm</label>
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Midterm">
                                            </div>
                                            <label>Learning Needs and Goals:</label>
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Learning Needs and Goals">
                                            </div>
                                            <label>Final</label>
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Final">
                                            </div>
                                            <label>Learning Needs and Goals:</label>
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Learning Needs and Goals">
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="tab">
                                    <div class="card card-primary">
                                        <div class="card-header">
                                            <h3 class="card-title">ACCOUNTABILITY INDICATORS</h3>
                                        </div>

                                        <div class="card-body">
                                            <div class="tab-pane" id="settings">
                                                <div class="form-group row">
                                                    <label for="inputName" class="col-sm-6 col-form-label"></label>
                                                    <div class="col-sm-3">
                                                        <label>Mid-Term</label>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <label>Final</label>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputName" class="col-sm-6 col-form-label">1. Comes prepared to all clinical and Simulation Center activities including handing in assignments on time</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputEmail" class="col-sm-6 col-form-label">2. Demonstrates respect in clinical/lab setting as evidenced by:
                                                        <br>a. Appropriate language and behaviour.</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputName2" class="col-sm-6 col-form-label">b. Proper identification of one’s self and role.</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputName" class="col-sm-6 col-form-label">c. Promptness for meetings, learning activities, and deadlines.</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputEmail" class="col-sm-6 col-form-label">d. Identifies one’s limitations in nursing practice and consults others</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputName" class="col-sm-6 col-form-label">3. Seeks assistance in an appropriate and timely manner</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputEmail" class="col-sm-6 col-form-label">4. Maintains clear, concise, accurate and timely records of client’s care.</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputEmail" class="col-sm-6 col-form-label">5 Is aware of the appropriate action to avoid errors and adverse events, and if required, initiates the actions.</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputEmail" class="col-sm-6 col-form-label">6. Accountable for ones’ decisions and actions by:
                                                        <br>a. practicing within one’s role and responsibilities</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputEmail" class="col-sm-6 col-form-label">b. verifying and clarifying policies and procedures</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>

                                            <h3 class="card-title">Clinical Student Comments</h3>
                                            <p>MANDATORY COMPETENCIES AND INDICATORS: Specific events/evidence/examples to support rating by Instructor:</p>

                                            <label>Midterm</label>
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Enter email">
                                            </div>
                                            <label>Learning Needs and Goals:</label>
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Enter email">
                                            </div>
                                            <label>Final</label>
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Enter email">
                                            </div>
                                            <label>Learning Needs and Goals:</label>
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Enter email">
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="tab">
                                    <div class="card card-primary">
                                        <div class="card-header">
                                            <h3 class="card-title">KNOWLEDGE INDICATORS</h3>
                                        </div>

                                        <div class="card-body">
                                            <div class="tab-pane" id="settings">
                                                <div class="form-group row">
                                                    <label for="inputName" class="col-sm-6 col-form-label"></label>
                                                    <div class="col-sm-3">
                                                        <label>Mid-Term</label>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <label>Final</label>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputName" class="col-sm-6 col-form-label">1. Participates in all educational activities (i.e., pre and post conferences) in the clinical setting and Simulation Center.</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputEmail" class="col-sm-6 col-form-label">2. Seeks out opportunities for learning.</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputName2" class="col-sm-6 col-form-label">3. Demonstrates knowledge appropriate to the assigned clients and practice setting without prompting or probing.</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputName" class="col-sm-6 col-form-label">4. Shares knowledge with colleagues in the clinical setting.</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputEmail" class="col-sm-6 col-form-label">5. Demonstrates knowledge of critical thinking and problem solving.</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputName" class="col-sm-6 col-form-label">6. Provides evidence-based rationale for decisions.</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputEmail" class="col-sm-6 col-form-label">7. Demonstrates knowledge of the determinants of health in practice.</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputEmail" class="col-sm-6 col-form-label">8. a. Collects data from a variety of sources to develop client(s) plan of care in a timely fashion</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputEmail" class="col-sm-6 col-form-label">b. Cluster relevant and pertinent assessment data to support nursing diagnosis and implement appropriate interventions.</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputEmail" class="col-sm-6 col-form-label">c. Analyzes and interprets initial assessment findings and collaborates with the client in developing approaches to nursing care.</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputEmail" class="col-sm-6 col-form-label">9. Successfully meets the requirements of the Psychomotor Skills Checklist.</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>

                                            <h3 class="card-title">Clinical Student Comments</h3>
                                            <p>KNOWLEDGE INDICATORS: Specific events/evidence/examples to support rating by Instructor:</p>

                                            <label>Midterm</label>
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Midterm">
                                            </div>
                                            <label>Learning Needs and Goals:</label>
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Learning Needs and Goals">
                                            </div>
                                            <label>Final</label>
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Final">
                                            </div>
                                            <label>Learning Needs and Goals:</label>
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Learning Needs and Goals">
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="tab">
                                    <div class="card card-primary">
                                        <div class="card-header">
                                            <h3 class="card-title">KNOWLEDGE APPLICATION INDICATORS</h3>
                                        </div>
                                        <div class="card-body">
                                            <div class="tab-pane" id="settings">
                                                <div class="form-group row">
                                                    <label for="inputName" class="col-sm-6 col-form-label"></label>
                                                    <div class="col-sm-3">
                                                        <label>Mid-Term</label>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <label>Final</label>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputName" class="col-sm-6 col-form-label">1. Utilizes the best available evidence, preferably practice guidelines (including policies and procedures) to provide care.</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputEmail" class="col-sm-6 col-form-label">2. Assesses client using theory/framework/evidence-based tool.</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputName2" class="col-sm-6 col-form-label">3. a. Utilizes the nursing process to develop a plan of care based on analysis and interpretation of client data.</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputName" class="col-sm-6 col-form-label">b Articulates the rationale to support the identified nursing priorities</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputEmail" class="col-sm-6 col-form-label">4. Manages nursing interventions competently.</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputName" class="col-sm-6 col-form-label">5. Manages multiple nursing interventions simultaneously.</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputEmail" class="col-sm-6 col-form-label">6. Manages an increasing number of clients with increasing complexity in a timely fashion.</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputEmail" class="col-sm-6 col-form-label">7. Recognizes changing events in the health care environment and notifies appropriately and timely.</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputEmail" class="col-sm-6 col-form-label">8. Develops clinical judgments that are consistent with clients’ needs and priorities by responding to changing situations that affect clients’ health and safety.</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputEmail" class="col-sm-6 col-form-label">9. Questions, clarifies and challenges unclear or questionable orders, decisions or actions made by other interprofessional health care team members.</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputEmail" class="col-sm-6 col-form-label">10. Collaborates with client and health care team to perform appropriate interventions.</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputEmail" class="col-sm-6 col-form-label">11. Incorporates appropriate health teaching strategies into care.</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputEmail" class="col-sm-6 col-form-label">12. Demonstrates knowledge of health informatics to document client care, and to obtain and forward information within the agency.</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputEmail" class="col-sm-6 col-form-label">13. Demonstrates critical thinking and independent judgment in clinical decision making through:
                                                        <br>a. Pre and post conferences and/or Simulation Center activities</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputEmail" class="col-sm-6 col-form-label">b. Medication cards</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputEmail" class="col-sm-6 col-form-label">c. Clinical daily worksheets</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputEmail" class="col-sm-6 col-form-label">d. Client daily assessments</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputEmail" class="col-sm-6 col-form-label">e. Questioning circles</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>

                                            <h3 class="card-title">Clinical Student Comments</h3>
                                            <p>KNOWLEDGE APPLICATION INDICATORS: Specific events/evidence/examples to support rating by Instructor:</p>

                                            <label>Midterm</label>
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Midterm">
                                            </div>
                                            <label>Learning Needs and Goals:</label>
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Learning Needs and Goals">
                                            </div>
                                            <label>Final</label>
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Final">
                                            </div>
                                            <label>Learning Needs and Goals:</label>
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Learning Needs and Goals">
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="tab">
                                    <div class="card card-primary">
                                        <div class="card-header">
                                            <h3 class="card-title">CONTINUING COMPETENCE INDICATORS</h3>
                                        </div>
                                        <div class="card-body">
                                            <div class="tab-pane" id="settings">
                                                <div class="form-group row">
                                                    <label for="inputName" class="col-sm-6 col-form-label"></label>
                                                    <div class="col-sm-3">
                                                        <label>Mid-Term</label>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <label>Final</label>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputName" class="col-sm-6 col-form-label">1. Uses standards of practice to self-evaluate one’s competence, to identify strengths and gaps in knowledge, skills and attitude.</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputEmail" class="col-sm-6 col-form-label">2. Responds positively to constructive feedback and demonstrates appropriate change in behaviour.</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputName2" class="col-sm-6 col-form-label">3. Takes action to achieve learning strategies identified by student/instructor during a performance appraisal.</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputName" class="col-sm-6 col-form-label">4. Demonstrates practice that reflects knowledge of professional laws and regulations. (E.g. CNO Standards, RHPA, PHIPA).</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputEmail" class="col-sm-6 col-form-label">5. Uses reflection and feedback from a variety of sources to evaluate and implement changes to their practice.</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputName" class="col-sm-6 col-form-label">6. Completes self-evaluation that reflects insight and self- awareness of performance in a timely fashion.</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>

                                            <h3 class="card-title">Clinical Student Comments</h3>
                                            <p>CONTINUING COMPETENCE INDICATORS: Specific events/evidence/examples to support rating by Instructor:</p>

                                            <label>Midterm</label>
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Midterm">
                                            </div>
                                            <label>Learning Needs and Goals:</label>
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Learning Needs and Goals">
                                            </div>
                                            <label>Final</label>
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Final">
                                            </div>
                                            <label>Learning Needs and Goals:</label>
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Learning Needs and Goals">
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="tab">
                                    <div class="card card-primary">
                                        <div class="card-header">
                                            <h3 class="card-title">ETHICS INDICATORS</h3>
                                        </div>
                                        <div class="card-body">
                                            <div class="tab-pane" id="settings">
                                                <div class="form-group row">
                                                    <label for="inputName" class="col-sm-6 col-form-label"></label>
                                                    <div class="col-sm-3">
                                                        <label>Mid-Term</label>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <label>Final</label>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputName" class="col-sm-6 col-form-label">1. Demonstrates behaviour that respects client choice, diversity, and cultural beliefs and practices.</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputEmail" class="col-sm-6 col-form-label">2. Interacts with peers and other health care providers in a non-judgmental and non-discriminatory manner.</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputName2" class="col-sm-6 col-form-label">3. Acts as a client and family advocate.</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputName" class="col-sm-6 col-form-label">4. Delivers care in a manner that preserves client autonomy, dignity, and rights.</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputEmail" class="col-sm-6 col-form-label">5. Demonstrates use of a decision-making process to address challenging situations for the student, client and peers.</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputName" class="col-sm-6 col-form-label">6. Demonstrates appropriate choices in clinical attendance and accepts any outcomes as a result. E.g. Followed flu protocol, attends all clinical days including Simulation.</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputEmail" class="col-sm-6 col-form-label">7. Provides care in a fair and equitable manner.</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputEmail" class="col-sm-6 col-form-label">8. Demonstrates behaviour that is respectful of the culture of the health care setting.</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>

                                            <h3 class="card-title">Clinical Student Comments</h3>
                                            <p>ETHICS INDICATORS: Specific events/evidence/examples to support rating by Instructor:</p>

                                            <label>Midterm</label>
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Midterm">
                                            </div>
                                            <label>Learning Needs and Goals:</label>
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Learning Needs and Goals">
                                            </div>
                                            <label>Final</label>
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Final">
                                            </div>
                                            <label>Learning Needs and Goals:</label>
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Learning Needs and Goals">
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="tab">
                                    <div class="card card-primary">
                                        <div class="card-header">
                                            <h3 class="card-title">RELATIONSHIPS INDICATORS</h3>
                                        </div>
                                        <div class="card-body">
                                            <div class="tab-pane" id="settings">
                                                <div class="form-group row">
                                                    <label for="inputName" class="col-sm-6 col-form-label"></label>
                                                    <div class="col-sm-3">
                                                        <label>Mid-Term</label>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <label>Final</label>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputName" class="col-sm-6 col-form-label">1. Communicates with appropriate team members regarding client care and the student nurse’s role in providing care.</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputEmail" class="col-sm-6 col-form-label">2. Assists client and family in identifying and securing appropriate and available services for health-related needs in all settings.</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputName2" class="col-sm-6 col-form-label">3. Demonstrates respect and empathy for clients (real and simulated).</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputName" class="col-sm-6 col-form-label">4. Develops collaborative partnerships with clients and families.</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputEmail" class="col-sm-6 col-form-label">5. Develops positive relationships with the interprofessional team.</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputName" class="col-sm-6 col-form-label">6. Demonstrates knowledge of conflict resolution skills and improves own skills as needed.</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputEmail" class="col-sm-6 col-form-label">7. Responds appropriately in stressful situations and demonstrates effective conflict resolution skills.</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputEmail" class="col-sm-6 col-form-label">8. Conducts oneself with all members of the health care team, peers and faculty in a civil, professional and courteous manner.</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputEmail" class="col-sm-6 col-form-label">9. Communicates clearly, concisely and accurately (using appropriate medical terminology and abbreviations) in:
                                                        <br>a. Oral form including their presentations, using SBAR/ISBARR and successfully completes the GBC Communication checklist requirements.</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputEmail" class="col-sm-6 col-form-label">b. Written form in a timely manner including successfully completing the GBC Documentation Checklist requirements.</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>

                                            <h3 class="card-title">Clinical Student Comments</h3>
                                            <p>RELATIONSHIPS INDICATORS: Specific events/evidence/examples to support rating by Instructor:</p>

                                            <label>Midterm</label>
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Midterm">
                                            </div>
                                            <label>Learning Needs and Goals:</label>
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Learning Needs and Goals">
                                            </div>
                                            <label>Final</label>
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Final">
                                            </div>
                                            <label>Learning Needs and Goals:</label>
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Learning Needs and Goals">
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="tab">
                                    <div class="card card-primary">
                                        <div class="card-header">
                                            <h3 class="card-title">LEADERSHIP INDICATORS</h3>
                                        </div>
                                        <div class="card-body">
                                            <div class="tab-pane" id="settings">
                                                <div class="form-group row">
                                                    <label for="inputName" class="col-sm-6 col-form-label"></label>
                                                    <div class="col-sm-3">
                                                        <label>Mid-Term</label>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <label>Final</label>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputName" class="col-sm-6 col-form-label">1. Role-models the professional values of the CNO and George Brown College.</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputEmail" class="col-sm-6 col-form-label">2. Advocates for clients, family, nursing profession and colleagues.</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputName2" class="col-sm-6 col-form-label">3. Provides direction and collaborates well with others.</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputName" class="col-sm-6 col-form-label">4. Provides leadership through formal and informal roles.</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputEmail" class="col-sm-6 col-form-label">5. Enhances group discussion in post-conferences.</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputName" class="col-sm-6 col-form-label">6. Demonstrates behaviours and decisions that supports mandatory attendance at clinical allowing for assessment and feedback on performance.</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <select class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">Select Performance</option>
                                                            <option>Satisfactory</option>
                                                            <option>Needs Improvement</option>
                                                            <option>Unsatisfactory</option>
                                                            <option>Not Demonstrated</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>

                                            <h3 class="card-title">Clinical Student Comments</h3>
                                            <p>LEADERSHIP INDICATORS: Specific events/evidence/examples to support rating by Instructor:</p>

                                            <label>Midterm</label>
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Midterm">
                                            </div>
                                            <label>Learning Needs and Goals:</label>
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Learning Needs and Goals">
                                            </div>
                                            <label>Final</label>
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Final">
                                            </div>
                                            <label>Learning Needs and Goals:</label>
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Learning Needs and Goals">
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="tab">
                                    <div class="card card-primary">

                                        <div class="card-body">

                                            <h3 class="card-title">Specific Areas for Improvement and Strategies for Success</h3>

                                            <label>At Midterm</label>
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Enter email">
                                            </div>
                                            <label>Learning Goals:</label>
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Enter email">
                                            </div>
                                            <div class="row">
                                                <div class="col-12 table-responsive">
                                                    <table class="table table-striped">
                                                        <tr>
                                                            <th>CLINICAL PRACTICE PERFORMANCE</th>
                                                            <th>FINAL RATING</th>
                                                            <th></th>
                                                            <th></th>
                                                        </tr>
                                                        <tr>
                                                            <td>Any indicator marked as Unacceptable On FINAL evaluation? No ____ Yes___</td>
                                                            <td>If Yes, Mark as “NP” If No, Mark as “P”</td>
                                                            <td rowspan="2">Simulation Passport = Completed (C) Not Completed (NC)</td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td>PROFESSIONAL STANDARD STATEMENT COMPETENCIES</td>
                                                            <td></td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Mandatory Indicators</td>
                                                            <td></td>
                                                            <td rowspan="3" colspan="2">Dates of clinical absences/lateness (if any)</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Accountability</td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Knowledge</td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Knowledge Application</td>
                                                            <td></td>
                                                            <td>CLINICAL ASSIGNMENTS</td>
                                                            <td>FINAL RATING</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Continuing Competence</td>
                                                            <td></td>
                                                            <td>Nursing Care Plans Average mark should be minimum 60% to pass (Average is calculated =Sum of 2 care plans, divide by 2)</td>
                                                            <td>
                                                                <select class="form-control select2" style="width: 100%;">
                                                                    <option selected="selected">Select Performance</option>
                                                                    <option>Satisfactory</option>
                                                                    <option>Needs Improvement</option>
                                                                    <option>Unsatisfactory</option>
                                                                    <option>Not Demonstrated</option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Ethics</td>
                                                            <td></td>
                                                            <td>Self-Reflective Journals Select 5 best reflective journals out of 7. (Average is calculated =Sum of 5 best journals divide by 5) Average mark should be minimum 60% to pass</td>
                                                            <td>
                                                                <select class="form-control select2" style="width: 100%;">
                                                                    <option selected="selected">Select Performance</option>
                                                                    <option>Satisfactory</option>
                                                                    <option>Needs Improvement</option>
                                                                    <option>Unsatisfactory</option>
                                                                    <option>Not Demonstrated</option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Relationships</td>
                                                            <td></td>
                                                            <td rowspan="2">Clinical Assignments Mark Average mark should be minimum 60% to pass on both Nursing Care Plans and Self-Reflective Journal</td>
                                                            <td rowspan="2">
                                                                <select class="form-control select2" style="width: 100%;">
                                                                    <option selected="selected">Select Performance</option>
                                                                    <option>Satisfactory</option>
                                                                    <option>Needs Improvement</option>
                                                                    <option>Unsatisfactory</option>
                                                                    <option>Not Demonstrated</option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Leadership</td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>

                                                            <td rowspan="3" colspan="2">The student must achieve a PASS on both key components (Clinical practice performance and Clinical assignments) in order to PASS the clinical course.</td>
                                                            <td>Final Rating: (Clinical Practice Performance and Clinical Assignments) Clinical Performance PartPass/ Non-Pass Clinical Assignments PartPass/ Non-Pass</td>
                                                            <td>Final Clinical Rating: Pass Non-Pass
                                                            </td>
                                                        </tr>

                                                    </table>

                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                    <div class="form-group row">
                                        <div class="offset-sm-12 col-sm-12">
                                            <div class="form-check">
                                                <input type="checkbox" class="form-check-input" id="exampleCheck2">
                                                <label class="form-check-label" for="exampleCheck2">"I have reviewed my self-evaluation for accuracy and am ready to submit to my instructor's review prior to the face-to-face meeting with my instructor"
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <p>This evaluation tool is used to grade the clinical practicum performance. Overall course mark will be based on evaluation of Clinical Performance and Clinical Assignments. This clinical evaluation tool is based on the College of Nurses of Ontario (CNO) Compendium of Standards of Practice for Nurses in Ontario. These Standards also include the Entry to Practice Competencies for Ontario Registered Practical Nurses (CNO, 2018) and Professional Standards, Revised 2002 (CNO, 2009) which describe the components of the nursing process. The Professional Standards, Revised 2002 (CNO, 2009) describe the competent level of behaviour expected in the professional practical nurse role, including activities related to accountability, continuing competency, knowledge, knowledge application, ethics, leadership and relationships.</p>

                                    <div style="text-align:center;margin-top:40px;">
                                        <span class="step"></span>
                                        <span class="step"></span>
                                        <span class="step"></span>
                                        <span class="step"></span>
                                        <span class="step"></span>
                                        <span class="step"></span>
                                        <span class="step"></span>
                                        <span class="step"></span>
                                        <span class="step"></span>
                                        <span class="step"></span>
                                        <span class="step"></span>
                                    </div>
                            </form>
                            </div>
                            <div style="overflow:auto;">
                                <div>
                                    <button type="button" class="btn btn-info float-left" id="prevBtn" onclick="nextPrev(-1)">Previous</button>
                                    <button type="button" class="btn btn-info float-right" id="nextBtn" onclick="nextPrev(1)">Save and Continue</button>
                                    <button type="button" class="btn btn-info float-right" id="draft" onclick="nextPrev(-1)">Save to draft</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<!-- <form role="form">
                  <div class="card-body">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Email address</label>
                      <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputPassword1">Password</label>
                      <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputFile">File input</label>
                      <div class="input-group">
                        <div class="custom-file">
                          <input type="file" class="custom-file-input" id="exampleInputFile">
                          <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                        </div>
                        <div class="input-group-append">
                          <span class="input-group-text" id="">Upload</span>
                        </div>
                      </div>
                    </div>
                    <div class="form-check">
                      <input type="checkbox" class="form-check-input" id="exampleCheck1">
                      <label class="form-check-label" for="exampleCheck1">Check me out</label>
                    </div>
                  </div> 

                  <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                  </form> 
                        </div>
                    </div>
                </div>
            </div>
        </div>