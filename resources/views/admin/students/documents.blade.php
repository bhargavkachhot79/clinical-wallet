@extends('admin.layouts.auth')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1>Students</h1>
            </div>
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="#">Home</a></li>
                  <li class="breadcrumb-item active">DataTables</li>
               </ol>
            </div>
         </div>
      </div>
      <!-- /.container-fluid -->
   </section>
   @if (session('status'))
   <div class="alert alert-success" role="alert">
      {{ session('status') }}
   </div>
   @endif
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <div class="col-12">
            <div class="card">
               <div class="card-header">
                  <h3 class="card-title">Documents</h3>
               </div>
               <!-- /.card-header -->
               <div class="card-body">
                  <div style="overflow-x:auto;">
                     <table id="example2" class="table table-bordered table-hover">
                        <thead>
                           <tr>
                              <th>No.</th>
                              <th>Profile picture</th>
                              <th>First Name</th>
                              <th>Last Name</th>
                              <th>Document</th>
                              <th>Download</th>
                              <th>Filetype</th>
                              <th>School</th> 
                              <th>Program</th>  
                              <th>Created At</th>
                              <th>Action</th>
                           </tr>
                        </thead>
                        <tbody>
                           @if(count($documents))
                           <?php $i=1; ?>                   
                           @foreach($documents as $document) 
                           <tr>
                              <td> <?php echo $i; $i++; ?> </td>
                              <td> 
                                 @if($document->user['image'] == Null)
                                    <img src="http://15.206.38.116/clinicalwallet/public/profile/images/avatar.png" id="profile-image"> 
                                 @else
                                    <img src="{{$document->user['image']}}" id="profile-image">
                                 @endif
                              </td> 
                              <td>{{$document->user['first_name']}}</td>
                              <td>{{$document->user['last_name']}}</td>
                              <td>{{$document->filename}}</td>
                              <td><a href="{{$document->document}}" target="_blank"><i style="font-size:95px;" class="fa">&#xf15c;</i></a></td>
                              <td>{{$document->filetype}}</td>
                              <td>{{$document->user['school']}}</td>
                              <td>{{$document->user['program']}}</td> 
                              <td>{{$document->created_at}}</td>
                              <td>
                                 <a href="" class="btn btn-default btn-primary btn-sm selectall"><span><i class="fas fa-edit"></i> Edit</span></a> 
                                 <a href="#delete-{{$document->id }}" data-toggle="modal" class="btn btn-default btn-primary btn-sm selectall"><span><i class="fa fa-trash" aria-hidden="true"></i> Delete</span></a>
                                 <div class="modal" id="delete-{{$document->id }}" tabindex="-1" role="dialog">
                                    <div class="modal-dialog" role="document">
                                       <div class="modal-content">
                                          <form action="{{route('delete.document',$document->id)}}" method="POST">
                                             {{ csrf_field() }}
                                             <div class="modal-header">
                                                <h4 class="modal-title">Delete Student</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                                </button>
                                             </div>
                                             <div class="modal-body">
                                                <label>Are you sure you want to delete document?</label>
                                             </div>
                                             <div class="modal-footer">
                                                <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">No</button>
                                                <input type="submit" class="btn btn-primary" value="Yes" />
                                             </div>
                                          </form>
                                       </div>
                                    </div>
                                 </div>
                              </td>
                           </tr>
                           @endforeach
                           @else 
                           <tr>
                              <td colspan="6">No Documents found</td>
                           </tr>
                           @endif
                        </tbody>
                        <tfoot>
                           <tr>
                              <th>No.</th>
                              <th>Profile picture</th>
                              <th>First Name</th>
                              <th>Last Name</th>
                              <th>Document</th>
                              <th>Download</th>
                              <th>Filetype</th>
                              <th>School</th> 
                              <th>Program</th>  
                              <th>Created At</th>
                              <th>Action</th>  
                           </tr>
                        </tfoot>
                     </table>
                  </div>
               </div>
               <!-- /.card-body -->
            </div>
         </div>
         <!-- /.col -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
@endsection