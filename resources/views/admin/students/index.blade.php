@extends('admin.layouts.auth')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1>Students</h1>
            </div>
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="#">Home</a></li>
                  <li class="breadcrumb-item active">Students</li>
               </ol>
            </div>
         </div>
      </div>
      <!-- /.container-fluid --> 
   </section>
   @if (session('status'))
   <div class="alert alert-success" role="alert">
      {{ session('status') }}
   </div>
   @endif
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <div class="col-12">
            <div class="card">
               <div class="card-header">
                  <h3 class="card-title">Total Students</h3>
               </div>
               <!-- /.card-header -->
               <div class="card-body">
                  <div style="overflow-x:auto;">
                     <table id="example2" class="table table-bordered table-hover">
                        <thead>
                           <tr>
                              <th>No.</th>
                              <th>Profile Picture</th>
                              <th>First Name</th>
                              <th>Last Name</th>
                              <th>Email</th>
                              <th>Phone</th>
                              <th>Student Id</th>
                              <th>School</th>
                              <th>Program</th>
                              <th>Zipcode</th>
                              <th>Is Approved</th>
                              <th>Created At</th>
                              <th>Action</th>
                           </tr>
                        </thead>
                        <tbody>
                           @if(count($students))
                           <?php $i=1; ?>                   
                           @foreach($students as $student) 
                           <tr>
                              <td> <?php echo $i; $i++; ?> </td>
                              <td>
                                 @if($student->image == Null)
                                    <img src="http://15.206.38.116/clinicalwallet/public/profile/images/avatar.png" id="profile-image"> 
                                 @else
                                    <img src="{{$student->image}}" id="profile-image">
                                 @endif
                              </td>
                              <td>{{$student->first_name}}</td>
                              <td>{{$student->last_name}}</td>
                              <td>{{$student->email}}</td>
                              <td>{{$student->phone}}</td>
                              <td>{{$student->student_id}}</td>
                              <td>{{$student->school}}</td>
                              <td>{{$student->program}}</td>
                              <td>{{$student->zipcode}}</td>
                              <td>
                                 @if ($student->is_approved == 0) 
                                 <button type="button" class="btn btn-danger">Verify Email</button> 
                                 @else
                                 <button type="button" class="btn btn-info">Email verified</button> 
                                 @endif
                              </td>
                              <td>{{$student->created_at}}</td>
                              <td>
                                 <a href="{{route('edit.student',$student->id )}}" class="btn btn-default btn-primary btn-sm selectall"><span><i class="fas fa-edit"></i> Edit</span></a> 
                                 <a href="#delete-{{$student->id }}" data-toggle="modal" class="btn btn-default btn-primary btn-sm selectall"><span><i class="fa fa-trash" aria-hidden="true"></i> Delete</span></a>
                                 <div class="modal" id="delete-{{$student->id }}" tabindex="-1" role="dialog">
                                    <div class="modal-dialog" role="document">
                                       <div class="modal-content">
                                          <form action="{{route('delete.student',$student->id)}}" method="POST">
                                             {{ csrf_field() }}
                                             <div class="modal-header">
                                                <h4 class="modal-title">Delete Student</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                                </button>
                                             </div>
                                             <div class="modal-body">
                                                <label>Are you sure you want to delete Student?</label>
                                             </div>
                                             <div class="modal-footer">
                                                <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">No</button>
                                                <input type="submit" class="btn btn-primary" value="Yes" />
                                             </div>
                                          </form>
                                       </div>
                                    </div>
                                 </div>
                              </td>
                           </tr>
                           @endforeach
                           @else 
                           <tr>
                              <td colspan="6">No Students found</td>
                           </tr>
                           @endif
                        </tbody>
                        <tfoot>
                           <tr>
                              <th>No.</th>
                              <th>Profile Picture</th>
                              <th>First Name</th>
                              <th>Last Name</th>
                              <th>Email</th>
                              <th>Phone</th>
                              <th>Student Id</th>
                              <th>School</th>
                              <th>Program</th>
                              <th>Zipcode</th>
                              <th>Is Approved</th>
                              <th>Created At</th>
                              <th>Action</th>
                           </tr>
                        </tfoot>
                     </table>
                  </div>
               </div>
               <!-- /.card-body -->
            </div>
         </div>
         <!-- /.col -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
@endsection