<!DOCTYPE html>
<html>
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>Clinical Wallet</title>
      <!-- Tell the browser to be responsive to screen width -->
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- Font Awesome -->
      <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">
      <!-- Ionicons -->
      <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
      <!-- Theme style -->
      <link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css') }}">
      <!-- Google Font: Source Sans Pro -->
      <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
      <link href="{{ asset('css\theme.css') }}" rel="stylesheet">
      <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 3 | Editors</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1"> 
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('user_theme/plugins/fontawesome-free/css/all.min.css') }} ">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('user_theme/dist/css/adminlte.min.css') }}">
  <!-- summernote -->
  <link rel="stylesheet" href="{{ asset('user_theme/plugins/summernote/summernote-bs4.css') }}"> 
      <!--  -->
   </head>
   <body class="hold-transition sidebar-mini layout-navbar-fixed">
      <!-- Site wrapper -->
      <div class="wrapper">
         <!-- Navbar -->
         @include('admin.layouts.header')
         <!-- /.navbar --> 
         @include('admin.layouts.sidebar')
         <!-- Main content -->
         @yield('content')
         <!-- /.content -->
         <footer class="main-footer">
            <div class="float-right d-none d-sm-block">
               <b> </b> 
            </div>
            <strong>Copyright &copy; 2020 Clinical wallet.</strong> All rights
            reserved.
         </footer>
      </div>
      <!-- /.content-wrapper -->
      <!-- Control Sidebar -->
      <aside class="control-sidebar control-sidebar-dark">
         <!-- Control sidebar content goes here -->
      </aside>
      <!-- /.control-sidebar -->
      </div>
      <!-- ./wrapper -->
      <!-- jQuery -->
      <script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
      <!-- Bootstrap 4 -->
      <script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
      <!-- AdminLTE App -->
      <script src="{{ asset('dist/js/adminlte.min.js') }}"></script>
      <!-- AdminLTE for demo purposes -->
      <script src="{{ asset('dist/js/demo.js') }}"></script>
      <!-- ./wrapper --> 
<script src="{{ asset('user_theme/plugins/jquery/jquery.min.js') }}"></script> 
<script src="{{ asset('user_theme/plugins/bootstrap/js/bootstrap.bundle.min.js') }} "></script> 
<script src="{{ asset('user_theme/dist/js/adminlte.min.js') }}"></script> 
<script src="{{ asset('user_theme/dist/js/demo.js') }}"></script> 
<script src="{{ asset('user_theme/plugins/summernote/summernote-bs4.min.js') }}"></script>
<script>
  $(function () {
    // Summernote
    $('.textarea').summernote()
  })
</script>
   </body>
</html>