<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
   <!-- Brand Logo -->
   <a href="../../index3.html" class="brand-link elevation-4">
   <img src="{{ asset('dist/img/AdminLTELogo.png') }}"
      alt="AdminLTE Logo"
      class="brand-image img-circle elevation-3"
      style="opacity: .8">
   <span class="brand-text font-weight-light">Clinical Wallet</span>
   </a>
   <!-- Sidebar -->
   <div class="sidebar">
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
         <div class="image">
            <img src="{{ asset('dist/img/user2-160x160.jpg') }}" class="img-circle elevation-2" alt="User Image">
         </div>
         <div class="info">
            <a href="#" class="d-block">Super Admin</a>
         </div>
      </div>
      <!-- Sidebar Menu -->
      <nav class="mt-2">
         <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
            <li class="nav-item has-treeview">
               <a href="{{route('dashboard')}}" class="nav-link">
                  <i class="nav-icon fas fa-tachometer-alt"></i>
                  <p>
                     Dashboard 
                  </p>
               </a>
            </li>
            <li class="nav-item has-treeview">
               <a href="#" class="nav-link">
                  <i class="nav-icon fas fa-tree"></i>
                  <p>
                     Students
                     <i class="fas fa-angle-left right"></i>
                  </p>
               </a>
               <ul class="nav nav-treeview">
                  <li class="nav-item">
                     <a href="{{route('total_students')}}" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>All Students</p>
                     </a>
                  </li>
                  <li class="nav-item">
                     <a href="{{route('student.documents')}}" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Documents</p>
                     </a>
                  </li> 
               </ul>
            </li>
            <li class="nav-item has-treeview">
               <a href="#" class="nav-link">
                  <i class="nav-icon fas fa-chart-pie"></i>
                  <p>
                     Instructors
                     <i class="right fas fa-angle-left"></i>
                  </p>
               </a>
               <ul class="nav nav-treeview">
                  <li class="nav-item">
                     <a href="{{route('total_instructors')}}" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>All Instructors</p>
                     </a>
                  </li> 
               </ul>
            </li>
            <li class="nav-item has-treeview">
               <a href="#" class="nav-link">
                  <i class="nav-icon fas fa-edit"></i>
                  <p>
                     Program Head
                     <i class="fas fa-angle-left right"></i>
                  </p>
               </a>
               <ul class="nav nav-treeview">
                  <li class="nav-item">
                     <a href="" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>General Elements</p>
                     </a>
                  </li> 
               </ul>
            </li>
            <li class="nav-item has-treeview">
               <a href="#" class="nav-link">
                  <i class="nav-icon fas fa-edit"></i>
                  <p>
                     Manage Static Pages
                     <i class="fas fa-angle-left right"></i>
                  </p>
               </a>
               <ul class="nav nav-treeview">
                  <li class="nav-item">
                     <a href="{{route('add.help')}}" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Edit Help Page</p>
                     </a>
                  </li> 
                  <li class="nav-item">
                     <a href="{{route('add.terms.of.services')}}" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Edit Terms of Services page</p>
                     </a>
                  </li>
                  <li class="nav-item">
                     <a href="{{route('add.about.us')}}" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Edit About Us Page</p>
                     </a>
                  </li>
                  <li class="nav-item">
                     <a href="{{route('add.privacy.policy')}}" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Edit Privacy Policy Page</p>
                     </a>
                  </li>

               </ul>
            </li>
            <li class="nav-item has-treeview">
               <a href="{{ route('logout') }}" class="nav-link" onclick="event.preventDefault();
                  document.getElementById('logout-form').submit();">
                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                     @csrf
                  </form>
                  <i class="nav-icon fas fa-power-off"></i>
                  <p>
                     Logout 
                  </p>
               </a>
            </li>
         </ul>
      </nav>
      <!-- /.sidebar-menu -->
   </div>
   <!-- /.sidebar -->
</aside>