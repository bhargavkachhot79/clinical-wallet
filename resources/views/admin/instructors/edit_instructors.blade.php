@extends('admin.layouts.auth')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1>Instructor Profile</h1>
            </div>
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="#">Home</a></li>
                  <li class="breadcrumb-item active">Instructor Profile</li>
               </ol>
            </div>
         </div>
      </div>
      <!-- /.container-fluid -->
   </section>
   @if (session('status'))
   <div class="alert alert-success" role="alert">
      {{ session('status') }}
   </div>
   @endif
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
         <section class="content">
            <div class="row">
               <div class="col-12">
                  <div class="card">
                     <div class="card card-primary">
                        <div class="card-header">
                           <h3 class="card-title">Edit Instructor profile</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form role="form" action="{{route('update.instructor',$instructor->id)}}" method="POST" enctype="multipart/form-data">
                           {{ csrf_field() }}
                           <div class="row">
                              <div class="col-sm-6">
                                 <div class="card-body">
                                    <div class="form-group">
                                       <label for="exampleInputEmail1">First Name</label>
                                       <input type="text" name="first_name" class="form-control" id="exampleInputEmail1" placeholder="Enter First Name" value="{{$instructor->first_name}}">
                                       @if ($errors->has('first_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                @endif
                                    </div>
                                    <div class="form-group">
                                       <label for="exampleInputEmail1">Email</label>
                                       <input type="email" name="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email" value="{{$instructor->email}}" disabled="disabled">
                                    </div>
                                    <div class="form-group">
                                       <label for="exampleInputEmail1">ZipCode</label>
                                       <input type="text" name="zipcode" class="form-control" value="{{$instructor->zipcode}}" id="exampleInputEmail1" placeholder="Enter ZipCode">
                                       @if ($errors->has('zipcode'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('zipcode') }}</strong>
                                    </span>
                                @endif
                                    </div>
                                    <div class="form-group">
                                       <label for="exampleInputEmail1">School</label>
                                       <input type="text" name="school" class="form-control" value="{{$instructor->school}}" id="exampleInputEmail1" placeholder="Enter School">
                                       @if ($errors->has('school'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('school') }}</strong>
                                    </span>
                                @endif
                                    </div>
                                    <div class="form-group"> 
                                       @if($instructor->image == Null)
                                          <img src="http://15.206.38.116/clinicalwallet/public/profile/images/avatar.png" class="form-control" style="width: 150px; height: 150px">
                                       @else 
                                             <img src="{{$instructor->image}}" class="form-control" style="width: 150px; height: 150px">
                                          @endif   
                                    </div>
                                 </div>
                              </div>
                              <div class="col-sm-6">
                                 <div class="card-body">
                                    <div class="form-group">
                                       <label for="exampleInputEmail1">Last Name</label>
                                       <input type="text" name="last_name" class="form-control" value="{{$instructor->last_name}}" id="exampleInputEmail1" placeholder="Enter Last Name">
                                       @if ($errors->has('last_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                @endif
                                    </div>
                                    <div class="form-group">
                                       <label for="exampleInputEmail1">Phone</label>
                                       <input type="text" name="phone" class="form-control" value="{{$instructor->phone}}" id="exampleInputEmail1" placeholder="Enter phone">
                                       @if ($errors->has('phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                                    </div>
                                    <div class="form-group">
                                       <label for="exampleInputEmail1">Program</label>
                                       <input type="text" name="program" class="form-control" value="{{$instructor->program}}" id="exampleInputEmail1" placeholder="Enter Program">
                                       @if ($errors->has('program'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('program') }}</strong>
                                    </span>
                                @endif
                                    </div>
                                    <div class="form-group">
                                       <label for="exampleInputEmail1">Student Id</label>
                                       <input type="text" name="student_id" class="form-control" value="{{$instructor->student_id}}" id="exampleInputEmail1" placeholder="Enter Student Id">
                                       @if ($errors->has('student_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('student_id') }}</strong>
                                    </span>
                                @endif
                                    </div>
                                    <div class="form-group">
                                       <label for="exampleInputFile">Profile Picture</label>
                                       <div class="input-group"> 
                                             <input type="file" name="image" class="custom-file-input" id="exampleInputFile">
                                             <label class="custom-file-label" for="exampleInputFile">Choose file</label> 
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="card-footer">
                              <button type="submit" class="btn btn-primary left">Cancle</button>
                              <input type="submit" class="btn btn-primary right">
                           </div>
                        </form>
                     </div> 
                  </div>
               </div>
               <!-- /.col -->
            </div>
            <!-- /.row -->
         </section>
         <!-- /.content -->
      </div>
   </section>
</div>
@endsection