@extends('admin.layouts.auth')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1>Instructors</h1>
            </div>
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="#">Home</a></li>
                  <li class="breadcrumb-item active">Instructors</li>
               </ol>
            </div>
         </div>
      </div>
      <!-- /.container-fluid -->
   </section>
   @if (session('status'))
   <div class="alert alert-success" role="alert">
      {{ session('status') }}
   </div>
   @endif
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <div class="col-12">
            <div class="card">
               <div class="card-header">
                  <h3 class="card-title">Total Instructors</h3>
               </div>
               <!-- /.card-header -->
               <div class="card-body">
                  <div style="overflow-x:auto;">
                     <table id="example2" class="table table-bordered table-hover">
                        <thead>
                           <tr>
                              <th>No.</th>
                              <th>Profile Picture</th>
                              <th>First Name</th>
                              <th>Last Name</th>
                              <th>Email</th>
                              <th>Phone</th>
                              <th>Student Id</th>
                              <th>School</th>
                              <th>Program</th>
                              <th>Zipcode</th>
                              <th>Is Approved</th>
                              <th>Created At</th>
                              <th>Action</th>
                           </tr>
                        </thead>
                        <tbody>
                           @if(count($instructors))
                           <?php $i=1; ?>                   
                           @foreach($instructors as $instructor) 
                           <tr>
                              <td> <?php echo $i; $i++; ?> </td>
                              <td>
                                 @if($instructor->image == Null)
                                    <img src="http://15.206.38.116/clinicalwallet/public/profile/images/avatar.png" id="profile-image"> 
                                 @else
                                    <img src="{{$instructor->image}}" id="profile-image">
                                 @endif
                              </td>
                              <td>{{$instructor->first_name}}</td>
                              <td>{{$instructor->last_name}}</td>
                              <td>{{$instructor->email}}</td>
                              <td>{{$instructor->phone}}</td>
                              <td>{{$instructor->student_id}}</td>
                              <td>{{$instructor->school}}</td>
                              <td>{{$instructor->program}}</td>
                              <td>{{$instructor->zipcode}}</td>
                              <td>
                                 @if ($instructor->is_approved == 0) 
                                 <button type="button" class="btn btn-danger">Verify Email</button> 
                                 @else
                                 <button type="button" class="btn btn-info">Email verified</button> 
                                 @endif
                              </td>
                              <td>{{$instructor->created_at}}</td>
                              <td>
                                 <a href="{{route('edit.instructor',$instructor->id )}}" class="btn btn-default btn-primary btn-sm selectall"><span><i class="fas fa-edit"></i> Edit</span></a> 
                                 <a href="#delete-{{$instructor->id }}" data-toggle="modal" class="btn btn-default btn-primary btn-sm selectall"><span><i class="fa fa-trash" aria-hidden="true"></i> Delete</span></a>
                                 <div class="modal" id="delete-{{$instructor->id }}" tabindex="-1" role="dialog">
                                    <div class="modal-dialog" role="document">
                                       <div class="modal-content">
                                          <form action="{{route('delete.instructor',$instructor->id)}}" method="POST">
                                             {{ csrf_field() }}
                                             <div class="modal-header">
                                                <h4 class="modal-title">Delete Instructor</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                                </button>
                                             </div>
                                             <div class="modal-body">
                                                <label>Are you sure you want to delete Instructor?</label>
                                             </div>
                                             <div class="modal-footer">
                                                <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">No</button>
                                                <input type="submit" class="btn btn-primary" value="Yes" />
                                             </div>
                                          </form>
                                       </div>
                                    </div>
                                 </div>
                              </td>
                           </tr>
                           @endforeach
                           @else 
                           <tr>
                              <td colspan="6">No Instructors found</td>
                           </tr>
                           @endif
                        </tbody>
                        <tfoot>
                           <tr>
                              <th>No.</th>
                              <th>Profile Picture</th>
                              <th>First Name</th>
                              <th>Last Name</th>
                              <th>Email</th>
                              <th>Phone</th>
                              <th>Student Id</th>
                              <th>School</th>
                              <th>Program</th>
                              <th>Zipcode</th>
                              <th>Is Approved</th>
                              <th>Created At</th>
                              <th>Action</th>
                           </tr>
                        </tfoot>
                     </table>
                  </div>
               </div>
               <!-- /.card-body -->
            </div>
         </div>
         <!-- /.col -->
      </div>
      <!-- /.row -->
   </section>
   <!-- /.content -->
</div>
@endsection