<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<div class="form-gap"></div>
<form id="myForm" method="POST" action="{{route('reset')}}">
   <div class="container">
      <div class="row">
         <div class="col-md-4 col-md-offset-4">
            <div class="panel panel-default">
               <div class="panel-body">
                  <div class="text-center">
                     <h3><i class="fa fa-lock fa-4x"></i></h3>
                     <h2 class="text-center">Change Password</h2>
                     <p>You can reset your password here.</p>
                     <div class="panel-body">
                        <div class="form-group">
                           <div class="input-group">
                              <span class="input-group-addon"><i class="glyphicon glyphicon-lock color-blue"></i></span>
                              <input type="password" id="password" name="password" placeholder="Password" class="form-control">
                           </div>
                        </div>
                        <div class="input-group">
                           <span class="input-group-addon"><i class="glyphicon glyphicon-lock color-blue"></i></span>
                           <input type="password" id="confirm_password" name="confirm_password" placeholder="Confirm Password" class="form-control">
                        </div>
                     </div>
                     <div class="form-group">
                        <input id="submit" name="submit" class="btn btn-lg btn-primary btn-block" value="Change Password" type="submit" disabled>
                     </div>
                     <input type="hidden" class="hide" name="user_id" id="user_id" value="{{$token}}"> 
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   </div>
</form>
<style type="text/css">
   .form-gap {
   padding-top: 70px;
   }
</style>
<script src="http://code.jquery.com/jquery-2.1.0.min.js"></script>
<style>
   #form label{float:left; width:140px;}
   #error_msg{color:red; font-weight:bold;}
</style>
<script>
   $(document).ready(function(){
       var $submitBtn = $("#submit");
       var $passwordBox = $("#password");
       var $confirmBox = $("#confirm_password");
       var $errorMsg =  $('<span id="error_msg">Passwords do not match.</span>'); 
       // This is incase the user hits refresh - some browsers will maintain the disabled state of the button.
       $submitBtn.removeAttr("disabled"); 
       function checkMatchingPasswords(){
           if($confirmBox.val() != "" && $passwordBox.val != ""){
               if( $confirmBox.val() != $passwordBox.val() ){
                   $submitBtn.attr("disabled", "disabled");
                   $errorMsg.insertAfter($confirmBox);
               }
           }
       } 
       function resetPasswordError(){
           $submitBtn.removeAttr("disabled");
           var $errorCont = $("#error_msg");
           if($errorCont.length > 0){
               $errorCont.remove();
           }  
       } 
       $("#confirm_password, #password")
            .on("submit", function(e){
               /* only check when the tab or enter keys are pressed
                * to prevent the method from being called needlessly  */
               if(e.keyCode == 13 || e.keyCode == 9) {
                   checkMatchingPasswords();
               }
            })
            .on("blur", function(){                    
               // also check when the element looses focus (clicks somewhere else)
               checkMatchingPasswords();
           })
           .on("focus", function(){
               // reset the error message when they go to make a change
               resetPasswordError();
           })
   
   });
</script>