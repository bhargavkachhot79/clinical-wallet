-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 18, 2020 at 01:52 PM
-- Server version: 5.7.29-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `clinicalwallet`
--

-- --------------------------------------------------------

--
-- Table structure for table `about_us`
--

CREATE TABLE `about_us` (
  `id` int(255) NOT NULL,
  `text` text,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `about_us`
--

INSERT INTO `about_us` (`id`, `text`, `updated_at`, `created_at`, `deleted_at`) VALUES
(1, '<p><br></p><h3 style=\"margin: 15px 0px; padding: 0px; font-weight: 700; font-size: 14px; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif;\">Section 1.10.32 of \"de Finibus Bonorum et Malorum\", written by Cicero in 45 BC</h3><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px;\">\"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?\"</p><h3 style=\"margin: 15px 0px; padding: 0px; font-weight: 700; font-size: 14px; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif;\">1914 translation by H. Rackham</h3><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px;\">\"But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?\"</p><h3 style=\"margin: 15px 0px; padding: 0px; font-weight: 700; font-size: 14px; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif;\">Section 1.10.33 of \"de Finibus Bonorum et Malorum\", written by Cicero in 45 BC</h3><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px;\">\"At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.\"</p><h3 style=\"margin: 15px 0px; padding: 0px; font-weight: 700; font-size: 14px; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif;\">1914 translation by H. Rackham</h3><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px;\">\"On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammelled and when nothing prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided. But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pains.\"</p>', '2020-03-18 11:17:21', NULL, '2020-03-17 13:42:22');

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_super` tinyint(1) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `password`, `is_super`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'test', 'test@gmail.com', '$2y$10$12p0mr44Kgn33jell5ufdunpvcMPVlZucNGpANRUEcim/QELT/hb2', 0, NULL, '2020-02-26 00:39:28', '2020-02-26 00:39:28'),
(2, 'test', 'bhargavkachhot.rs@gmail.com', '$2y$10$rQzByoZYB1/Al9tJ7YxhMexrGbd2DclXeuEU.hfhxfUrBmpIyDu3S', 0, NULL, '2020-02-26 00:47:55', '2020-02-26 00:47:55'),
(4, 'bhargav', 'tessdsdt@gmail.com', '$2y$10$fa/LN4P8savLf5CdQMqodOzhveJyBOi427D9ecAVQew0pmfmHXuTy', 0, NULL, '2020-02-27 06:19:18', '2020-02-27 06:19:18'),
(5, 'test', 'test@gmail.comdffsdfdf', '$2y$10$am7hqwDXfxao7fVmPpAMVeH5Izyt2dbyxznkQBwxbsuD4b/gt9AkG', 0, NULL, '2020-02-27 07:32:33', '2020-02-27 07:32:33'),
(6, 'test', 'test@gmail.comasa', '$2y$10$RjN0SN1rn5H16H0ma1NCmeM1lRGYsUAb.h942dGHfumbLVEVvft.C', 0, NULL, '2020-02-27 07:41:47', '2020-02-27 07:41:47'),
(7, 'test', 'test@gmail.comsdsdsdsd', '$2y$10$BEtcq2DwpaI4U8hNRXQOFu39n4yc4CGDdPu9XFxvt621PY1ixyFta', 0, NULL, '2020-02-27 07:43:14', '2020-02-27 07:43:14');

-- --------------------------------------------------------

--
-- Table structure for table `contact_us`
--

CREATE TABLE `contact_us` (
  `id` int(11) NOT NULL,
  `user_id` int(255) NOT NULL,
  `comments` text NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `contact_us`
--

INSERT INTO `contact_us` (`id`, `user_id`, `comments`, `image`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 2, 'hello there.', 'http://15.206.38.116/clinicalwallet/public/contact_us/1584512628.png', '2020-03-18 00:53:48', '2020-03-18 00:53:48', '2020-03-18 06:23:48'),
(2, 2, 'hello there.', 'http://15.206.38.116/clinicalwallet/public/contact_us/1584512886.png', '2020-03-18 00:58:06', '2020-03-18 00:58:06', '2020-03-18 06:28:06'),
(3, 2, 'hello there.', 'http://15.206.38.116/clinicalwallet/public/contact_us/1584512893.png', '2020-03-18 00:58:13', '2020-03-18 00:58:13', '2020-03-18 06:28:13'),
(4, 2, 'hello there.', 'http://15.206.38.116/clinicalwallet/public/contact_us/1584513136.png', '2020-03-18 01:02:16', '2020-03-18 01:02:16', '2020-03-18 06:32:16'),
(5, 52, 'hello there.', 'http://15.206.38.116/clinicalwallet/public/contact_us/1584513151.png', '2020-03-18 01:02:31', '2020-03-18 01:02:31', '2020-03-18 06:32:31'),
(6, 52, 'hello there.', 'http://15.206.38.116/clinicalwallet/public/contact_us/1584513197.png', '2020-03-18 01:03:17', '2020-03-18 01:03:17', '2020-03-18 06:33:17'),
(7, 52, 'hello there.', 'http://15.206.38.116/clinicalwallet/public/contact_us/1584513325.png', '2020-03-18 01:05:25', '2020-03-18 01:05:25', '2020-03-18 06:35:25'),
(8, 52, 'hello there.', 'http://15.206.38.116/clinicalwallet/public/contact_us/1584513484.png', '2020-03-18 01:08:04', '2020-03-18 01:08:04', '2020-03-18 06:38:04'),
(9, 52, 'hello there.', 'http://15.206.38.116/clinicalwallet/public/contact_us/1584513658.png', '2020-03-18 01:10:58', '2020-03-18 01:10:58', '2020-03-18 06:40:58'),
(10, 52, 'hello there.', 'http://15.206.38.116/clinicalwallet/public/contact_us/1584513691.png', '2020-03-18 01:11:31', '2020-03-18 01:11:31', '2020-03-18 06:41:31'),
(11, 52, 'hello there.', 'http://15.206.38.116/clinicalwallet/public/contact_us/1584513701.png', '2020-03-18 01:11:41', '2020-03-18 01:11:41', '2020-03-18 06:41:41'),
(12, 52, 'hello there.', 'http://15.206.38.116/clinicalwallet/public/contact_us/1584513838.png', '2020-03-18 01:13:58', '2020-03-18 01:13:58', '2020-03-18 06:43:58'),
(13, 52, 'hello there.', 'http://15.206.38.116/clinicalwallet/public/contact_us/1584513965.png', '2020-03-18 01:16:05', '2020-03-18 01:16:05', '2020-03-18 06:46:05'),
(14, 52, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'http://15.206.38.116/clinicalwallet/public/contact_us/1584514595.png', '2020-03-18 06:56:35', '2020-03-18 06:56:35', '2020-03-18 06:56:35'),
(15, 52, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'http://15.206.38.116/clinicalwallet/public/contact_us/1584514644.png', '2020-03-18 06:57:24', '2020-03-18 06:57:24', '2020-03-18 06:57:24'),
(16, 52, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'http://15.206.38.116/clinicalwallet/public/contact_us/1584514839.png', '2020-03-18 07:00:39', '2020-03-18 07:00:39', '2020-03-18 07:00:39'),
(17, 52, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'http://15.206.38.116/clinicalwallet/public/contact_us/1584514907.png', '2020-03-18 07:01:47', '2020-03-18 07:01:47', '2020-03-18 07:01:47'),
(18, 52, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'http://15.206.38.116/clinicalwallet/public/contact_us/1584514951.png', '2020-03-18 07:02:31', '2020-03-18 07:02:31', '2020-03-18 07:02:31'),
(19, 52, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'http://15.206.38.116/clinicalwallet/public/contact_us/1584514960.png', '2020-03-18 07:02:40', '2020-03-18 07:02:40', '2020-03-18 07:02:40'),
(20, 52, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'http://15.206.38.116/clinicalwallet/public/contact_us/1584514972.png', '2020-03-18 07:02:52', '2020-03-18 07:02:52', '2020-03-18 07:02:52'),
(21, 52, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'http://15.206.38.116/clinicalwallet/public/contact_us/1584515008.png', '2020-03-18 07:03:28', '2020-03-18 07:03:28', '2020-03-18 07:03:28'),
(22, 2, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'http://15.206.38.116/clinicalwallet/public/contact_us/1584515055.png', '2020-03-18 07:04:15', '2020-03-18 07:04:15', '2020-03-18 07:04:15'),
(23, 2, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'http://15.206.38.116/clinicalwallet/public/contact_us/1584515068.png', '2020-03-18 07:04:28', '2020-03-18 07:04:28', '2020-03-18 07:04:28'),
(24, 2, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'http://15.206.38.116/clinicalwallet/public/contact_us/1584515248.png', '2020-03-18 07:07:28', '2020-03-18 07:07:28', '2020-03-18 07:07:28'),
(25, 2, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'http://15.206.38.116/clinicalwallet/public/contact_us/1584515266.png', '2020-03-18 07:07:46', '2020-03-18 07:07:46', '2020-03-18 07:07:46'),
(26, 2, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'http://15.206.38.116/clinicalwallet/public/contact_us/1584515325.png', '2020-03-18 07:08:45', '2020-03-18 07:08:45', '2020-03-18 07:08:45'),
(27, 2, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', NULL, '2020-03-18 07:09:44', '2020-03-18 07:09:44', '2020-03-18 07:09:44'),
(28, 2, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', NULL, '2020-03-18 07:11:20', '2020-03-18 07:11:20', '2020-03-18 07:11:20'),
(29, 2, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', NULL, '2020-03-18 07:12:37', '2020-03-18 07:12:37', '2020-03-18 07:12:37'),
(30, 2, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', NULL, '2020-03-18 07:13:03', '2020-03-18 07:13:03', '2020-03-18 07:13:03'),
(31, 2, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', NULL, '2020-03-18 07:13:27', '2020-03-18 07:13:27', '2020-03-18 07:13:27'),
(32, 2, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'http://15.206.38.116/clinicalwallet/public/contact_us/1584515622.png', '2020-03-18 07:13:42', '2020-03-18 07:13:42', '2020-03-18 07:13:42'),
(33, 2, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'http://15.206.38.116/clinicalwallet/public/contact_us/1584515668.png', '2020-03-18 07:14:28', '2020-03-18 07:14:28', '2020-03-18 07:14:28'),
(34, 1, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', NULL, '2020-03-18 12:17:21', '2020-03-18 12:17:21', '2020-03-18 12:17:21'),
(35, 1, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', NULL, '2020-03-18 12:23:35', '2020-03-18 12:23:35', '2020-03-18 12:23:35'),
(36, 1, 'dsfsdf dsf dsf dsf', NULL, '2020-03-18 12:29:21', '2020-03-18 12:29:21', '2020-03-18 12:29:21'),
(37, 1, 'asdasdasdasd asd asd', 'http://15.206.38.116/clinicalwallet/public/contact_us/1584534757.jpeg', '2020-03-18 12:32:37', '2020-03-18 12:32:37', '2020-03-18 12:32:37');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `folders`
--

CREATE TABLE `folders` (
  `id` int(255) NOT NULL,
  `user_id` int(255) NOT NULL,
  `parent_folder_id` int(255) DEFAULT NULL,
  `folder_name` varchar(255) DEFAULT NULL,
  `filetype` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `folders`
--

INSERT INTO `folders` (`id`, `user_id`, `parent_folder_id`, `folder_name`, `filetype`, `created_at`, `updated_at`) VALUES
(1, 1, 0, '1', NULL, '2020-03-14 05:17:17', '2020-03-14 05:17:17'),
(2, 78, 1, 'Monang', 'folder', '2020-03-14 05:19:48', '2020-03-14 05:19:48'),
(8, 1, 5, 'Test 3', 'folder', '2020-03-14 05:23:45', '2020-03-14 05:23:45'),
(9, 1, 5, 'Test 2', 'folder', '2020-03-14 05:23:51', '2020-03-14 05:23:51'),
(10, 1, 5, 'Test 1', 'folder', '2020-03-14 05:23:53', '2020-03-14 05:23:53'),
(11, 1, 10, 'Test 1', 'folder', '2020-03-14 05:24:18', '2020-03-14 05:24:18'),
(12, 1, 10, 'Test 11', 'folder', '2020-03-14 05:24:25', '2020-03-14 05:24:25'),
(13, 1, 10, 'Test 12', 'folder', '2020-03-14 05:24:27', '2020-03-14 05:24:27'),
(14, 1, 10, 'Test 13', 'folder', '2020-03-14 05:24:28', '2020-03-14 05:24:28'),
(15, 2, 0, '2', NULL, '2020-03-14 05:26:46', '2020-03-14 05:26:46'),
(27, 1, 20, 'Test 1 - 11', 'folder', '2020-03-14 06:13:12', '2020-03-14 06:13:12'),
(29, 1, 20, 'Test 1 - 12', 'folder', '2020-03-14 06:14:19', '2020-03-14 06:14:19'),
(31, 1, 20, 'Test 1 - 13', 'folder', '2020-03-14 06:16:13', '2020-03-14 06:16:13'),
(32, 1, 27, 'Test 11 - 21', 'folder', '2020-03-14 06:16:35', '2020-03-14 06:16:35'),
(33, 1, 27, 'Test 11 - 22', 'folder', '2020-03-14 06:16:44', '2020-03-14 06:16:44'),
(34, 1, 27, 'Test 11 - 23', 'folder', '2020-03-14 06:16:51', '2020-03-14 06:16:51'),
(35, 1, 29, 'Test 12 - 21', 'folder', '2020-03-14 06:17:10', '2020-03-14 06:17:10'),
(36, 1, 29, 'Test 12 - 22', 'folder', '2020-03-14 06:17:16', '2020-03-14 06:17:16'),
(37, 1, 29, 'Test 12 - 23', 'folder', '2020-03-14 06:17:22', '2020-03-14 06:17:22'),
(38, 1, 31, 'Test 13 - 21', 'folder', '2020-03-14 06:17:37', '2020-03-14 06:17:37'),
(39, 1, 31, 'Test 13 - 22', 'folder', '2020-03-14 06:17:48', '2020-03-14 06:17:48'),
(40, 1, 31, 'Test 13 - 23', 'folder', '2020-03-14 06:17:54', '2020-03-14 06:17:54'),
(41, 1, 21, 'Test 2 - 11', 'folder', '2020-03-14 06:18:10', '2020-03-14 06:18:10'),
(42, 1, 21, 'Test 2 - 12', 'folder', '2020-03-14 06:18:18', '2020-03-14 06:18:18'),
(44, 1, 21, 'Test 2 - 13', 'folder', '2020-03-14 06:18:27', '2020-03-14 06:18:27'),
(45, 1, 22, 'Test 3 - 11', 'folder', '2020-03-14 06:20:20', '2020-03-14 06:20:20'),
(46, 1, 22, 'Test 3 - 12', 'folder', '2020-03-14 06:20:27', '2020-03-14 06:20:27'),
(47, 1, 22, 'Test 3 - 13', 'folder', '2020-03-14 06:20:34', '2020-03-14 06:20:34'),
(48, 1, 45, 'Test 11 - 21', 'folder', '2020-03-14 06:20:50', '2020-03-14 06:20:50'),
(49, 1, 45, 'Test 11 - 22', 'folder', '2020-03-14 06:20:57', '2020-03-14 06:20:57'),
(50, 1, 45, 'Test 11 - 23', 'folder', '2020-03-14 06:21:02', '2020-03-14 06:21:02'),
(51, 1, 34, 'hdhdhdhd', 'folder', '2020-03-14 06:29:53', '2020-03-14 06:29:53'),
(52, 1, 51, 'hdhdhdhd', 'folder', '2020-03-14 06:30:03', '2020-03-14 06:30:03'),
(53, 5, 0, '5', NULL, '2020-03-14 06:40:00', '2020-03-14 06:40:00'),
(54, 5, 5, 'Test 1', 'folder', '2020-03-14 06:46:12', '2020-03-14 06:46:12'),
(56, 78, 1, 'bhargav', 'folder', '2020-03-14 06:48:09', '2020-03-14 06:48:09'),
(57, 60, 60, 'F1', 'folder', '2020-03-14 06:52:19', '2020-03-14 06:52:19'),
(58, 60, 60, 'F2', 'folder', '2020-03-14 06:52:29', '2020-03-14 06:52:29'),
(59, 60, 60, 'F1- 1', 'folder', '2020-03-14 07:26:31', '2020-03-14 07:26:31'),
(60, 60, 59, 'F1-2', 'folder', '2020-03-14 07:27:44', '2020-03-14 07:27:44'),
(82, 6, 0, '6', NULL, '2020-03-14 09:19:15', '2020-03-14 09:19:15'),
(83, 78, 1, 'bhargav', 'folder', '2020-03-14 09:20:30', '2020-03-14 09:20:30'),
(84, 78, 81, 'test-11', 'folder', '2020-03-14 09:38:56', '2020-03-14 09:38:56'),
(85, 78, 81, 'test-12', 'folder', '2020-03-14 09:40:39', '2020-03-14 09:40:39'),
(86, 4, 81, 'test - 21', 'folder', '2020-03-14 09:41:56', '2020-03-14 09:41:56'),
(99, 4, 86, 'test - 22', 'folder', '2020-03-14 10:08:51', '2020-03-14 10:08:51'),
(100, 78, 4, 'New - 1', 'folder', '2020-03-14 10:10:20', '2020-03-14 10:10:20'),
(103, 78, 1, 'bhargav', 'folder', '2020-03-14 10:16:08', '2020-03-14 10:16:08'),
(104, 4, 101, 'New - 11', 'folder', '2020-03-14 10:17:32', '2020-03-14 10:17:32'),
(107, 4, 105, 'New - 22', 'folder', '2020-03-14 10:24:10', '2020-03-14 10:24:10'),
(125, 4, 119, 'F4 1', 'folder', '2020-03-14 11:44:27', '2020-03-14 11:44:27'),
(134, 4, 125, 'F4 2', 'folder', '2020-03-14 11:51:19', '2020-03-14 11:51:19'),
(135, 4, 125, 'F4 2', 'folder', '2020-03-14 11:51:52', '2020-03-14 11:51:52'),
(136, 4, 125, 'F4 2', 'folder', '2020-03-14 11:58:32', '2020-03-14 11:58:32'),
(137, 4, 119, 'F3 1', 'folder', '2020-03-14 11:59:13', '2020-03-14 11:59:13'),
(138, 4, 119, 'F3 2', 'folder', '2020-03-14 12:00:17', '2020-03-14 12:00:17'),
(143, 4, 142, 'F8 1', 'folder', '2020-03-14 12:20:50', '2020-03-14 12:20:50'),
(149, 4, 142, 'F9 1', 'folder', '2020-03-14 12:25:22', '2020-03-14 12:25:22'),
(151, 1, 150, 'Mac Champs1', 'folder', '2020-03-14 12:25:59', '2020-03-14 12:25:59'),
(153, 1, 151, 'Mac Champs2', 'folder', '2020-03-14 12:26:20', '2020-03-14 12:26:20'),
(157, 4, 148, 'F10 1', 'folder', '2020-03-14 12:27:20', '2020-03-14 12:27:20'),
(158, 4, 156, 'F9 1', 'folder', '2020-03-14 12:27:41', '2020-03-14 12:27:41'),
(164, 4, 158, 'F9 11', 'folder', '2020-03-14 12:30:03', '2020-03-14 12:30:03'),
(167, 1, 166, 't222', 'folder', '2020-03-14 12:31:30', '2020-03-14 12:31:30'),
(168, 1, 167, 't2222', 'folder', '2020-03-14 12:31:47', '2020-03-14 12:31:47'),
(169, 1, 168, 't22222', 'folder', '2020-03-14 12:32:05', '2020-03-14 12:32:05'),
(172, 1, 171, 't11', 'folder', '2020-03-14 12:36:46', '2020-03-14 12:36:46'),
(173, 1, 172, 't111', 'folder', '2020-03-14 12:37:05', '2020-03-14 12:37:05'),
(174, 1, 173, 't1111', 'folder', '2020-03-14 12:37:13', '2020-03-14 12:37:13'),
(175, 4, 170, 'F11 1', 'folder', '2020-03-14 12:39:36', '2020-03-14 12:39:36'),
(176, 4, 175, 'F11 2', 'folder', '2020-03-14 12:39:45', '2020-03-14 12:39:45'),
(177, 4, 175, 'F11 3', 'folder', '2020-03-14 12:40:24', '2020-03-14 12:40:24'),
(178, 4, 177, 'F11 4', 'folder', '2020-03-14 12:40:34', '2020-03-14 12:40:34'),
(179, 4, 178, 'F11 5', 'folder', '2020-03-14 12:40:53', '2020-03-14 12:40:53'),
(181, 1, 180, 't2', 'folder', '2020-03-14 12:41:44', '2020-03-14 12:41:44'),
(183, 4, 170, 'F12 1', 'folder', '2020-03-14 12:41:56', '2020-03-14 12:41:56'),
(184, 1, 181, 't3', 'folder', '2020-03-14 12:41:58', '2020-03-14 12:41:58'),
(186, 4, 182, 'F13 1', 'folder', '2020-03-14 12:44:32', '2020-03-14 12:44:32'),
(189, 1, 188, 't2', 'folder', '2020-03-14 12:46:55', '2020-03-14 12:46:55'),
(190, 1, 189, 't3', 'folder', '2020-03-14 12:47:08', '2020-03-14 12:47:08'),
(191, 1, 190, 't4', 'folder', '2020-03-14 12:47:22', '2020-03-14 12:47:22'),
(194, 4, 192, 'F16 1', 'folder', '2020-03-14 12:50:26', '2020-03-14 12:50:26'),
(196, 4, 195, 'F17 1', 'folder', '2020-03-14 12:52:24', '2020-03-14 12:52:24'),
(197, 4, 196, 'F17  1 1', 'folder', '2020-03-14 12:52:57', '2020-03-14 12:52:57'),
(198, 4, 197, 'F17 111', 'folder', '2020-03-14 12:53:16', '2020-03-14 12:53:16'),
(199, 4, 196, 'F17 2', 'folder', '2020-03-14 12:54:03', '2020-03-14 12:54:03'),
(201, 4, 200, 'F18 1', 'folder', '2020-03-14 12:58:13', '2020-03-14 12:58:13'),
(202, 4, 201, 'F18 2', 'folder', '2020-03-14 12:58:43', '2020-03-14 12:58:43'),
(204, 4, 203, 'F20 1', 'folder', '2020-03-14 12:59:36', '2020-03-14 12:59:36'),
(205, 4, 204, 'F20 11', 'folder', '2020-03-14 12:59:49', '2020-03-14 12:59:49'),
(206, 4, 205, 'F20 111', 'folder', '2020-03-14 13:00:00', '2020-03-14 13:00:00'),
(207, 4, 206, 'F20 1111', 'folder', '2020-03-14 13:00:12', '2020-03-14 13:00:12'),
(209, 4, 208, 'F21 1', 'folder', '2020-03-14 13:01:07', '2020-03-14 13:01:07'),
(210, 4, 209, 'F20 11', 'folder', '2020-03-14 13:01:15', '2020-03-14 13:01:15'),
(211, 4, 210, 'F20 111', 'folder', '2020-03-14 13:01:23', '2020-03-14 13:01:23'),
(212, 4, 211, 'F20 112', 'folder', '2020-03-14 13:01:37', '2020-03-14 13:01:37'),
(213, 4, 212, 'Vvv', 'folder', '2020-03-14 13:02:08', '2020-03-14 13:02:08'),
(214, 4, 213, 'Vvvv', 'folder', '2020-03-14 13:02:33', '2020-03-14 13:02:33'),
(218, 4, 209, 'Fbbb', 'folder', '2020-03-14 13:05:46', '2020-03-14 13:05:46'),
(222, 4, 218, 'Hbv', 'folder', '2020-03-14 13:06:25', '2020-03-14 13:06:25'),
(223, 4, 222, 'Vvv', 'folder', '2020-03-14 13:06:43', '2020-03-14 13:06:43'),
(226, 4, 225, 'F22 1', 'folder', '2020-03-14 13:11:17', '2020-03-14 13:11:17'),
(227, 4, 226, 'F22 2', 'folder', '2020-03-14 13:11:23', '2020-03-14 13:11:23'),
(229, 4, 226, 'Tvtv', 'folder', '2020-03-14 13:14:39', '2020-03-14 13:14:39'),
(231, 4, 229, 'Vvv', 'folder', '2020-03-14 13:15:38', '2020-03-14 13:15:38'),
(233, 4, 232, 'Abc 1', 'folder', '2020-03-14 13:16:47', '2020-03-14 13:16:47'),
(235, 4, 233, 'Abc 2', 'folder', '2020-03-14 13:17:24', '2020-03-14 13:17:24'),
(243, 4, 241, 'F1', 'folder', '2020-03-14 13:24:02', '2020-03-14 13:24:02'),
(244, 4, 241, 'F11', 'folder', '2020-03-14 13:24:17', '2020-03-14 13:24:17'),
(247, 4, 242, 'G1', 'folder', '2020-03-14 13:25:41', '2020-03-14 13:25:41'),
(250, 4, 248, 'H2', 'folder', '2020-03-14 13:26:43', '2020-03-14 13:26:43'),
(251, 4, 248, 'H3', 'folder', '2020-03-14 13:26:51', '2020-03-14 13:26:51'),
(252, 4, 251, 'H3 1', 'folder', '2020-03-14 13:27:31', '2020-03-14 13:27:31'),
(253, 4, 251, 'H3 2', 'folder', '2020-03-14 13:27:39', '2020-03-14 13:27:39'),
(254, 4, 251, 'H3 3', 'folder', '2020-03-14 13:27:50', '2020-03-14 13:27:50'),
(255, 4, 250, 'H2 1', 'folder', '2020-03-14 13:28:02', '2020-03-14 13:28:02'),
(256, 4, 249, 'H1 1', 'folder', '2020-03-14 13:28:12', '2020-03-14 13:28:12'),
(257, 4, 248, 'H4', 'folder', '2020-03-14 13:28:41', '2020-03-14 13:28:41'),
(258, 4, 257, 'H4 1', 'folder', '2020-03-14 13:28:47', '2020-03-14 13:28:47'),
(264, 4, 263, 'B1', 'folder', '2020-03-14 13:42:15', '2020-03-14 13:42:15'),
(266, 4, 265, 'C1', 'folder', '2020-03-14 13:48:56', '2020-03-14 13:48:56'),
(267, 4, 266, 'C11', 'folder', '2020-03-14 13:49:03', '2020-03-14 13:49:03'),
(268, 4, 266, 'C12', 'folder', '2020-03-14 13:49:10', '2020-03-14 13:49:10'),
(270, 78, 1, 'bhargav', 'folder', '2020-03-14 14:10:32', '2020-03-14 14:10:32'),
(271, 78, 1, 'bhargav', 'folder', '2020-03-14 14:10:33', '2020-03-14 14:10:33'),
(277, 1, 273, 'Test 1 - 11', 'folder', '2020-03-16 05:12:55', '2020-03-16 05:12:55'),
(278, 1, 277, 'Test 11 - 111', 'folder', '2020-03-16 05:22:55', '2020-03-16 05:22:55'),
(280, 4, 279, 'Test 11', 'folder', '2020-03-16 05:49:07', '2020-03-16 05:49:07'),
(281, 4, 279, 'Test 12', 'folder', '2020-03-16 05:49:17', '2020-03-16 05:49:17'),
(282, 4, 280, 'Test 11 1', 'folder', '2020-03-16 05:49:32', '2020-03-16 05:49:32'),
(287, 4, 286, 'Test 21', 'folder', '2020-03-16 05:53:07', '2020-03-16 05:53:07'),
(288, 4, 287, 'Test 21 1', 'folder', '2020-03-16 05:54:03', '2020-03-16 05:54:03'),
(289, 4, 286, 'Test 22', 'folder', '2020-03-16 05:54:47', '2020-03-16 05:54:47'),
(291, 1, 283, 'Test 1 - 11', 'folder', '2020-03-16 05:57:57', '2020-03-16 05:57:57'),
(292, 4, 290, 'Test 31', 'folder', '2020-03-16 05:58:51', '2020-03-16 05:58:51'),
(293, 4, 292, 'Test 31 1', 'folder', '2020-03-16 05:59:28', '2020-03-16 05:59:28'),
(294, 4, 292, 'test 31 2', 'folder', '2020-03-16 06:00:01', '2020-03-16 06:00:01'),
(295, 4, 290, 'Test 32', 'folder', '2020-03-16 06:00:30', '2020-03-16 06:00:30'),
(296, 1, 291, 'Test 11 - 111', 'folder', '2020-03-16 06:10:52', '2020-03-16 06:10:52'),
(297, 1, 296, 'Test 111 - 1111', 'folder', '2020-03-16 06:11:22', '2020-03-16 06:11:22'),
(298, 1, 283, 'Test 1 - 12', 'folder', '2020-03-16 06:11:43', '2020-03-16 06:11:43'),
(299, 1, 283, 'Test 1 - 13', 'folder', '2020-03-16 06:11:58', '2020-03-16 06:11:58'),
(300, 1, 298, 'Test 12 - 121', 'folder', '2020-03-16 06:12:17', '2020-03-16 06:12:17'),
(301, 4, 293, 'Test 31 1 1', 'folder', '2020-03-16 06:26:10', '2020-03-16 06:26:10'),
(302, 4, 293, 'Test 31 1 2', 'folder', '2020-03-16 06:26:31', '2020-03-16 06:26:31'),
(304, 2, 2, 'haha', 'folder', '2020-03-16 09:48:27', '2020-03-16 09:48:27'),
(305, 2, 2, 'test', 'folder', '2020-03-16 10:10:56', '2020-03-16 10:10:56'),
(306, 2, 305, 'test1', 'folder', '2020-03-16 10:11:11', '2020-03-16 10:11:11'),
(307, 2, 305, 'test2', 'folder', '2020-03-16 10:11:20', '2020-03-16 10:11:20'),
(308, 2, 306, 'test11', 'folder', '2020-03-16 10:11:31', '2020-03-16 10:11:31'),
(309, 2, 308, 'test111', 'folder', '2020-03-16 10:11:43', '2020-03-16 10:11:43'),
(310, 2, 309, 'test1111', 'folder', '2020-03-16 10:11:54', '2020-03-16 10:11:54'),
(311, 4, 176, 'Gbb', 'folder', '2020-03-16 10:31:16', '2020-03-16 10:31:16'),
(314, 4, 313, 'F1 1', 'folder', '2020-03-16 11:55:23', '2020-03-16 11:55:23'),
(315, 4, 314, 'F1 1', 'folder', '2020-03-16 11:55:30', '2020-03-16 11:55:30'),
(317, 4, 316, 'F2 2', 'folder', '2020-03-16 11:55:48', '2020-03-16 11:55:48'),
(322, 78, 1, 'bhargav', 'folder', '2020-03-16 12:26:34', '2020-03-16 12:26:34'),
(323, 78, 78, 'bhargav', 'folder', '2020-03-16 12:26:45', '2020-03-16 12:26:45'),
(329, 4, 327, 'F1 1', 'folder', '2020-03-16 12:45:23', '2020-03-16 12:45:23'),
(331, 4, 327, 'F1 sds', 'folder', '2020-03-16 12:46:31', '2020-03-16 12:46:31'),
(334, 4, 332, 'F1 1', 'folder', '2020-03-16 12:56:49', '2020-03-16 12:56:49'),
(338, 4, 337, 'Vbbb', 'folder', '2020-03-16 13:25:16', '2020-03-16 13:25:16'),
(339, 4, 338, 'Hhhhh', 'folder', '2020-03-16 13:27:33', '2020-03-16 13:27:33'),
(340, 4, 337, 'F1 sds', 'folder', '2020-03-16 13:28:56', '2020-03-16 13:28:56'),
(342, 4, 341, 'Ghhh', 'folder', '2020-03-16 13:39:01', '2020-03-16 13:39:01'),
(343, 4, 342, 'Hhhh', 'folder', '2020-03-16 13:39:14', '2020-03-16 13:39:14'),
(344, 4, 341, 'Hhhhh', 'folder', '2020-03-16 13:39:30', '2020-03-16 13:39:30'),
(345, 4, 344, 'Uuhhh', 'folder', '2020-03-16 13:39:35', '2020-03-16 13:39:35'),
(348, 4, 347, 'Vvv', 'folder', '2020-03-16 13:56:20', '2020-03-16 13:56:20'),
(349, 4, 348, 'Hhbb', 'folder', '2020-03-16 13:56:26', '2020-03-16 13:56:26'),
(352, 4, 350, 'Bbb', 'folder', '2020-03-16 13:56:46', '2020-03-16 13:56:46'),
(353, 4, 350, 'Gvvv', 'folder', '2020-03-16 13:56:58', '2020-03-16 13:56:58'),
(356, 4, 355, 'Ggvv', 'folder', '2020-03-16 14:01:10', '2020-03-16 14:01:10'),
(357, 4, 356, 'Ghgh', 'folder', '2020-03-16 14:01:19', '2020-03-16 14:01:19'),
(358, 4, 357, 'Bbbb', 'folder', '2020-03-16 14:01:25', '2020-03-16 14:01:25'),
(359, 4, 357, 'Vbbb', 'folder', '2020-03-16 14:01:29', '2020-03-16 14:01:29'),
(362, 4, 361, 'F1 1', 'folder', '2020-03-16 14:11:44', '2020-03-16 14:11:44'),
(363, 4, 362, 'F1 11', 'folder', '2020-03-16 14:11:52', '2020-03-16 14:11:52'),
(365, 4, 364, 'F2 1', 'folder', '2020-03-16 14:12:09', '2020-03-16 14:12:09'),
(366, 4, 365, 'F2 11', 'folder', '2020-03-16 14:12:19', '2020-03-16 14:12:19'),
(368, 4, 367, 'F1 1', 'folder', '2020-03-17 04:35:41', '2020-03-17 04:35:41'),
(369, 4, 368, 'F1 11', 'folder', '2020-03-17 04:36:01', '2020-03-17 04:36:01'),
(370, 4, 367, 'F1 2', 'folder', '2020-03-17 04:36:26', '2020-03-17 04:36:26'),
(371, 4, 370, 'F1 21', 'folder', '2020-03-17 04:36:42', '2020-03-17 04:36:42'),
(372, 4, 370, 'F1 22', 'folder', '2020-03-17 04:36:53', '2020-03-17 04:36:53'),
(375, 3, 374, 'test1', 'folder', '2020-03-17 05:06:48', '2020-03-17 05:06:48'),
(376, 4, 373, 'F2 1', 'folder', '2020-03-17 05:06:50', '2020-03-17 05:06:50'),
(377, 3, 374, 'test2', 'folder', '2020-03-17 05:06:57', '2020-03-17 05:06:57'),
(378, 3, 375, 'test11', 'folder', '2020-03-17 05:07:06', '2020-03-17 05:07:06'),
(379, 3, 378, 'test111', 'folder', '2020-03-17 05:07:20', '2020-03-17 05:07:20'),
(380, 3, 379, 'test1111', 'folder', '2020-03-17 05:07:34', '2020-03-17 05:07:34'),
(383, 4, 382, 'F2 1', 'folder', '2020-03-17 05:35:36', '2020-03-17 05:35:36'),
(384, 4, 381, 'F1 1', 'folder', '2020-03-17 05:35:44', '2020-03-17 05:35:44'),
(389, 4, 388, 'F2', 'folder', '2020-03-17 06:04:56', '2020-03-17 06:04:56'),
(393, 4, 390, 'F1_1', 'folder', '2020-03-17 06:07:48', '2020-03-17 06:07:48'),
(394, 4, 393, 'F1_11', 'folder', '2020-03-17 06:07:58', '2020-03-17 06:07:58'),
(395, 4, 390, 'F1_2', 'folder', '2020-03-17 06:10:42', '2020-03-17 06:10:42'),
(400, 4, 398, 'F3_1', 'folder', '2020-03-17 06:22:15', '2020-03-17 06:22:15'),
(401, 4, 396, 'F1_1', 'folder', '2020-03-17 06:22:26', '2020-03-17 06:22:26'),
(402, 4, 397, 'F2_2', 'folder', '2020-03-17 06:23:50', '2020-03-17 06:23:50'),
(403, 4, 398, 'F3_2', 'folder', '2020-03-17 06:25:51', '2020-03-17 06:25:51'),
(410, 4, 409, 'F2_1', 'folder', '2020-03-17 07:24:20', '2020-03-17 07:24:20'),
(411, 4, 409, 'F2_2', 'folder', '2020-03-17 07:24:31', '2020-03-17 07:24:31'),
(412, 4, 410, 'F2_11', 'folder', '2020-03-17 07:24:39', '2020-03-17 07:24:39'),
(414, 1, 1, 'Assad’s', 'folder', '2020-03-17 07:41:02', '2020-03-17 07:41:02'),
(415, 1, 414, 'test', 'folder', '2020-03-17 10:29:41', '2020-03-17 10:29:41'),
(416, 1, 1, 'my docs', 'folder', '2020-03-17 10:30:18', '2020-03-17 10:30:18'),
(417, 1, 1, 'test', 'folder', '2020-03-17 10:30:51', '2020-03-17 10:30:51'),
(418, 7, 0, '7', NULL, '2020-03-17 13:27:02', '2020-03-17 13:27:02'),
(419, 8, 0, '8', NULL, '2020-03-18 05:22:24', '2020-03-18 05:22:24'),
(420, 9, 0, '9', NULL, '2020-03-18 05:44:05', '2020-03-18 05:44:05'),
(421, 3, 3, 'my folders', 'folder', '2020-03-18 11:51:06', '2020-03-18 11:51:06'),
(422, 3, 421, 'test1', 'folder', '2020-03-18 11:51:16', '2020-03-18 11:51:16'),
(423, 3, 422, 'test11', 'folder', '2020-03-18 11:51:26', '2020-03-18 11:51:26'),
(424, 3, 423, 'test111', 'folder', '2020-03-18 11:51:38', '2020-03-18 11:51:38'),
(425, 3, 421, 'test1 new', 'folder', '2020-03-18 11:52:19', '2020-03-18 11:52:19'),
(426, 3, 424, 't', 'folder', '2020-03-18 11:53:42', '2020-03-18 11:53:42'),
(427, 3, 423, 'test', 'folder', '2020-03-18 12:43:25', '2020-03-18 12:43:25'),
(428, 3, 3, 'test', 'folder', '2020-03-18 13:05:09', '2020-03-18 13:05:09');

-- --------------------------------------------------------

--
-- Table structure for table `folders_instructor`
--

CREATE TABLE `folders_instructor` (
  `id` int(255) NOT NULL,
  `user_id` int(255) NOT NULL,
  `current_folder_id` int(255) DEFAULT NULL,
  `parent_folder_id` int(255) DEFAULT NULL,
  `folder_name` varchar(255) NOT NULL,
  `filetype` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `folders_instructor`
--

INSERT INTO `folders_instructor` (`id`, `user_id`, `current_folder_id`, `parent_folder_id`, `folder_name`, `filetype`, `created_at`, `updated_at`) VALUES
(1, 78, NULL, 70, 'bhargav', 'folder', '2020-03-18 02:11:07', '2020-03-18 02:11:07'),
(2, 78, NULL, 1, 'bhargav', 'folder', '2020-03-18 02:11:21', '2020-03-18 02:11:21'),
(3, 78, NULL, 1, 'bhargav', 'folder', '2020-03-18 08:57:51', '2020-03-18 08:57:51');

-- --------------------------------------------------------

--
-- Table structure for table `help`
--

CREATE TABLE `help` (
  `id` int(255) NOT NULL,
  `text` text,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `help`
--

INSERT INTO `help` (`id`, `text`, `updated_at`, `created_at`) VALUES
(1, '<p><br></p><h3 style=\"margin: 15px 0px; padding: 0px; font-weight: 700; font-size: 14px; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif;\">Section 1.10.32 of \"de Finibus Bonorum et Malorum\", written by Cicero in 45 BC</h3><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px;\">\"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?\"</p><h3 style=\"margin: 15px 0px; padding: 0px; font-weight: 700; font-size: 14px; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif;\">1914 translation by H. Rackham</h3><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px;\">\"But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?\"</p><h3 style=\"margin: 15px 0px; padding: 0px; font-weight: 700; font-size: 14px; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif;\">Section 1.10.33 of \"de Finibus Bonorum et Malorum\", written by Cicero in 45 BC</h3><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px;\">\"At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.\"</p><h3 style=\"margin: 15px 0px; padding: 0px; font-weight: 700; font-size: 14px; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif;\">1914 translation by H. Rackham</h3><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px;\">\"On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammelled and when nothing prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided. But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pains.\"</p>', '2020-03-17 08:10:47', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `instructors`
--

CREATE TABLE `instructors` (
  `id` int(255) NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `student_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `school` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `program` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `zipcode` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `device_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `otp` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role_id` int(255) DEFAULT NULL,
  `is_approved` int(10) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `instructors`
--

INSERT INTO `instructors` (`id`, `first_name`, `last_name`, `email`, `email_verified_at`, `password`, `phone`, `student_id`, `school`, `program`, `zipcode`, `user_token`, `remember_token`, `device_token`, `image`, `otp`, `role_id`, `is_approved`, `created_at`, `updated_at`) VALUES
(2, 'bhargav', 'kachhot', 'bhargavkachhot.rs@gmail.com', NULL, '$2y$10$Z2fGsIaCdl93sCO6quLrDeS6F8Ad6P4kh.MCuX06MiM7t1b8.V5Ou', '7984327389', '46', 'Marwadi', 'Mech', '380006', '7be9f243aa1bde1c3787cca520b84d6f2b3639cc7bf5009079749c05ab78c355', NULL, '123456789', 'http://15.206.38.116/clinicalwallet/public/profile/images/1584535584.jfif', '615866', 1, 1, '2020-03-14 04:05:07', '2020-03-18 12:46:24'),
(3, 'Bhargav', 'Kachhot', 'bhargavkachhot.rs@gmail.comrtrt', NULL, '$2y$10$6BDSOJpgCD2.EH7FTZ747OvX4XVD0mYClT3niiYeG2073NbLjXOxi', '7984327389', 'MQ123', 'AIS', 'iOS', '983682', 'c7631fea08f07b063e8ff300d', NULL, '123456789', 'http://15.206.38.116/clinicalwallet/public/profile/images/1584535561.jfif', NULL, 1, 0, '2020-03-17 07:56:47', '2020-03-18 12:46:01'),
(4, 'Bhargav', 'Kachhot', 'bhargavkachhot.rs@gmail.comssss', NULL, '$2y$10$GVTMPULTIZYtxgkO37YkOu6IUCEYYGdRxwrY3V4XHJdTtHlYLv3Ha', '7984327389', 'MQ123', 'AIS', 'iOS', '983682', 'Kc84c901176b496b00e4c8bc5', NULL, '123456789', 'http://15.206.38.116/clinicalwallet/public/profile/images/1584508315.jpg', NULL, 1, 0, '2020-03-18 05:11:55', '2020-03-18 05:11:55'),
(5, 'Bhargav', 'Kachhot', 'bhargavkachhot.rs@gmail.comsssss', NULL, '$2y$10$Uw1XvtiUqFY9IJ9/rI9GZ.fXzr06bzWmMD11UeDGYTJtS3RVwHdD2', '7984327389', 'MQ123', 'AIS', 'iOS', '983682', 'Nd1e839c7c41efdebb07d006e', NULL, '123456789', 'http://15.206.38.116/clinicalwallet/public/profile/images/1584508665.jpg', NULL, 1, 0, '2020-03-18 05:17:45', '2020-03-18 05:17:45'),
(6, 'Bhargav', 'Kachhot', 'bhargavkachhot.rs@gmail.comssssssss', NULL, '$2y$10$Do.kqARVGbPAo2Vy1saolOthY8IPAXSvJqrTcfxuUj12KZgKamrXe', '7984327389', 'MQ123', 'AIS', 'iOS', '983682', 't2cfe6f9250a247d16f6e00d4', NULL, '123456789', 'http://15.206.38.116/clinicalwallet/public/profile/images/1584508871.jpg', NULL, 1, 0, '2020-03-18 05:21:11', '2020-03-18 05:21:11'),
(7, 'Bhargav', 'Kachhot', 'bhargavkachhot.rs@gmail.comssssssssdfdfdf', NULL, '$2y$10$W5LTpALYqHA83f2LiHsFcuMXrpN0lE.6pheW0LF2ph688r3CcsEwi', '7984327389', 'MQ123', 'AIS', 'iOS', '983682', 'O17c479e2ead882d535f314e2', NULL, '123456789', 'http://15.206.38.116/clinicalwallet/public/profile/images/1584522158.jpg', NULL, 1, 0, '2020-03-18 09:02:38', '2020-03-18 09:02:38');

-- --------------------------------------------------------

--
-- Table structure for table `instructor_documents`
--

CREATE TABLE `instructor_documents` (
  `id` int(255) NOT NULL,
  `user_id` int(255) NOT NULL,
  `instructor_id` int(255) DEFAULT NULL,
  `group_id` int(255) DEFAULT NULL,
  `folder_id` int(255) DEFAULT NULL,
  `document` varchar(255) DEFAULT NULL,
  `parent_folder_id` int(255) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `filetype` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `instructor_documents`
--

INSERT INTO `instructor_documents` (`id`, `user_id`, `instructor_id`, `group_id`, `folder_id`, `document`, `parent_folder_id`, `filename`, `filetype`, `created_at`, `updated_at`) VALUES
(64, 78, NULL, NULL, NULL, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/78/Clinical Wallet Requirments (1).xlsx', 78, 'Clinical Wallet Requirments (1).xlsx', 'xlsx', '2020-03-18 02:16:33', '2020-03-18 02:16:33'),
(65, 78, NULL, NULL, NULL, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/78/commands for git bash.txt', 78, 'commands for git bash.txt', 'txt', '2020-03-18 08:57:57', '2020-03-18 08:57:57');

-- --------------------------------------------------------

--
-- Table structure for table `instructor_folders`
--

CREATE TABLE `instructor_folders` (
  `id` int(255) NOT NULL,
  `user_id` int(255) NOT NULL,
  `current_folder_id` int(255) DEFAULT NULL,
  `parent_folder_id` int(255) DEFAULT NULL,
  `folder_name` varchar(255) NOT NULL,
  `filetype` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_02_26_051409_create_admins_table', 2),
(5, '2020_02_26_072344_create_instructor_table', 3),
(6, '2020_02_26_072735_create_instructor_table', 4);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `id` int(255) NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(255) DEFAULT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`id`, `email`, `user_id`, `token`, `created_at`, `updated_at`) VALUES
(1, 'bhargavkachhot.rs@gmail.com', 2, '$2y$10$5x.FFodlEsDtMC0g02BPou/aFIVbnD65/mEM7cA.HItPnXuxscqPO', '2020-02-28 05:49:07', '2020-03-18 04:50:13'),
(2, 'o@gmail.com', 50, NULL, '2020-03-05 01:24:47', '2020-03-05 01:24:47'),
(3, 'happybhimani.rs@gmail.com', 4, NULL, '2020-03-06 09:10:01', '2020-03-17 07:27:28'),
(4, 'khyatip.rs@gmail.com', 3, NULL, '2020-03-06 12:28:56', '2020-03-17 07:18:11'),
(5, 'test@yopmail.com', 83, NULL, '2020-03-11 07:17:32', '2020-03-11 07:17:32');

-- --------------------------------------------------------

--
-- Table structure for table `privacy_policy`
--

CREATE TABLE `privacy_policy` (
  `id` int(255) NOT NULL,
  `text` text,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `privacy_policy`
--

INSERT INTO `privacy_policy` (`id`, `text`, `updated_at`, `created_at`, `deleted_at`) VALUES
(1, '<p><br></p><h3 style=\"margin: 15px 0px; padding: 0px; font-weight: 700; font-size: 14px; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif;\">Section 1.10.32 of \"de Finibus Bonorum et Malorum\", written by Cicero in 45 BC</h3><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px;\">\"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?\"</p><h3 style=\"margin: 15px 0px; padding: 0px; font-weight: 700; font-size: 14px; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif;\">1914 translation by H. Rackham</h3><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px;\">\"But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?\"</p><h3 style=\"margin: 15px 0px; padding: 0px; font-weight: 700; font-size: 14px; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif;\">Section 1.10.33 of \"de Finibus Bonorum et Malorum\", written by Cicero in 45 BC</h3><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px;\">\"At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.\"</p><h3 style=\"margin: 15px 0px; padding: 0px; font-weight: 700; font-size: 14px; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif;\">1914 translation by H. Rackham</h3><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px;\">\"On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammelled and when nothing prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided. But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pains.\"</p>', '2020-03-18 11:18:16', '2020-03-18 00:00:00', '2020-03-17 13:44:46');

-- --------------------------------------------------------

--
-- Table structure for table `programs`
--

CREATE TABLE `programs` (
  `id` int(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `school_id` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `programs`
--

INSERT INTO `programs` (`id`, `name`, `school_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 'BSN', '1', NULL, NULL, '2020-03-17 08:07:14'),
(3, 'BSN', '2', NULL, NULL, '2020-03-17 08:07:14'),
(4, 'BSN', '3', NULL, NULL, '2020-03-17 08:07:14'),
(5, 'BSN', '4', NULL, NULL, '2020-03-17 08:07:14'),
(6, 'BSN', '5', NULL, NULL, '2020-03-17 08:07:14'),
(7, 'BSN', '6', NULL, NULL, '2020-03-17 08:07:14'),
(8, 'BSN', '9', NULL, NULL, '2020-03-17 08:07:14'),
(9, 'BSN', '10', NULL, NULL, '2020-03-17 08:07:14'),
(10, 'BSN', '12', NULL, NULL, '2020-03-17 08:07:14'),
(11, 'BSN', '13', NULL, NULL, '2020-03-17 08:07:14'),
(12, 'BSN', '14', NULL, NULL, '2020-03-17 08:07:14'),
(13, 'BSN', '15', NULL, NULL, '2020-03-17 08:07:14'),
(14, 'BSN', '16', NULL, NULL, '2020-03-17 08:07:14'),
(15, 'BSN', '17', NULL, NULL, '2020-03-17 08:07:14'),
(16, 'BSN', '18', NULL, NULL, '2020-03-17 08:07:14'),
(17, 'BSN', '19', NULL, NULL, '2020-03-17 08:07:14'),
(18, 'BSN', '20', NULL, NULL, '2020-03-17 08:07:14'),
(19, 'BSN', '21', NULL, NULL, '2020-03-17 08:07:14'),
(20, 'BSN', '22', NULL, NULL, '2020-03-17 08:07:14'),
(21, 'BSN', '23', NULL, NULL, '2020-03-17 08:07:14'),
(22, 'BSN', '24', NULL, NULL, '2020-03-17 08:07:14'),
(23, 'BSN', '25', NULL, NULL, '2020-03-17 08:07:14'),
(24, 'BSN', '26', NULL, NULL, '2020-03-17 08:07:14'),
(25, 'BSN', '27', NULL, NULL, '2020-03-17 08:07:14'),
(26, 'BSN', '26', NULL, NULL, '2020-03-17 08:07:14'),
(27, 'BSN', '27', NULL, NULL, '2020-03-17 08:07:14'),
(28, 'BSN', '28', NULL, NULL, '2020-03-17 08:07:14'),
(29, 'BSN', '29', NULL, NULL, '2020-03-17 08:07:14'),
(30, 'BSN', '30', NULL, NULL, '2020-03-17 08:07:14'),
(31, 'BSN', '31', NULL, NULL, '2020-03-17 08:07:14'),
(32, 'BSN', '32', NULL, NULL, '2020-03-17 08:07:14'),
(33, 'BSN', '33', NULL, NULL, '2020-03-17 08:07:14'),
(34, 'BSN', '11', NULL, NULL, '2020-03-17 08:07:14'),
(35, 'BSN', '7', NULL, NULL, '2020-03-17 08:07:14'),
(36, 'BSN', '8', NULL, NULL, '2020-03-17 08:07:14'),
(37, 'BSN', '9', NULL, NULL, '2020-03-17 08:07:14');

-- --------------------------------------------------------

--
-- Table structure for table `schools`
--

CREATE TABLE `schools` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `zipcode` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` enum('0','1') NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `schools`
--

INSERT INTO `schools` (`id`, `name`, `zipcode`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'David Geffen School of Medicine', '90073', '2017-02-07 12:48:43', '2017-06-02 07:05:27', '0'),
(2, 'Pritzker School of Medicine', 'IL 60637', '2017-02-07 12:48:57', '2017-04-17 04:49:05', '0'),
(3, 'Perelman Center for Advanced Medicine', 'PA 19104', '2017-02-07 12:49:13', '2017-02-23 14:53:07', '0'),
(4, 'American Boychoir School', 'NJ 123', '2017-02-07 12:53:30', '2017-02-23 14:52:36', '0'),
(5, 'Harvard Medical School', 'MA 02115', '2017-02-23 14:51:00', '2017-02-23 14:52:00', '0'),
(6, 'Baylor College of Medicine Academy', 'TX 77030', '2017-02-23 14:51:42', '0000-00-00 00:00:00', '0'),
(7, 'Weill Cornell Graduate School of Medical Sciences', 'NY 10065', '2017-02-23 14:55:27', '0000-00-00 00:00:00', '0'),
(8, 'Mayo Medical School', 'MN 55905', '2017-02-23 14:55:59', '0000-00-00 00:00:00', '0'),
(9, 'Washington University School of Medicine', 'MO 63110', '2017-02-23 14:56:25', '0000-00-00 00:00:00', '0'),
(10, 'Oregon Health & Science University', 'OR 97239', '2017-02-24 05:27:28', '0000-00-00 00:00:00', '0'),
(12, 'Alpert Medical School', '02903', '2017-02-24 10:48:14', '2017-04-21 09:46:43', '0'),
(14, 'Georgetown University School of Medicine', '200075', '2017-02-24 12:21:16', '2017-04-13 07:50:05', '0'),
(15, 'Tufts University School of Medicine', '02111', '2017-04-14 05:26:20', '2017-04-14 07:27:01', '0'),
(16, 'GateWay Community College', '85034', '2017-04-17 02:10:15', '2017-04-17 03:39:45', '1'),
(17, 'Grand Canyon University', '85017', '2017-04-17 02:12:29', '2017-04-17 04:15:09', '0'),
(18, 'GateWay Community College', '85034', '2017-04-17 03:40:26', '2017-04-17 05:41:17', '0'),
(23, 'KU School of Nursing', '66160', '2017-06-20 00:43:37', '2017-06-21 20:11:53', '1'),
(24, 'Saint Luke\'s College of Health Sciences', '64111', '2017-06-21 20:15:30', '2018-03-06 00:56:17', '0'),
(25, 'TracPrac University', '85267-2303', '2017-06-28 17:06:05', '2018-03-06 00:46:10', '0'),
(26, 'Phoenix College ', '85013', '2017-10-13 20:10:05', '2017-10-13 20:20:49', '0'),
(28, 'Trinity Valley Community College', '75142', '2017-11-28 20:11:34', '2017-12-29 10:19:12', '0'),
(30, 'Weatherford College', '76086', '2017-12-13 16:28:07', '0000-00-00 00:00:00', '0'),
(31, 'Seminole State College of Florida', '32714', '2018-05-10 21:43:44', '2018-05-14 12:12:04', '0'),
(32, 'Central Arizona College', '85119', '2018-05-10 21:58:32', '2018-05-10 21:59:17', '0'),
(33, 'Jefferson Community & Technical College', '40202-2005', '2018-08-05 23:07:31', '2018-08-05 23:08:21', '0'),
(34, 'Paradise Valley CC', '85032', '2018-08-16 07:09:37', '2018-08-16 07:10:20', '0');

-- --------------------------------------------------------

--
-- Table structure for table `students_documents`
--

CREATE TABLE `students_documents` (
  `id` int(255) NOT NULL,
  `user_id` int(255) NOT NULL,
  `document` varchar(255) DEFAULT NULL,
  `parent_folder_id` int(255) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `filetype` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `students_documents`
--

INSERT INTO `students_documents` (`id`, `user_id`, `document`, `parent_folder_id`, `filename`, `filetype`, `created_at`, `updated_at`) VALUES
(1, 1, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/1/1584166880.8625798.doc', 32, '1584166880.8625798.doc', 'doc', '2020-03-14 06:21:21', '2020-03-14 06:21:21'),
(2, 1, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/1/1584167018.4531221.doc', 32, '1584167018.4531221.doc', 'doc', '2020-03-14 06:23:38', '2020-03-14 06:23:38'),
(3, 1, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/1/1584167018.453393.doc', 32, '1584167018.453393.doc', 'doc', '2020-03-14 06:23:38', '2020-03-14 06:23:38'),
(4, 1, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/1/1584167018.455116.docx', 32, '1584167018.455116.docx', 'docx', '2020-03-14 06:23:38', '2020-03-14 06:23:38'),
(5, 1, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/1/1584167421.2724729.pdf', 42, '1584167421.2724729.pdf', 'pdf', '2020-03-14 06:30:21', '2020-03-14 06:30:21'),
(8, 75, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/75/BubbleView.java', 1, 'BubbleView.java', 'java', '2020-03-14 06:35:49', '2020-03-14 06:35:49'),
(9, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584176603.0983238.pdf', 68, '1584176603.0983238.pdf', 'pdf', '2020-03-14 09:03:23', '2020-03-14 09:03:23'),
(10, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584177043.5328012.pdf', 79, '1584177043.5328012.pdf', 'pdf', '2020-03-14 09:10:43', '2020-03-14 09:10:43'),
(11, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584177052.45549.pdf', 79, '1584177052.45549.pdf', 'pdf', '2020-03-14 09:10:52', '2020-03-14 09:10:52'),
(12, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584177052.455933.pdf', 79, '1584177052.455933.pdf', 'pdf', '2020-03-14 09:10:52', '2020-03-14 09:10:52'),
(13, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584177052.456468.pdf', 79, '1584177052.456468.pdf', 'pdf', '2020-03-14 09:10:52', '2020-03-14 09:10:52'),
(14, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584177062.606493.pdf', 79, '1584177062.606493.pdf', 'pdf', '2020-03-14 09:11:03', '2020-03-14 09:11:03'),
(15, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584177062.606835.pdf', 79, '1584177062.606835.pdf', 'pdf', '2020-03-14 09:11:03', '2020-03-14 09:11:03'),
(16, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584177062.607136.pdf', 79, '1584177062.607136.pdf', 'pdf', '2020-03-14 09:11:03', '2020-03-14 09:11:03'),
(17, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584177062.607429.pdf', 79, '1584177062.607429.pdf', 'pdf', '2020-03-14 09:11:03', '2020-03-14 09:11:03'),
(18, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584177062.6077108.pdf', 79, '1584177062.6077108.pdf', 'pdf', '2020-03-14 09:11:03', '2020-03-14 09:11:03'),
(19, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584177062.608.pdf', 79, '1584177062.608.pdf', 'pdf', '2020-03-14 09:11:03', '2020-03-14 09:11:03'),
(20, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584177120.463479.pdf', 67, '1584177120.463479.pdf', 'pdf', '2020-03-14 09:12:00', '2020-03-14 09:12:00'),
(21, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584177127.001946.pdf', 67, '1584177127.001946.pdf', 'pdf', '2020-03-14 09:12:07', '2020-03-14 09:12:07'),
(22, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584177127.002392.pdf', 67, '1584177127.002392.pdf', 'pdf', '2020-03-14 09:12:07', '2020-03-14 09:12:07'),
(23, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584177127.002956.pdf', 67, '1584177127.002956.pdf', 'pdf', '2020-03-14 09:12:07', '2020-03-14 09:12:07'),
(24, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584177365.246891.pdf', 76, '1584177365.246891.pdf', 'pdf', '2020-03-14 09:16:05', '2020-03-14 09:16:05'),
(25, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584177365.247247.pdf', 76, '1584177365.247247.pdf', 'pdf', '2020-03-14 09:16:05', '2020-03-14 09:16:05'),
(26, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584177365.247548.pdf', 76, '1584177365.247548.pdf', 'pdf', '2020-03-14 09:16:05', '2020-03-14 09:16:05'),
(27, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584177365.2478561.pdf', 76, '1584177365.2478561.pdf', 'pdf', '2020-03-14 09:16:05', '2020-03-14 09:16:05'),
(28, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584177365.248547.pdf', 76, '1584177365.248547.pdf', 'pdf', '2020-03-14 09:16:05', '2020-03-14 09:16:05'),
(29, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584177365.2488852.pdf', 76, '1584177365.2488852.pdf', 'pdf', '2020-03-14 09:16:05', '2020-03-14 09:16:05'),
(30, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584177386.0567951.pdf', 75, '1584177386.0567951.pdf', 'pdf', '2020-03-14 09:16:27', '2020-03-14 09:16:27'),
(31, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584177386.05706.pdf', 75, '1584177386.05706.pdf', 'pdf', '2020-03-14 09:16:27', '2020-03-14 09:16:27'),
(32, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584177386.0572891.pdf', 75, '1584177386.0572891.pdf', 'pdf', '2020-03-14 09:16:27', '2020-03-14 09:16:27'),
(33, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584177386.057502.pdf', 75, '1584177386.057502.pdf', 'pdf', '2020-03-14 09:16:27', '2020-03-14 09:16:27'),
(34, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584177386.0577168.pdf', 75, '1584177386.0577168.pdf', 'pdf', '2020-03-14 09:16:27', '2020-03-14 09:16:27'),
(35, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584177386.0579371.pdf', 75, '1584177386.0579371.pdf', 'pdf', '2020-03-14 09:16:27', '2020-03-14 09:16:27'),
(36, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584177386.058156.pdf', 75, '1584177386.058156.pdf', 'pdf', '2020-03-14 09:16:27', '2020-03-14 09:16:27'),
(37, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584177386.058358.pdf', 75, '1584177386.058358.pdf', 'pdf', '2020-03-14 09:16:27', '2020-03-14 09:16:27'),
(38, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584177386.058573.pdf', 75, '1584177386.058573.pdf', 'pdf', '2020-03-14 09:16:27', '2020-03-14 09:16:27'),
(39, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584177386.0588598.pdf', 75, '1584177386.0588598.pdf', 'pdf', '2020-03-14 09:16:27', '2020-03-14 09:16:27'),
(40, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584178532.6231809.jpg', 65, '1584178532.6231809.jpg', 'jpg', '2020-03-14 09:35:32', '2020-03-14 09:35:32'),
(41, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584178555.8099809.jpg', 65, '1584178555.8099809.jpg', 'jpg', '2020-03-14 09:35:57', '2020-03-14 09:35:57'),
(42, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584178555.811527.jpg', 65, '1584178555.811527.jpg', 'jpg', '2020-03-14 09:35:57', '2020-03-14 09:35:57'),
(43, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584178555.812996.jpg', 65, '1584178555.812996.jpg', 'jpg', '2020-03-14 09:35:57', '2020-03-14 09:35:57'),
(44, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584178679.975388.jpg', 75, '1584178679.975388.jpg', 'jpg', '2020-03-14 09:38:03', '2020-03-14 09:38:03'),
(45, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584178679.97691.jpg', 75, '1584178679.97691.jpg', 'jpg', '2020-03-14 09:38:03', '2020-03-14 09:38:03'),
(46, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584178679.978348.jpg', 75, '1584178679.978348.jpg', 'jpg', '2020-03-14 09:38:03', '2020-03-14 09:38:03'),
(47, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584178679.979813.jpg', 75, '1584178679.979813.jpg', 'jpg', '2020-03-14 09:38:03', '2020-03-14 09:38:03'),
(48, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584178679.981052.jpg', 75, '1584178679.981052.jpg', 'jpg', '2020-03-14 09:38:03', '2020-03-14 09:38:03'),
(49, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584178679.9821448.jpg', 75, '1584178679.9821448.jpg', 'jpg', '2020-03-14 09:38:03', '2020-03-14 09:38:03'),
(55, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584179134.211992.jpg', 90, '1584179134.211992.jpg', 'jpg', '2020-03-14 09:45:34', '2020-03-14 09:45:34'),
(56, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584179143.53154.doc', 90, '1584179143.53154.doc', 'doc', '2020-03-14 09:45:43', '2020-03-14 09:45:43'),
(57, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584179152.504486.jpg', 90, '1584179152.504486.jpg', 'jpg', '2020-03-14 09:45:53', '2020-03-14 09:45:53'),
(58, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584179166.9923959.jpg', 90, '1584179166.9923959.jpg', 'jpg', '2020-03-14 09:46:07', '2020-03-14 09:46:07'),
(59, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584179189.8340821.jpg', 90, '1584179189.8340821.jpg', 'jpg', '2020-03-14 09:46:30', '2020-03-14 09:46:30'),
(60, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584179201.44612.jpg', 90, '1584179201.44612.jpg', 'jpg', '2020-03-14 09:46:42', '2020-03-14 09:46:42'),
(61, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584179225.5311198.jpg', 90, '1584179225.5311198.jpg', 'jpg', '2020-03-14 09:47:06', '2020-03-14 09:47:06'),
(69, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584184483.780194.doc', 115, '1584184483.780194.doc', 'doc', '2020-03-14 11:14:43', '2020-03-14 11:14:43'),
(70, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584184778.0270839.doc', 115, '1584184778.0270839.doc', 'doc', '2020-03-14 11:19:38', '2020-03-14 11:19:38'),
(71, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584184778.027697.doc', 115, '1584184778.027697.doc', 'doc', '2020-03-14 11:19:38', '2020-03-14 11:19:38'),
(72, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584184982.533345.jpg', 115, '1584184982.533345.jpg', 'jpg', '2020-03-14 11:23:02', '2020-03-14 11:23:02'),
(73, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584185139.279669.jpg', 114, '1584185139.279669.jpg', 'jpg', '2020-03-14 11:25:39', '2020-03-14 11:25:39'),
(74, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584185167.766749.doc', 120, '1584185167.766749.doc', 'doc', '2020-03-14 11:26:07', '2020-03-14 11:26:07'),
(75, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584185175.031395.doc', 117, '1584185175.031395.doc', 'doc', '2020-03-14 11:26:15', '2020-03-14 11:26:15'),
(76, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584185175.031513.doc', 117, '1584185175.031513.doc', 'doc', '2020-03-14 11:26:15', '2020-03-14 11:26:15'),
(77, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584185505.885462.jpg', 123, '1584185505.885462.jpg', 'jpg', '2020-03-14 11:31:46', '2020-03-14 11:31:46'),
(78, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584185523.3784761.jpg', 123, '1584185523.3784761.jpg', 'jpg', '2020-03-14 11:32:03', '2020-03-14 11:32:03'),
(79, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584185550.431477.jpg', 122, '1584185550.431477.jpg', 'jpg', '2020-03-14 11:32:31', '2020-03-14 11:32:31'),
(81, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584186037.203394.jpg', 121, '1584186037.203394.jpg', 'jpg', '2020-03-14 11:40:37', '2020-03-14 11:40:37'),
(82, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584186058.856914.jpg', 122, '1584186058.856914.jpg', 'jpg', '2020-03-14 11:40:59', '2020-03-14 11:40:59'),
(83, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584186080.5826411.doc', 123, '1584186080.5826411.doc', 'doc', '2020-03-14 11:41:20', '2020-03-14 11:41:20'),
(84, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584186099.166298.doc', 123, '1584186099.166298.doc', 'doc', '2020-03-14 11:41:39', '2020-03-14 11:41:39'),
(85, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584186135.672142.jpg', 123, '1584186135.672142.jpg', 'jpg', '2020-03-14 11:42:16', '2020-03-14 11:42:16'),
(86, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584186149.791315.jpg', 123, '1584186149.791315.jpg', 'jpg', '2020-03-14 11:42:30', '2020-03-14 11:42:30'),
(87, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584186214.466004.jpg', 122, '1584186214.466004.jpg', 'jpg', '2020-03-14 11:43:34', '2020-03-14 11:43:34'),
(88, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584186391.5898042.jpg', 132, '1584186391.5898042.jpg', 'jpg', '2020-03-14 11:46:32', '2020-03-14 11:46:32'),
(89, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584186429.242734.jpg', 132, '1584186429.242734.jpg', 'jpg', '2020-03-14 11:47:09', '2020-03-14 11:47:09'),
(90, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584186820.3152652.doc', 127, '1584186820.3152652.doc', 'doc', '2020-03-14 11:53:40', '2020-03-14 11:53:40'),
(91, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584186866.1154718.jpg', 127, '1584186866.1154718.jpg', 'jpg', '2020-03-14 11:54:26', '2020-03-14 11:54:26'),
(92, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584186910.54631.jpg', 127, '1584186910.54631.jpg', 'jpg', '2020-03-14 11:55:11', '2020-03-14 11:55:11'),
(93, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584187032.9517012.jpg', 127, '1584187032.9517012.jpg', 'jpg', '2020-03-14 11:57:13', '2020-03-14 11:57:13'),
(94, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584187049.1563.jpg', 131, '1584187049.1563.jpg', 'jpg', '2020-03-14 11:57:29', '2020-03-14 11:57:29'),
(95, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584187072.5594811.doc', 131, '1584187072.5594811.doc', 'doc', '2020-03-14 11:57:52', '2020-03-14 11:57:52'),
(96, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584187124.31596.jpg', 132, '1584187124.31596.jpg', 'jpg', '2020-03-14 11:58:45', '2020-03-14 11:58:45'),
(97, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584187169.758842.jpg', 132, '1584187169.758842.jpg', 'jpg', '2020-03-14 11:59:30', '2020-03-14 11:59:30'),
(98, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584187182.208187.doc', 133, '1584187182.208187.doc', 'doc', '2020-03-14 11:59:42', '2020-03-14 11:59:42'),
(99, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584187206.594265.doc', 132, '1584187206.594265.doc', 'doc', '2020-03-14 12:00:06', '2020-03-14 12:00:06'),
(100, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584187401.8062391.doc', 132, '1584187401.8062391.doc', 'doc', '2020-03-14 12:03:21', '2020-03-14 12:03:21'),
(101, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584187412.082817.jpg', 133, '1584187412.082817.jpg', 'jpg', '2020-03-14 12:03:32', '2020-03-14 12:03:32'),
(102, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584187604.883548.jpg', 132, '1584187604.883548.jpg', 'jpg', '2020-03-14 12:06:45', '2020-03-14 12:06:45'),
(103, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584187617.709032.jpg', 133, '1584187617.709032.jpg', 'jpg', '2020-03-14 12:06:58', '2020-03-14 12:06:58'),
(104, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584188374.248297.doc', 132, '1584188374.248297.doc', 'doc', '2020-03-14 12:19:34', '2020-03-14 12:19:34'),
(105, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584188388.9363399.doc', 132, '1584188388.9363399.doc', 'doc', '2020-03-14 12:19:49', '2020-03-14 12:19:49'),
(106, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584188412.433505.doc', 132, '1584188412.433505.doc', 'doc', '2020-03-14 12:20:12', '2020-03-14 12:20:12'),
(108, 1, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/1/1584188768.112519.pdf', 151, '1584188768.112519.pdf', 'pdf', '2020-03-14 12:26:09', '2020-03-14 12:26:09'),
(109, 1, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/1/1584188790.387023.doc', 151, '1584188790.387023.doc', 'doc', '2020-03-14 12:26:31', '2020-03-14 12:26:31'),
(110, 1, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/1/1584188800.660224.pdf', 153, '1584188800.660224.pdf', 'pdf', '2020-03-14 12:26:43', '2020-03-14 12:26:43'),
(111, 1, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/1/1584188819.974283.doc', 150, '1584188819.974283.doc', 'doc', '2020-03-14 12:27:01', '2020-03-14 12:27:01'),
(112, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584188855.3149471.jpg', 145, '1584188855.3149471.jpg', 'jpg', '2020-03-14 12:27:36', '2020-03-14 12:27:36'),
(113, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584188873.927516.jpg', 145, '1584188873.927516.jpg', 'jpg', '2020-03-14 12:27:54', '2020-03-14 12:27:54'),
(114, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584188993.0597792.jpg', 162, '1584188993.0597792.jpg', 'jpg', '2020-03-14 12:29:53', '2020-03-14 12:29:53'),
(115, 1, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/1/1584189133.092938.doc', 168, '1584189133.092938.doc', 'doc', '2020-03-14 12:32:14', '2020-03-14 12:32:14'),
(116, 1, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/1/1584189448.853895.doc', 173, '1584189448.853895.doc', 'doc', '2020-03-14 12:37:29', '2020-03-14 12:37:29'),
(117, 1, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/1/1584189550.0152779.docx', 174, '1584189550.0152779.docx', 'docx', '2020-03-14 12:39:29', '2020-03-14 12:39:29'),
(118, 1, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/1/1584189622.815793.docx', 173, '1584189622.815793.docx', 'docx', '2020-03-14 12:40:23', '2020-03-14 12:40:23'),
(119, 1, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/1/1584189869.342342.docx', 181, '1584189869.342342.docx', 'docx', '2020-03-14 12:44:29', '2020-03-14 12:44:29'),
(120, 1, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/1/1584189924.286152.doc', 184, '1584189924.286152.doc', 'doc', '2020-03-14 12:45:31', '2020-03-14 12:45:31'),
(121, 1, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/1/1584190146.363471.doc', 190, '1584190146.363471.doc', 'doc', '2020-03-14 12:49:06', '2020-03-14 12:49:06'),
(122, 1, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/1/1584190294.745098.doc', 190, '1584190294.745098.doc', 'doc', '2020-03-14 12:51:34', '2020-03-14 12:51:34'),
(123, 1, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/1/1584190559.9331942.pdf', 191, '1584190559.9331942.pdf', 'pdf', '2020-03-14 12:56:03', '2020-03-14 12:56:03'),
(124, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584191179.66974.jpg', 217, '1584191179.66974.jpg', 'jpg', '2020-03-14 13:06:20', '2020-03-14 13:06:20'),
(125, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584191246.0945039.doc', 217, '1584191246.0945039.doc', 'doc', '2020-03-14 13:07:26', '2020-03-14 13:07:26'),
(126, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584191257.507848.jpg', 217, '1584191257.507848.jpg', 'jpg', '2020-03-14 13:07:38', '2020-03-14 13:07:38'),
(127, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584191280.8735971.jpg', 224, '1584191280.8735971.jpg', 'jpg', '2020-03-14 13:08:01', '2020-03-14 13:08:01'),
(128, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584191288.061678.jpg', 224, '1584191288.061678.jpg', 'jpg', '2020-03-14 13:08:08', '2020-03-14 13:08:08'),
(129, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584191294.469939.doc', 224, '1584191294.469939.doc', 'doc', '2020-03-14 13:08:14', '2020-03-14 13:08:14'),
(130, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584191313.8153958.doc', 217, '1584191313.8153958.doc', 'doc', '2020-03-14 13:08:33', '2020-03-14 13:08:33'),
(131, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584191334.608381.jpg', 217, '1584191334.608381.jpg', 'jpg', '2020-03-14 13:08:54', '2020-03-14 13:08:54'),
(132, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584191350.848919.doc', 216, '1584191350.848919.doc', 'doc', '2020-03-14 13:09:10', '2020-03-14 13:09:10'),
(133, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584191368.239775.jpg', 216, '1584191368.239775.jpg', 'jpg', '2020-03-14 13:09:28', '2020-03-14 13:09:28'),
(134, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584191388.685968.jpg', 216, '1584191388.685968.jpg', 'jpg', '2020-03-14 13:09:49', '2020-03-14 13:09:49'),
(135, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584191418.063344.doc', 217, '1584191418.063344.doc', 'doc', '2020-03-14 13:10:18', '2020-03-14 13:10:18'),
(136, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584191443.12866.doc', 216, '1584191443.12866.doc', 'doc', '2020-03-14 13:10:43', '2020-03-14 13:10:43'),
(137, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584191507.6220632.doc', 216, '1584191507.6220632.doc', 'doc', '2020-03-14 13:12:48', '2020-03-14 13:12:48'),
(138, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584191619.7470388.doc', 216, '1584191619.7470388.doc', 'doc', '2020-03-14 13:13:39', '2020-03-14 13:13:39'),
(139, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584191651.597739.doc', 228, '1584191651.597739.doc', 'doc', '2020-03-14 13:14:11', '2020-03-14 13:14:11'),
(140, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584191708.976253.doc', 230, '1584191708.976253.doc', 'doc', '2020-03-14 13:15:09', '2020-03-14 13:15:09'),
(141, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584191909.277792.jpg', 234, '1584191909.277792.jpg', 'jpg', '2020-03-14 13:18:41', '2020-03-14 13:18:41'),
(142, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584192058.753011.jpg', 236, '1584192058.753011.jpg', 'jpg', '2020-03-14 13:20:59', '2020-03-14 13:20:59'),
(143, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584192085.002562.jpg', 237, '1584192085.002562.jpg', 'jpg', '2020-03-14 13:21:25', '2020-03-14 13:21:25'),
(144, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584192095.21019.doc', 237, '1584192095.21019.doc', 'doc', '2020-03-14 13:21:35', '2020-03-14 13:21:35'),
(146, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584192736.886369.jpg', 261, '1584192736.886369.jpg', 'jpg', '2020-03-14 13:32:17', '2020-03-14 13:32:17'),
(148, 1, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/1/1584339146.6223059.doc', 298, '1584339146.6223059.doc', 'doc', '2020-03-16 06:12:26', '2020-03-16 06:12:26'),
(149, 1, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/1/1584339154.513715.docx', 300, '1584339154.513715.docx', 'docx', '2020-03-16 06:12:34', '2020-03-16 06:12:34'),
(153, 75, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/75/BubbleInfo.java', 1, 'BubbleInfo.java', 'java', '2020-03-16 09:07:58', '2020-03-16 09:07:58'),
(154, 75, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/75/BubbleInfo.java', NULL, 'BubbleInfo.java', 'java', '2020-03-16 09:08:09', '2020-03-16 09:08:09'),
(155, 75, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/75/bubble_31x31.png', NULL, 'bubble_31x31.png', 'png', '2020-03-16 09:08:37', '2020-03-16 09:08:37'),
(156, 2, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/2/1584352101.963748.doc', 2, '1584352101.963748.doc', 'doc', '2020-03-16 09:48:22', '2020-03-16 09:48:22'),
(157, 2, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/2/1584352118.864633.jpg', 304, '1584352118.864633.jpg', 'jpg', '2020-03-16 09:48:40', '2020-03-16 09:48:40'),
(158, 75, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/75/bubble_20x20.png', NULL, 'bubble_20x20.png', 'png', '2020-03-16 10:05:05', '2020-03-16 10:05:05'),
(159, 2, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/2/1584353523.394391.jpg', 310, '1584353523.394391.jpg', 'jpg', '2020-03-16 10:12:04', '2020-03-16 10:12:04'),
(160, 4, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/4/sample.pdf', NULL, 'sample.pdf', 'pdf', '2020-03-16 10:12:04', '2020-03-16 10:12:04'),
(161, 2, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/2/1584353538.669385.jpg', 309, '1584353538.669385.jpg', 'jpg', '2020-03-16 10:12:19', '2020-03-16 10:12:19'),
(162, 2, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/2/1584353559.709641.doc', 308, '1584353559.709641.doc', 'doc', '2020-03-16 10:12:39', '2020-03-16 10:12:39'),
(163, 2, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/2/1584353578.125255.jpg', 306, '1584353578.125255.jpg', 'jpg', '2020-03-16 10:12:59', '2020-03-16 10:12:59'),
(164, 2, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/2/1584353594.4092588.doc', 306, '1584353594.4092588.doc', 'doc', '2020-03-16 10:13:14', '2020-03-16 10:13:14'),
(165, 2, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/2/1584353603.578961.doc', 308, '1584353603.578961.doc', 'doc', '2020-03-16 10:13:23', '2020-03-16 10:13:23'),
(166, 2, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/2/1584353635.920939.jpg', 309, '1584353635.920939.jpg', 'jpg', '2020-03-16 10:13:56', '2020-03-16 10:13:56'),
(167, 2, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/2/1584353642.397018.jpg', 309, '1584353642.397018.jpg', 'jpg', '2020-03-16 10:14:03', '2020-03-16 10:14:03'),
(168, 2, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/2/1584353657.531244.jpg', 310, '1584353657.531244.jpg', 'jpg', '2020-03-16 10:14:18', '2020-03-16 10:14:18'),
(169, 2, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/2/1584353663.860732.jpg', 310, '1584353663.860732.jpg', 'jpg', '2020-03-16 10:14:24', '2020-03-16 10:14:24'),
(170, 2, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/2/1584353669.574582.jpg', 310, '1584353669.574582.jpg', 'jpg', '2020-03-16 10:14:30', '2020-03-16 10:14:30'),
(171, 4, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/4/images_1582479771631.jpeg', NULL, 'images_1582479771631.jpeg', 'jpeg', '2020-03-16 10:15:09', '2020-03-16 10:15:09'),
(172, 75, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/75/bubble_62x62.png', 4, 'bubble_62x62.png', 'png', '2020-03-16 10:17:27', '2020-03-16 10:17:27'),
(193, 5, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/5/Text File.txt', 5, 'Text File.txt', 'txt', '2020-03-16 13:18:23', '2020-03-16 13:18:23'),
(195, 4, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/4/sample pdf 7.pdf', 373, 'sample pdf 7.pdf', 'pdf', '2020-03-17 05:07:17', '2020-03-17 05:07:17'),
(196, 4, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/4/images_1575013021919.jpeg', 376, 'images_1575013021919.jpeg', 'jpeg', '2020-03-17 05:08:15', '2020-03-17 05:08:15'),
(201, 4, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/4/sample pdf 7.pdf', 390, 'sample pdf 7.pdf', 'pdf', '2020-03-17 06:06:57', '2020-03-17 06:06:57'),
(203, 1, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/1/file-sample_500kB.docx', 1, 'file-sample_500kB.docx', 'docx', '2020-03-17 06:08:51', '2020-03-17 06:08:51'),
(204, 4, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/4/images (4)_1582479770868.jpeg', 391, 'images (4)_1582479770868.jpeg', 'jpeg', '2020-03-17 06:10:59', '2020-03-17 06:10:59'),
(206, 4, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/4/images_1582479771631.jpeg', 397, 'images_1582479771631.jpeg', 'jpeg', '2020-03-17 06:18:55', '2020-03-17 06:18:55'),
(207, 4, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/4/images (3).jpeg', 398, 'images (3).jpeg', 'jpeg', '2020-03-17 06:20:19', '2020-03-17 06:20:19'),
(208, 4, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/4/file-sample_100kB.doc', 398, 'file-sample_100kB.doc', 'doc', '2020-03-17 06:20:39', '2020-03-17 06:20:39'),
(209, 4, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/4/sample pdf 7.pdf', 399, 'sample pdf 7.pdf', 'pdf', '2020-03-17 06:21:43', '2020-03-17 06:21:43'),
(210, 4, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/4/25-tree-png-image.png', 399, '25-tree-png-image.png', 'png', '2020-03-17 06:22:01', '2020-03-17 06:22:01'),
(211, 4, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/4/file-sample_100kB.docx', 396, 'file-sample_100kB.docx', 'docx', '2020-03-17 06:22:37', '2020-03-17 06:22:37'),
(212, 4, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/4/IMG_20200315_151815.jpg', 398, 'IMG_20200315_151815.jpg', 'jpg', '2020-03-17 06:26:11', '2020-03-17 06:26:11'),
(213, 4, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/4/images_1575013021919.jpeg', 397, 'images_1575013021919.jpeg', 'jpeg', '2020-03-17 06:26:27', '2020-03-17 06:26:27'),
(214, 4, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/4/images_1575013021919.jpeg', 404, 'images_1575013021919.jpeg', 'jpeg', '2020-03-17 06:27:54', '2020-03-17 06:27:54'),
(217, 1, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/1/file-sample_100kB.doc', 1, 'file-sample_100kB.doc', 'doc', '2020-03-17 07:39:40', '2020-03-17 07:39:40'),
(218, 1, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/1/file-sample_150kB.pdf', 1, 'file-sample_150kB.pdf', 'pdf', '2020-03-17 07:40:19', '2020-03-17 07:40:19'),
(225, 1, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/1/IMG_0167.JPG', 1, 'IMG_0167.JPG', 'JPG', '2020-03-17 10:26:55', '2020-03-17 10:26:55'),
(226, 1, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/1/IMG_0168.PNG', 1, 'IMG_0168.PNG', 'PNG', '2020-03-17 10:29:12', '2020-03-17 10:29:12'),
(227, 1, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/1/IMG_0168.PNG', 416, 'IMG_0168.PNG', 'PNG', '2020-03-17 10:30:27', '2020-03-17 10:30:27'),
(228, 1, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/1/IMG_0167.JPG', 417, 'IMG_0167.JPG', 'JPG', '2020-03-17 10:30:59', '2020-03-17 10:30:59'),
(242, 4, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/4/images_1575013021919.jpeg', 4, 'images_1575013021919.jpeg', 'jpeg', '2020-03-18 06:46:29', '2020-03-18 06:46:29'),
(243, 4, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/4/images (4)_1582479770868.jpeg', 4, 'images (4)_1582479770868.jpeg', 'jpeg', '2020-03-18 06:50:15', '2020-03-18 06:50:15'),
(244, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/IMG_0167.JPG', 3, 'IMG_0167.JPG', 'JPG', '2020-03-18 06:55:34', '2020-03-18 06:55:34'),
(245, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/IMG_0168.PNG', 3, 'IMG_0168.PNG', 'PNG', '2020-03-18 07:00:00', '2020-03-18 07:00:00'),
(246, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/IMG_9981-1.jpg', 3, 'IMG_9981-1.jpg', 'jpg', '2020-03-18 07:00:14', '2020-03-18 07:00:14'),
(247, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/1584352101.963748.doc', 3, '1584352101.963748.doc', 'doc', '2020-03-18 07:00:27', '2020-03-18 07:00:27'),
(248, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/ios.doc', 3, 'ios.doc', 'doc', '2020-03-18 07:04:39', '2020-03-18 07:04:39'),
(249, 4, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/4/file-sample_100kB.doc', 4, 'file-sample_100kB.doc', 'doc', '2020-03-18 08:02:50', '2020-03-18 08:02:50'),
(250, 4, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/4/IMG_20200315_151815.jpg', 4, 'IMG_20200315_151815.jpg', 'jpg', '2020-03-18 08:28:09', '2020-03-18 08:28:09'),
(251, 4, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/4/images_1582479771631.jpeg', 4, 'images_1582479771631.jpeg', 'jpeg', '2020-03-18 09:05:39', '2020-03-18 09:05:39'),
(252, 4, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/4/images (2)_1582479771146.jpeg', 4, 'images (2)_1582479771146.jpeg', 'jpeg', '2020-03-18 09:07:11', '2020-03-18 09:07:11'),
(253, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/IMG_0167.JPG', 421, 'IMG_0167.JPG', 'JPG', '2020-03-18 11:52:36', '2020-03-18 11:52:36'),
(254, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/ios.doc', 421, 'ios.doc', 'doc', '2020-03-18 11:52:48', '2020-03-18 11:52:48'),
(255, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/IMG_0168.PNG', 422, 'IMG_0168.PNG', 'PNG', '2020-03-18 11:53:05', '2020-03-18 11:53:05'),
(256, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/IMG_0167.JPG', 424, 'IMG_0167.JPG', 'JPG', '2020-03-18 11:53:27', '2020-03-18 11:53:27'),
(257, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/IMG_0168.PNG', 424, 'IMG_0168.PNG', 'PNG', '2020-03-18 11:53:33', '2020-03-18 11:53:33'),
(258, 3, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/3/IMG_0167.JPG', 423, 'IMG_0167.JPG', 'JPG', '2020-03-18 12:43:17', '2020-03-18 12:43:17'),
(259, 4, 'http://15.206.38.116/clinicalwallet/public/uploads/documents/4/01 (20).jpg', 4, '01 (20).jpg', 'jpg', '2020-03-18 12:58:04', '2020-03-18 12:58:04');

-- --------------------------------------------------------

--
-- Table structure for table `terms_of_services`
--

CREATE TABLE `terms_of_services` (
  `id` int(255) NOT NULL,
  `text` text,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `terms_of_services`
--

INSERT INTO `terms_of_services` (`id`, `text`, `updated_at`, `created_at`, `deleted_at`) VALUES
(1, '<p><br></p><h3 style=\"margin: 15px 0px; padding: 0px; font-weight: 700; font-size: 14px; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif;\">Section 1.10.32 of \"de Finibus Bonorum et Malorum\", written by Cicero in 45 BC</h3><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px;\">\"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?\"</p><h3 style=\"margin: 15px 0px; padding: 0px; font-weight: 700; font-size: 14px; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif;\">1914 translation by H. Rackham</h3><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px;\">\"But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?\"</p><h3 style=\"margin: 15px 0px; padding: 0px; font-weight: 700; font-size: 14px; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif;\">Section 1.10.33 of \"de Finibus Bonorum et Malorum\", written by Cicero in 45 BC</h3><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px;\">\"At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.\"</p><h3 style=\"margin: 15px 0px; padding: 0px; font-weight: 700; font-size: 14px; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif;\">1914 translation by H. Rackham</h3><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px;\">\"On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammelled and when nothing prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided. But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pains.\"</p>', '2020-03-17 08:11:10', NULL, '2020-03-17 13:22:01');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `google_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `student_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `school` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `program` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zipcode` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `device_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `otp` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role_id` int(255) DEFAULT NULL,
  `is_approved` int(10) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `google_id`, `facebook_id`, `email_verified_at`, `password`, `phone`, `student_id`, `school`, `program`, `zipcode`, `user_token`, `remember_token`, `device_token`, `image`, `otp`, `role_id`, `is_approved`, `created_at`, `updated_at`) VALUES
(1, 'Monang', 'Champaneri', 'monangchampaneri.rs@gmail.com', '', '', NULL, '$2y$10$wHpjdxfYXaPqHjBlOJg5ROkWbVan8CElENf858MExref2Ulhc1MHG', '8866726599', '46', 'Marwadi', 'Mech', '380006', '113ee4df48d6c7628a13e728964b97baa2320d83ac83fd44967e825af496f0ff', NULL, '123456789', 'http://15.206.38.116/clinicalwallet/public/profile/images/1584534565.jfif', NULL, 2, 1, '2020-03-14 05:17:17', '2020-03-18 12:29:25'),
(2, 'bhargav', 'kachhot', 'bhargavkachhot.rs@gmail.com', '', '', NULL, '$2y$10$.DJgWrMDy49ScqrPW8bwgOUdOApsJ94.8M1rnI4cU2yD8KKPaSxk.', '7984327389', '46789', 'Marwadi', 'Mech', '380006', '4311f0884e5a8b95c03071bd3541f805eb7cea854487135985d743e92a27a5fc', NULL, 'qq', 'http://15.206.38.116/clinicalwallet/public/profile/images/1584537374.jfif', '3303', 2, 1, '2020-03-14 05:26:46', '2020-03-18 13:16:14'),
(3, 'Kia', 'K', 'khyatip.rs@gmail.com', '', '', NULL, '$2y$10$wrXtRKTs/VaaBkeo/HuiqOm7FW5RZzPU0RZ5ApRGjTbJV3cpUWT8m', '9979775508', '15', 'ks', 'IT', '380016', '581f3045b81e76d3b74da969df9d803d4526980b9e14b711fcafd809798586ef', NULL, '123456789', 'http://15.206.38.116/clinicalwallet/public/profile/images/1584535097.jpeg', '281193', 2, 1, '2020-03-14 05:53:28', '2020-03-18 12:56:02'),
(4, 'Happy Rs', 'Bhimani', 'happybhimani.rs@gmail.com', '', '', NULL, '$2y$10$eZ.ynBy22ksiqEOPNTQWXOHRYQgMxzqsJ9pGX/Htb4UFZ179gvLZK', '8530620472', '12', 'Allen school', 'Comp', '380006', '26d1e7fad2b15b17fdb946081b688c6f13cbc9481d9d7c73867fa9ea25691ecf', NULL, '123456', 'http://15.206.38.116/clinicalwallet/public/profile/images/1584534494.jpg', '963108', 2, 1, '2020-03-14 06:06:51', '2020-03-18 12:59:57'),
(5, 'Monang', 'Champaneri', 'macios@yopmail.com', '', '', NULL, '$2y$10$RdOwUnOd9SI05.mHd1eS/uVTRMMydMqXfZ8gmeIZsBlb5kF1mNeKK', '7041426599', 'MQC1234', 'Hello Test', 'Test1', '323232', '93e3597704f78b965d6bcf27325bf45a364fb986eeeaf053dc15e831b3aeeecb', NULL, '123456789', 'http://15.206.38.116/clinicalwallet/public/profile/images/1584534614.jfif', NULL, 2, 1, '2020-03-14 06:40:00', '2020-03-18 12:30:14'),
(6, 'Bhargav', 'Kachhot', 'bhargavkachhot.rs@gmail.coms', '', '', NULL, '$2y$10$4Xm3XwPxmJZifaaw22tPyuuVj7sJRfRPldaqO/Rw1FKnYom8IekAi', '7984327389', 'MQ123', 'AIS', 'iOS', '983682', 'yd4c0179cf7a0f2978cfdfd88', NULL, '123456789', 'http://15.206.38.116/clinicalwallet/public/profile/images/1584534601.jfif', NULL, 2, 0, '2020-03-14 09:19:15', '2020-03-18 12:30:01'),
(7, 'Bhargav', 'Kachhot', 'bhargavkachhot.rs@gmail.comrtrt', '', '', NULL, '$2y$10$IM5dQKzycNPzYHQqtGXcWeVs8bLFEtdJ7PNg4XHAmghoMT0JJS0ui', '7984327389', 'MQ123', 'AIS', 'iOS', '983682', 'Le9c00e58a5f5c7c28d02058d', NULL, '123456789', 'http://15.206.38.116/clinicalwallet/public/profile/images/1584451622.jpg', NULL, 2, 0, '2020-03-17 13:27:02', '2020-03-18 12:29:48'),
(9, 'Bhargav', 'Kachhot', 'bhargavkachhot.rs@gmail.comssssssssdfdfdf', NULL, NULL, NULL, '$2y$10$YuM2Pe8WqmiHNSv.RJh1uOoiGtXHighIwhJZydN9uJpAH2u0sogoC', '7984327389', 'MQ123', 'AIS', 'iOS', '983682', 't202033bc179aec68cd0cedbc', NULL, '123456789', 'http://15.206.38.116/clinicalwallet/public/profile/images/1584510245.jpg', NULL, 2, 0, '2020-03-18 05:44:05', '2020-03-18 05:44:05');

-- --------------------------------------------------------

--
-- Table structure for table `x_apis`
--

CREATE TABLE `x_apis` (
  `id` int(10) UNSIGNED NOT NULL,
  `api_key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `platform` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `x_apis`
--

INSERT INTO `x_apis` (`id`, `api_key`, `platform`, `created_at`, `updated_at`) VALUES
(1, 'qiqhyah5kBu9XdyNa1KQsMVhxR', 'android', '2018-08-28 00:00:00', '2018-08-28 00:00:00'),
(2, 'qiqhyah5kBu9XdyNa1KQsMVhxR', 'ios', '2018-08-28 00:00:00', '2018-08-28 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about_us`
--
ALTER TABLE `about_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Indexes for table `contact_us`
--
ALTER TABLE `contact_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `folders`
--
ALTER TABLE `folders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `folders_instructor`
--
ALTER TABLE `folders_instructor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `help`
--
ALTER TABLE `help`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `instructors`
--
ALTER TABLE `instructors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `instructor_documents`
--
ALTER TABLE `instructor_documents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `privacy_policy`
--
ALTER TABLE `privacy_policy`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `programs`
--
ALTER TABLE `programs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `schools`
--
ALTER TABLE `schools`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `students_documents`
--
ALTER TABLE `students_documents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `terms_of_services`
--
ALTER TABLE `terms_of_services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `x_apis`
--
ALTER TABLE `x_apis`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about_us`
--
ALTER TABLE `about_us`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `contact_us`
--
ALTER TABLE `contact_us`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `folders`
--
ALTER TABLE `folders`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=429;
--
-- AUTO_INCREMENT for table `folders_instructor`
--
ALTER TABLE `folders_instructor`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `help`
--
ALTER TABLE `help`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `instructors`
--
ALTER TABLE `instructors`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `instructor_documents`
--
ALTER TABLE `instructor_documents`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `password_resets`
--
ALTER TABLE `password_resets`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `privacy_policy`
--
ALTER TABLE `privacy_policy`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `programs`
--
ALTER TABLE `programs`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `schools`
--
ALTER TABLE `schools`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `students_documents`
--
ALTER TABLE `students_documents`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=260;
--
-- AUTO_INCREMENT for table `terms_of_services`
--
ALTER TABLE `terms_of_services`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `x_apis`
--
ALTER TABLE `x_apis`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
