<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/verify/email/{id}', 'Verifycontroller@Verify')->name('verify.email');
Route::get('/verify/email/instructor/{id}', 'Verifycontroller@VerifyInstructor')->name('verify.email.instructor');
// Route::get('/', function () {
//     return view('welcome');
// }); 
// Auth::routes(); 
// Route::get('/home', 'HomeController@index')->name('home');
 
    Route::view('/', 'auth.login');
    Route::view('/welcome', 'welcome');
    Auth::routes(); 
    Route::get('/login/admin', 'admin\LoginController@showAdminLoginForm'); 
    Route::get('/register/admin', 'admin\RegisterController@showAdminRegisterForm');  
    Route::post('/login/admin', 'admin\LoginController@adminLogin'); 
    Route::post('/register/admin', 'admin\RegisterController@createAdmin');
    // Route::view('/home', 'home')->middleware('auth')->name('home');
    Route::get('/home', 'HomeController@index')->name('home');
    Route::view('/admin', 'admin.students.admin'); 

// Admin Routes
    Route::get('/add/help', 'admin\HomeController@AddHelp')->name('add.help');
    Route::post('/store-help', 'admin\HomeController@StoreHelp')->name('store.help');
    Route::get('/add/terms-of-services', 'admin\HomeController@AddTermsOfServices')->name('add.terms.of.services');
    Route::post('/store-terms-of-services', 'admin\HomeController@StoreTermsOfServices')->name('store.terms.of.services');
    Route::get('/add/privacy-policy', 'admin\HomeController@AddPrivacyPolicy')->name('add.privacy.policy');
    Route::post('/store-privacy-policy', 'admin\HomeController@StorePrivacyPolicy')->name('store.privacy.policy');
    Route::get('/add/about-us', 'admin\HomeController@AddAboutUs')->name('add.about.us');
    Route::post('/store-about-us', 'admin\HomeController@StoreAboutUs')->name('store.about.us'); 

    Route::get('/admin/dashboard', 'admin\HomeController@Dashboard')->name('dashboard');
    Route::get('/admin/total_students', 'admin\StudentsController@TotalStudents')->name('total_students');
    Route::get('/admin/student/documents', 'admin\DocumentsController@Documents')->name('student.documents');
    Route::get('/admin/edit/student/{id}', 'admin\StudentsController@EditStudent')->name('edit.student');
    Route::post('/admin/update/student/{id}', 'admin\StudentsController@UpdateStudent')->name('update.student');
    Route::post('/admin/delete/student/{id}', 'admin\StudentsController@DeleteStudent')->name('delete.student');
    Route::post('/admin/delete/document/{id}', 'admin\DocumentsController@DeleteDocument')->name('delete.document');

    Route::get('/admin/total_instructors', 'admin\InstructorController@TotalInstructors')->name('total_instructors');
    Route::get('/admin/edit/instructor/{id}', 'admin\InstructorController@EditInstructor')->name('edit.instructor');
    Route::post('/admin/update/instructor/{id}', 'admin\InstructorController@UpdateInstructor')->name('update.instructor');
    Route::post('/admin/delete/instructor/{id}', 'admin\InstructorController@DeleteInstructor')->name('delete.instructor'); 

// Student Routes 


    Route::get('/students/change-password', 'HomeController@EditPassword')->name('student.edit-password');
    Route::post('/students/change-password/{id}', 'HomeController@UpdatePassword')->name('student.update-password');
    Route::post('/students/change-profile/{id}', 'HomeController@Updateprofile')->name('student.update-profile'); 

// Instructor Routes

     Route::get('/login/instructor', 'instructor\LoginController@showInstructorLoginForm');
     Route::post('/register/instructor', 'instructor\RegisterController@createInstructor');
     Route::post('/login/instructor', 'instructor\LoginController@InstructorLogin');
     Route::get('/register/instructor', 'instructor\RegisterController@showInstructorRegisterForm');
     // Route::view('/instructor', 'instructor.instructor');
     Route::get('/instructor', 'instructor\InstructorController@index')->name('instructor.home');
     Route::get('/instructor/competency-list', 'instructor\CompetencyController@CompetencyList')->name('competency.list');
     Route::get('/instructor/clinical_site_evaluation', 'instructor\EvaluationController@ClinicalSiteEvaluation')->name('clinical_site_evaluation');
     Route::get('/instructor/assigned-clinical-remediation', 'instructor\EvaluationController@AssignedClinicalRemediation')->name('assigned.clinical.remediation');
     Route::get('/instructor/journal', 'instructor\EvaluationController@Journal')->name('journal');
     Route::get('/instructor/summative-evaluation', 'instructor\EvaluationController@SummativeEvaluation')->name('summative_evaluation');
     Route::get('/instructor/add/summative-evaluation', 'instructor\EvaluationController@AddSummativeEvaluation')->name('add.summative_evaluation');
     Route::get('/instructor/GBC/form', 'instructor\EvaluationController@GBCForm')->name('GBC_form');
     Route::get('/instructor/GBC/form/add', 'instructor\EvaluationController@AddGBCForm')->name('add.GBC_location');
     Route::get('/instructor/Learning-Contract-Form', 'instructor\EvaluationController@LearningContractForm')->name('Learning.Contract.Form');
     Route::get('/instructor/reports', 'instructor\ReportsController@Reports')->name('instructor.student_activity_log');
     Route::get('/instructor/attendence', 'instructor\ReportsController@Attendence')->name('instructor.attendence');
     Route::get('/instructor/history', 'instructor\HistoryController@History')->name('instructor.history');
     Route::get('/instructor/students-list', 'instructor\StudentsController@students')->name('instructor.students');
     Route::get('/instructor/students/details/{id}', 'instructor\StudentsController@studentsDetailProfile')->name('instructor.students.detail_profile'); 
     Route::post('/instructor/delete/student/{id}', 'instructor\StudentsController@DeleteStudent')->name('delete.student_from_instructor_side'); 
     Route::post('/instructor/update/student/profile/{id}', 'instructor\StudentsController@Updateprofile')->name('update.student_from_instructor_side');



     Route::get('/instructor/rubric', 'instructor\RubricController@Rubric')->name('instructor.rubric');
     Route::get('/instructor/SBARR', 'instructor\SBARRController@SBARR')->name('instructor.SBARR');
     Route::get('/instructor/SBARR/details', 'instructor\SBARRController@SBARRDetails')->name('instructor.SBARR.details');
     Route::get('/instructor/profile', 'instructor\InstructorController@InstructorProfile')->name('instructor.profile');
     
     Route::post('/instructor/update/profile/{id}', 'instructor\InstructorController@UpdateInstructor')->name('update.instructor.profile');
     Route::get('/instructor/change-password/{id}', 'instructor\InstructorController@ChangePassword')->name('instructor.change.password');
     Route::post('/instructor/change-password/{id}', 'instructor\InstructorController@UpdatePassword')->name('instructor.update-password'); 

     Route::get('/student/learningcontractform', 'LearningContractController@LearningContract')->name('learningcontractform');
     Route::get('/student/documents', 'DocumentsController@Documents')->name('documents');
     Route::get('/GBC/location', 'GBCController@Location')->name('GBC_location');
     
     Route::get('/students/reports', 'StudentReportController@StudentReport')->name('student.reports');


     



      