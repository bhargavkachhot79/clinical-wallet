<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('/privacy-policy', 'Api\getApiController@PrivacyPolicy')->name('privacy.policy');
Route::get('/terms-of-services', 'Api\getApiController@TermsOfServices')->name('terms.of.services');
Route::get('/help', 'Api\getApiController@Help')->name('help');
Route::get('/about-us', 'Api\getApiController@AboutUs')->name('about.us');
Route::post('/contact-us', 'Api\getApiController@ContactUs')->name('contact.us');

Route::get('/metadata', 'Api\getApiController@MetaData')->name('metadata');
Route::post('user/sign-up', 'Api\getApiController@signup')->name('sign-up');
Route::post('user/login', 'Api\getApiController@login')->name('login');
Route::post('user/password/create', 'Api\getApiController@create')->name('create.password');
Route::post('user/otp-verification', 'Api\getApiController@otpVerification')->name('otpVerification');
Route::post('/user/change-password', 'Api\getApiController@ChangePassword')->name('change.password');
Route::post('/user/documents', 'Api\DocumentsController@DocumentList')->name('user.documents');
Route::post('/user/upload/documents', 'Api\DocumentsController@UploadDocument')->name('upload.document');
Route::post('/user/create/folder', 'Api\DocumentsController@CreateFolder')->name('create.folder');
// Route::post('/user/folders', 'Api\DocumentsController@FolderList')->name('user.folders'); 


Route::post('/user/profile', 'Api\ProfileController@UserProfile')->name('user.profile');
Route::post('/user/update/profile', 'Api\ProfileController@UpdateProfile')->name('user.update_profile');
Route::post('/user/delete/file', 'Api\DocumentsController@DeleteFile')->name('delete.file');




//instructor
Route::post('/students', 'Api\StudentsController@Students')->name('Student.list');
Route::post('/user/Document_profile', 'Api\ProfileController@UserDocumentProfile')->name('user.document.profile');


Route::post('/instructor/documents', 'Api\instructor\InstructorController@DocumentList')->name('instructor.documents');
Route::post('/instructor/create/folder', 'Api\instructor\DocumentsController@CreateFolder')->name('create.folder_instructor');
Route::post('/instructor/upload/documents', 'Api\instructor\DocumentsController@UploadDocument')->name('upload.document_instructor');
Route::post('/instructor/delete/file', 'Api\instructor\DocumentsController@DeleteFile')->name('delete.file_instructor'); 
