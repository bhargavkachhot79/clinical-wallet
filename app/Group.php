<?php

namespace App;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $table = 'term_group';
    
     public function User()
    {
        return $this->hasMany(User::class); 
    }
}
