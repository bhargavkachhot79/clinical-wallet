<?php

namespace App;
use App\Document;
use App\Term;
use App\Group;
use App\Prog;
use App\School;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Hash;
class User extends Authenticatable
{
    protected $table = 'users';
    use Notifiable;
    public static function IsValidCredentials($email,$password) {
        $results = User::select('password')->where('email', '=', $email)->first();
        if ($results != null)
            return Hash::check($password, $results->password) ? true : false;
        else
            return false;
    }

    public function Document()
    {
        return $this->hasMany(Document::class);
        return $this->belongsTo(Document::class);
    }
    public function Term()
    {
        return $this->belongsTo(Term::class); 
    }
     public function Group()
    {
        return $this->belongsTo(Group::class); 
    }
    public function Prog()
    {
        return $this->belongsTo(Prog::class); 
    }
     public function School()
    {
        return $this->belongsTo(School::class); 
    }
          
}
