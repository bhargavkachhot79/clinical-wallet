<?php

namespace App;
use App\School;
use Illuminate\Database\Eloquent\Model;

class Program extends Model
{
	protected $table = 'programs'; 
    public function School()
    {
        return $this->belongsTo(School::class); 
    }
}
