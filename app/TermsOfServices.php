<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TermsOfServices extends Model
{
    protected $table = 'terms_of_services';
}
