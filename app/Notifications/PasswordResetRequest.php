<?php
namespace App\Notifications;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Auth; 
use App\User;
use App\Instructor;

class PasswordResetRequest extends Notification implements ShouldQueue
{
    use Queueable;
    protected $token;
    /**
    * Create a new notification instance.
    *
    * @return void
    */
    public function __construct($token)
    {
        $this->token = $token;
    }
    /**
    * Get the notification's delivery channels.
    *
    * @param  mixed  $notifiable
    * @return array
    */
    public function via($notifiable)
    {
        return ['mail'];
    }
     /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */

     public function toMail($notifiable)
     {
        //print_r($this->passwordReset->id);die;

        $random_number = rand(1000,10000);
        $auth_user = Auth::user(); 
        if ($notifiable->role_id == 1) {
            $user = Instructor::where('id',$notifiable->id)->update(['otp' => $random_number]);
        }else{
            $user = User::where('id',$notifiable->id)->update(['otp' => $random_number]); 
        }
        
        $url = url('/api/password/change-password/'.$this->token);
        return (new MailMessage)
            ->line('You are receiving this email because we        received a password reset request for your account.')
            ->line('You one time verification code is - '.$random_number.' .'); 
    }
    /**
    * Get the array representation of the notification.
    *
    * @param  mixed  $notifiable
    * @return array
    */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}