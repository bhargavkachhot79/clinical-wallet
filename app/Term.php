<?php

namespace App;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Term extends Model
{
    protected $table = 'terms';

     public function User()
    {
        return $this->hasMany(User::class); 
    }
}
 