<?php

namespace App;
use App\User;
use App\Instructor;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    protected $table = 'students_documents';

     public function User()
    {
        return $this->belongsTo(User::class);
        return $this->hasMany(User::class);
    }
     public function Instructor()
    {
        return $this->belongsTo(Instructor::class);
        return $this->hasMany(Instructor::class);
    }
}
