<?php

namespace App;
use App\Program;
use App\User;
use Illuminate\Database\Eloquent\Model;

class School extends Model
{
    protected $table = 'schools';
    public function Program()
    {
        return $this->hasMany(Program::class); 
    }
    public function User()
    {
        return $this->hasMany(User::class); 
    }
}
