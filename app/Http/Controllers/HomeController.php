<?php

namespace App\Http\Controllers; 
use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Prog;
use App\Code;
use App\Term;
use App\School;
use Illuminate\Support\Facades\Input;

class HomeController extends Controller
{ 
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    { 
        $id=Auth::id();
        $payment_details = Code::where('student_id',$id)->first();
        $term = User::with('term')->where('id',$id)->first(); 
        $group = User::with('group')->where('id',$id)->first();
        $program = User::with('prog')->where('id',$id)->first();
        $school_name = User::with('school')->where('id',$id)->first(); 
        return view('home',compact('id','payment_details','term','group','program','school_name'));
    }

     public function EditPassword()
    { 
        $id=Auth::id(); 
        return view('student.changepassword',compact('id'));
    }

    public function UpdatePassword( Request $request,$id)
    {  
        $validatedData = $request->validate([  
            'password'  => 'required|same:confirm_password|min:6',  
        ]); 
        $student = User::where('id',$id)->update([ 
           'password' => bcrypt($request->password), 
        ]);
        $id=Auth::id();
         return redirect()->route('home', ['id' => $id])->with('status', 'Your Password changed sucessfully!'); 
    }

    public function Updateprofile(Request $request ,$id)
    {  
        $validatedData = $request->validate([ 
            'first_name' => 'required',
            'last_name' => 'required',
            'student_id' => 'required',
            'phone' => 'required',
            'email' => 'required',    
        ]);

        $image = $request->file('fileupload');
        if($image){ 
             $name = time().'.'.$image->getClientOriginalExtension();
             $destinationPath = public_path('/profile/images');
             $image->move($destinationPath, $name);
             $image_name =   "http://15.206.38.116/clinicalwallet/public/profile/images/".$name;
             $student = User::where('id',$id)->update([ 
                  'image' => $image_name,  
              ]);
        }  
        $student = User::where('id',$id)->update([
              'first_name' => $request->first_name,
              'last_name' => $request->last_name,
              'student_id' => $request->student_id,
              'phone' => $request->phone,
              'email' => $request->email,   
        ]); 
        $id=Auth::id();
        return redirect()->route('home', ['id' => $id])->with('status', 'Your profile updated sucessfully!'); 
    }
    
    
     
}
