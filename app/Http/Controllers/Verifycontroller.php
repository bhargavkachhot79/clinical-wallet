<?php

namespace App\Http\Controllers;
use App\Instructor;
use Illuminate\Http\Request;
use App\User;

class Verifycontroller extends Controller
{
    public function Verify($id)
    {
        $users = User::where('id',$id)->update(['is_approved' => 1]);        
        return view('forgotPassword.mail_verification');
    }
    public function VerifyInstructor($id)
    {
        $users = Instructor::where('id',$id)->update(['is_approved' => 1]);        
        return view('forgotPassword.mail_verification');
    }
    

}
