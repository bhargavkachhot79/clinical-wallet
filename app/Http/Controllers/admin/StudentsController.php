<?php

namespace App\Http\Controllers\admin; 
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\User;

class StudentsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    
    public function TotalStudents()
    {
    	$students = User::orderBy('created_at', 'desc')->get(); 
        return view('admin.students.index',compact('students'));
    }

    public function EditStudent($id)
    { 
        $student = User::where('id',$id)->first();
        return view('admin.students.edit_students',['student' => $student]); 
    }

    public function UpdateStudent(Request $request , $id)
    { 
        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'phone' => 'required',
            'zipcode' => 'required',
            'school' => 'required',
            'program' => 'required',
            'student_id' => 'required',
        ]); 
        $user = User::where('id',$id)->update([ 
            'first_name'=> $request->first_name,
            'last_name' => $request->last_name,
            'phone' => $request->phone,
            'zipcode'=> $request->zipcode,
            'program' => $request->program,
            'school' => $request->school, 
            'student_id' => $request->student_id, 
        ]); 
        $image = $request->file('image'); 
        if($image){ 
             $name = time().'.'.$image->getClientOriginalExtension();
             $destinationPath = public_path('/profile/images');
             $image->move($destinationPath, $name);
             $image_name =   "http://15.206.38.116/clinicalwallet/public/profile/images/".$name;
             $student = User::where('id',$id)->update([ 
                  'image' => $image_name,  
              ]);
        } 
        return redirect()->route('total_students')->with('status', 'Student profile updated sucessfully!'); 
    }

    public function DeleteStudent($id)
    { 
        $user = User::where('id',$id)->delete(); 
        return redirect()->route('total_students', ['id' => $id])->with('status', 'Student deleted sucessfully!'); 
    }
}
