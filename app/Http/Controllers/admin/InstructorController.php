<?php

namespace App\Http\Controllers\admin; 
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Instructor;
use App\Document; 

class InstructorController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    
    public function TotalInstructors()
    {
    	$instructors = Instructor::orderBy('created_at', 'desc')->get(); 
        return view('admin.instructors.index',compact('instructors'));
    }

    public function EditInstructor($id)
    { 
        $instructor = Instructor::where('id',$id)->first();
        return view('admin.instructors.edit_instructors',['instructor' => $instructor]); 
    }

    public function UpdateInstructor(Request $request , $id)
    { 
        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'phone' => 'required',
            'zipcode' => 'required',
            'school' => 'required',
            'program' => 'required',
            'student_id' => 'required',
        ]); 
        $user = Instructor::where('id',$id)->update([ 
            'first_name'=> $request->first_name,
            'last_name' => $request->last_name,
            'phone' => $request->phone,
            'zipcode'=> $request->zipcode,
            'program' => $request->program,
            'school' => $request->school, 
            'student_id' => $request->student_id, 
        ]);
        
        $image = $request->file('image'); 
        if($image){ 
            $name = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/profile/images');
            $image->move($destinationPath, $name);
            $image_name =   "http://15.206.38.116/clinicalwallet/public/profile/images/".$name;
            $student = Instructor::where('id',$id)->update([ 
                  'image' => $image_name,  
            ]);
        } 
        return redirect()->route('total_instructors')->with('status', 'Instructor profile updated sucessfully!'); 
    }

    public function DeleteInstructor($id)
    { 
        $user = Instructor::where('id',$id)->delete(); 
        return redirect()->route('total_instructors', ['id' => $id])->with('status', 'Instructor deleted sucessfully!'); 
    }
}
