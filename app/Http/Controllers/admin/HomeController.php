<?php

namespace App\Http\Controllers\admin; 
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\User;
use App\Instructor;
use App\Document;
use App\Help;
use App\TermsOfServices;
use App\PrivacyPolicy;
use App\AboutUs;

class HomeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }


    public function Dashboard()
    {
    	$total_students = User::get();
    	$total_instructors = Instructor::get();
    	$total_documents = Document::get(); 
        return view('admin.students.admin',compact('total_students','total_instructors','total_documents'));
    }  

// Help 


    public function AddHelp(Request $request)
    {  
        $description = Help::pluck('text')->first(); 
        return view('help',['description'=>$description]);   
    }

    public function StoreHelp(Request $request)
    {       
        $description = Help::where('id',1)->update([ 
                            'text'=> $request->get('description'), 
                        ]);
        return redirect()->route('add.help');
    }


// Terms Of Services


    public function AddTermsOfServices(Request $request)
    {  
        $description = TermsOfServices::pluck('text')->first(); 
        return view('terms_of_services',['description'=>$description]);   
    }

    public function StoreTermsOfServices(Request $request)
    {       
        $description = TermsOfServices::where('id',1)->update([ 
                            'text'=> $request->get('description'), 
                        ]);
        return redirect()->route('add.terms.of.services');
    }

// Privacy Policy


    public function AddPrivacyPolicy(Request $request)
    {  
        $description = PrivacyPolicy::pluck('text')->first(); 
        return view('privacypolicy',['description'=>$description]);   
    }

    public function StorePrivacyPolicy(Request $request)
    {     
        $description = PrivacyPolicy::where('id',1)->update([ 
                            'text'=> $request->get('description'), 
                        ]);
        return redirect()->route('add.privacy.policy');
    }

// About Us

    public function AddAboutUs(Request $request)
    {  
        $description = AboutUs::pluck('text')->first(); 
        return view('about_us',['description'=>$description]);   
    }

    public function StoreAboutUs(Request $request)
    {       
        $description = AboutUs::where('id',1)->update([ 
                            'text'=> $request->get('description'), 
                        ]);
        return redirect()->route('add.about.us');
    }


}
