<?php

namespace App\Http\Controllers\admin; 
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\User;
use App\Document;

class DocumentsController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    
    public function Documents()
    {
    	$documents = Document::with('user')->orderBy('created_at', 'desc')->get();  
        return view('admin.students.documents',compact('documents'));
    }
    
     public function DeleteDocument($id)
    { 
        $document = Document::where('id',$id)->delete(); 
        return redirect()->route('student.documents', ['id' => $id])->with('status', 'Document deleted sucessfully!'); 
    }
}
