<?php

namespace App\Http\Controllers\instructor;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CompetencyController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth:instructor');
    }
    public function CompetencyList()
    {
    	
        return view('instructor.competency_list.competency_list');
    }
   
    
}
