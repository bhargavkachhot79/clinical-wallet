<?php

namespace App\Http\Controllers\instructor;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class ReportsController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth:instructor');
    }
     public function Reports()
    {
        return view('instructor.reports.student_activity_log');
    }
     public function Attendence()
    {
        return view('instructor.reports.attendence');
    }
    
}
