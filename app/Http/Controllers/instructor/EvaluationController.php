<?php

namespace App\Http\Controllers\instructor;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class EvaluationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:instructor');
    }
     public function ClinicalSiteEvaluation()
    {
        return view('instructor.evaluation.clinical_site_evaluation');
    }
    public function AssignedClinicalRemediation()
    {
        return view('instructor.evaluation.assigned_clinical_remediation');
    }
    public function Journal()
    {
        return view('instructor.evaluation.journal');
    }
    public function SummativeEvaluation()
    {
        return view('instructor.evaluation.summative_evaluation');
    }
    public function AddSummativeEvaluation()
    {
        return view('instructor.evaluation.add_summative_evaluation');
    }
     public function GBCForm()
    {
        return view('instructor.evaluation.GBC_form');
    }
    public function AddGBCForm()
    {
        return view('student.GBC.location');
    }
    public function LearningContractForm()
    {
        return view('instructor.evaluation.learning_contract_form');
    }
    
    
}
