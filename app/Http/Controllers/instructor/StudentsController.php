<?php

namespace App\Http\Controllers\instructor;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

class StudentsController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth:instructor');
    }
     public function Students()
    {
        $students = User::get();
        return view('instructor.students.students',['students'=>$students]);
    }
     public function studentsDetailProfile($id)
    { 
        $student = User::where('id',$id)->first(); 
        return view('instructor.students.detail_profile',['student'=>$student]);
    } 

    public function Updateprofile(Request $request ,$id)
    {   
        $validatedData = $request->validate([ 
            'first_name' => 'required',
            'last_name' => 'required',
            'student_id' => 'required',
            'phone' => 'required',    
        ]); 
             $image = $request->file('image'); 
             if($image){ 
                 $name = time().'.'.$image->getClientOriginalExtension();
                 $destinationPath = public_path('/profile/images');
                 $image->move($destinationPath, $name);
                 $image_name =   "http://15.206.38.116/clinicalwallet/public/profile/images/".$name;
                 $student = User::where('id',$id)->update([ 
                      'image' => $image_name,  
                  ]);
             }  
             $student = User::where('id',$id)->update([
                  'first_name' => $request->first_name,
                  'last_name' => $request->last_name,
                  'student_id' => $request->student_id,
                  'phone' => $request->phone,  
              ]);  
         return redirect()->route('instructor.students.detail_profile', ['id' => $id])->with('status', 'Your profile updated sucessfully!'); 
    }

    public function DeleteStudent($id)
    { 
        $user = User::where('id',$id)->delete(); 
        return redirect()->route('instructor.students')->with('status', 'Student deleted sucessfully!'); 
    }
}
