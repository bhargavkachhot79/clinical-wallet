<?php

namespace App\Http\Controllers\instructor;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class HistoryController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth:instructor');
    }
    public function History()
    {
        return view('instructor.history.history');
    }
}
