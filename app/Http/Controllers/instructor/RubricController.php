<?php

namespace App\Http\Controllers\instructor;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class RubricController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:instructor');
    }
     public function Rubric()
    {
        return view('instructor.rubric.rubric');
    }
}
