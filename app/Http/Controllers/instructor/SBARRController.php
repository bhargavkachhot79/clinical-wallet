<?php

namespace App\Http\Controllers\instructor;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class SBARRController extends Controller
{
   	public function __construct()
    {
        $this->middleware('auth:instructor');
    }
    public function SBARR()
    {
        return view('instructor.SBARR.SBARR');
    }
    public function SBARRDetails()
    {
        return view('instructor.SBARR.SBARR_details');
    }
}
