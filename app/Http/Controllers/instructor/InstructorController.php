<?php

namespace App\Http\Controllers\instructor;
use App\Http\Controllers\Controller;
use Auth;
use App\Instructor;
use Illuminate\Http\Request;

class InstructorController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:instructor');
    }
    public function index()
    { 
        $id=Auth::id();
        return view('instructor.instructor',compact('id'));
    }
    public function InstructorProfile()
    {
        return view('instructor.profile');
    }
    public function ChangePassword($id)
    {
        return view('instructor.change_password',['id'=>$id]);
    }
    public function UpdateInstructor(Request $request , $id)
    { 
        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'phone' => 'required',
            'zipcode' => 'required',
            'school' => 'required',
            'program' => 'required',
            'student_id' => 'required',
        ]); 
        $user = Instructor::where('id',$id)->update([ 
            'first_name'=> $request->first_name,
            'last_name' => $request->last_name,
            'phone' => $request->phone,
            'zipcode'=> $request->zipcode,
            'program' => $request->program,
            'school' => $request->school, 
            'student_id' => $request->student_id, 
        ]);
        $image = $request->file('image'); 
        if($image){ 
             $name = time().'.'.$image->getClientOriginalExtension();
             $destinationPath = public_path('/profile/images');
             $image->move($destinationPath, $name);
             $image_name =   "http://15.206.38.116/clinicalwallet/public/profile/images/".$name;
             $student = Instructor::where('id',$id)->update([ 
                  'image' => $image_name,  
              ]);
        } 
        return redirect()->route('instructor.home')->with('status', 'Instructor profile updated sucessfully!'); 
    }

    
    public function UpdatePassword( Request $request,$id)
    {  
        $validatedData = $request->validate([  
            'password'  => 'required|same:confirm_password|min:6',  
        ]); 
        $student = Instructor::where('id',$id)->update([ 
           'password' => bcrypt($request->password), 
        ]); 
        return redirect()->route('instructor.home')->with('status', 'Your Password changed sucessfully!'); 
    } 
    
}
