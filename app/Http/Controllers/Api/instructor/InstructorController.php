<?php

 
namespace App\Http\Controllers\Api\instructor;
use Illuminate\Support\Facades\Input;
use Illuminate\Contracts\Hashing\Hasher as HasherContract;
use Illuminate\Support\Str;
use Illuminate\Database\Query\Builder; 
use Route;
use File; 
use Response;
use App\Document;
use App\User;
use App\Http\Controllers\Controller; 
use Validator;
use DB;
use App\XApi;
use Hash;
use Mail;
use App\Folder; 
use App\Notifications\MailVerification;
use App\Notifications\ResendOTP; 
use Illuminate\Http\Request; 
use App\PasswordReset;
use App\Notifications\PasswordResetRequest;
use App\Notifications\PasswordResetSuccess;
use Illuminate\Support\Facades\Storage;

class InstructorController extends Controller
{
    public function __construct() {
        //API log code.
        $headers = apache_request_headers();
        $file = 'apilog.txt';
        list($controller , $action) = explode('@', Route::getCurrentRoute()->getActionName()); 
        if(file_exists($file)) {
            $data        = '';
            $info        = $headers;
            $headInfo    = http_build_query($info,'', ',');
            list($controller , $action) = explode('@', Route::getCurrentRoute()->getActionName()); 
            $requestData = http_build_query(Input::all(), '', ',');
            $date        = date('d-m-Y H:i:s');
            $data        = 'Date : '.$date.' | Controller : '.$controller.' | Action : '.$action.' | Request  Parameters :'.$requestData.' | Header Information :'.$headInfo;
            if ( 0 != filesize( $file )) {
                $data = "\r\n".$data;
            }
            $handle      = fopen($file, 'a') or die('Cannot open file:  '.$file);
            fwrite($handle, $data);
            fclose($handle);
        } 
    }

    public function Append($log_file, $value)
    {   
        File::append($log_file, $value . "\r\n");
    }

    public function LogInput(Request $request)
    {
        $log_file = storage_path() . '/logs/api' . date('Ymd') . '.log';
        $headers = apache_request_headers(); 
        $this->Append($log_file,'----------------' . debug_backtrace()[1]['function'] . ' --------------');
        $this->Append($log_file,'Request Info : ');
        $this->Append($log_file, 'Date: ' . date('Y-m-d H:i:s') . 'IP:' . $request->ip());
        $this->Append($log_file, 'User-Agent: ' . $request->header('User-Agent'));
        $this->Append($log_file, 'URL: ' . $request->url());
        $this->Append($log_file,'Input Parameters: ' .  json_encode(Input::all()));
        $this->Append($log_file,'Headers Parameters: ' .  json_encode($headers));
        $this->Append($log_file,'-----------');
        return;
    }
    public function LogOutput($output)
    {
        $log_file = storage_path() . '/logs/api' . date('Ymd') . '.log';
        $this->Append($log_file, 'Output: ');
        $this->Append($log_file,$output);
        $this->Append($log_file,'--------------------END------------------------');
        $this->Append($log_file,'');
        return;
    }

    public function DocumentList(Request $request){
        $this->LogInput($request); 
        $errors_array = array();
        $headers = apache_request_headers();
        $validator = Validator::make($request->all(), []);

        if ( $validator->fails()){
            $this->LogOutput(Response::json(array('status'=>500,'message' => $validator->errors())));
            return Response::json(array('status'=>500,'message' => $validator->errors()));
        }else{  
            if ($request->filter_by == "my_session"){
                        if (!isset($request->page)){
                            $request->page  = 1;
                        }
                        $page = $request->page * 10; 
                        $document = Document::where('instructor_id',$headers['id'])->offset($page - 10)->limit($page)->orderBy('id', 'desc')->get();  
                        if($document){  
                            $this->LogOutput(Response::json(array('status'=>200,'message' => 'Document list','data' => $document )));
                            return Response::json(array('status'=>200,'message' => 'Document list','data'=>$document ));
                        }else{  
                            $this->LogOutput(Response::json(array('status'=>500,'message' => 'Error No Document found!','data' => $document)));
                            return Response::json(array('status'=>500,'message' => 'Error No Document found!','data' => $document));
                        }
            }else if($request->filter_by == "group_by"){
                          if (!isset($request->page)){
                            $request->page  = 1;
                        }
                        $page = $request->page * 10; 
                        $document = Document::where('instructor_id',$headers['id'])->where('group_id',$request->group_id)->offset($page - 10)->limit($page)->orderBy('id', 'desc')->get();  
                        if($document){  
                            $this->LogOutput(Response::json(array('status'=>200,'message' => 'Document list','data' => $document )));
                            return Response::json(array('status'=>200,'message' => 'Document list','data'=>$document ));
                        }else{  
                            $this->LogOutput(Response::json(array('status'=>500,'message' => 'Error No Document found!','data' => $document)));
                            return Response::json(array('status'=>500,'message' => 'Error No Document found!','data' => $document));
                        }
            }else{
                         if (!isset($request->page)){
                            $request->page  = 1;
                        }
                        $page = $request->page * 10; 
                        $document = Document::where('instructor_id',$headers['id'])->offset($page - 10)->limit($page)->orderBy('id', 'desc')->get();  
                        if($document){  
                            $this->LogOutput(Response::json(array('status'=>200,'message' => 'Document list','data' => $document )));
                            return Response::json(array('status'=>200,'message' => 'Document list','data'=>$document ));
                        }else{  
                            $this->LogOutput(Response::json(array('status'=>500,'message' => 'Error No Document found!','data' => $document)));
                            return Response::json(array('status'=>500,'message' => 'Error No Document found!','data' => $document));
                        }
            }
        }
    } 
}
