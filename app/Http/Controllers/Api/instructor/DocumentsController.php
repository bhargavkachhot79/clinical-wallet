<?php
namespace App\Http\Controllers\Api\instructor;
use Illuminate\Support\Facades\Input;
use Illuminate\Contracts\Hashing\Hasher as HasherContract;
use Illuminate\Support\Str;
use Illuminate\Database\Query\Builder; 
use Route;
use File; 
use Response;
use App\InstructorDocuments;
use App\User;
use App\Http\Controllers\Controller; 
use Validator;
use DB;
use App\XApi;
use Hash;
use Mail;
use App\InstructorFoler; 
use App\Notifications\MailVerification;
use App\Notifications\ResendOTP; 
use Illuminate\Http\Request; 
use App\PasswordReset;
use App\Notifications\PasswordResetRequest;
use App\Notifications\PasswordResetSuccess;
use Illuminate\Support\Facades\Storage;

class DocumentsController extends Controller
{
      public function __construct() {
        //API log code.
        $headers = apache_request_headers();
        $file = 'apilog.txt';
        list($controller , $action) = explode('@', Route::getCurrentRoute()->getActionName()); 
        if(file_exists($file)) {
            $data        = '';
            $info        = $headers;
            $headInfo    = http_build_query($info,'', ',');
            list($controller , $action) = explode('@', Route::getCurrentRoute()->getActionName()); 
            $requestData = http_build_query(Input::all(), '', ',');
            $date        = date('d-m-Y H:i:s');
            $data        = 'Date : '.$date.' | Controller : '.$controller.' | Action : '.$action.' | Request  Parameters :'.$requestData.' | Header Information :'.$headInfo;
            if ( 0 != filesize( $file )) {
                $data = "\r\n".$data;
            }
            $handle      = fopen($file, 'a') or die('Cannot open file:  '.$file);
            fwrite($handle, $data);
            fclose($handle);
        } 
    }

    public function Append($log_file, $value)
    {   
        File::append($log_file, $value . "\r\n");
    }

    public function LogInput(Request $request)
    {
        $log_file = storage_path() . '/logs/api' . date('Ymd') . '.log';
        $headers = apache_request_headers(); 
        $this->Append($log_file,'----------------' . debug_backtrace()[1]['function'] . ' --------------');
        $this->Append($log_file,'Request Info : ');
        $this->Append($log_file, 'Date: ' . date('Y-m-d H:i:s') . 'IP:' . $request->ip());
        $this->Append($log_file, 'User-Agent: ' . $request->header('User-Agent'));
        $this->Append($log_file, 'URL: ' . $request->url());
        $this->Append($log_file,'Input Parameters: ' .  json_encode(Input::all()));
        $this->Append($log_file,'Headers Parameters: ' .  json_encode($headers));
        $this->Append($log_file,'-----------');
        return;
    }
    public function LogOutput($output)
    {
        $log_file = storage_path() . '/logs/api' . date('Ymd') . '.log';
        $this->Append($log_file, 'Output: ');
        $this->Append($log_file,$output);
        $this->Append($log_file,'--------------------END------------------------');
        $this->Append($log_file,'');
        return;
    }

    public function CreateFolder(Request $request){
        $this->LogInput($request); 
        $errors_array = array();
        $headers = apache_request_headers();  
            if(!$request->folder_name){
                $folder_name = "New Folder";
            }else{
                $folder_name = $request->folder_name;
            }
            if(!$request->parent_folder_id){
                $parent_folder_id =$headers['id'];
            }else{
                $parent_folder_id = $request->parent_folder_id;
            } 
            if($folder_name || $parent_folder_id){  
                $document = new InstructorFoler(); 
                $document->user_id = $headers['id'];
                $document->parent_folder_id  = $parent_folder_id; 
                $document->folder_name = $folder_name;
                $document->filetype = "folder";   
                $document->save(); 
                $this->LogOutput(Response::json(array('status'=>200,'message' => 'Folder Created sucessfully')));
                return Response::json(array('status'=>200,'message' => 'Folder Created sucessfully'));
            }else{  
                $this->LogOutput(Response::json(array('status'=>500,'message' => 'Error while creating the folder!')));
                return Response::json(array('status'=>500,'message' => 'Error while creating the folder!'));
            } 
    }

    public function UploadDocument(Request $request){
        $this->LogInput($request); 
        $errors_array = array();
        $headers = apache_request_headers();
        $validator = Validator::make($request->all(), []); 

        if ( $validator->fails()){
            $this->LogOutput(Response::json(array('status'=>500,'message' => $validator->errors())));
            return Response::json(array('status'=>500,'message' => $validator->errors()));
        }else{  
            $files = $request->file('document'); 
            foreach ($files as $key_answer => $file) {
                $originalName= $file->getClientOriginalName(); 
                $extension= $file->getClientOriginalExtension(); 
                $file->move(public_path('uploads/instructor/' . $headers['id'] . '/'), $originalName);

                 $is_file = InstructorDocuments::where('user_id',$headers['id'])->where('parent_folder_id',$request->parent_folder_id)->where('filename',$originalName)->get();
                 
                 if(count($is_file)){
                   $this->LogOutput(Response::json(array('status'=>500,'message' => 'File with ['. $originalName . ']is alredy exist please rename your file to continue.')));
                   return Response::json(array('status'=>500,'message' => 'File with ['.$originalName.']  is already exist please rename your file to continue.'));
                }else{

                $document = new InstructorDocuments(); 
                $document->user_id = $headers['id'];
                $document->parent_folder_id = $request->parent_folder_id;
                $document->filename = $originalName;
                $document->filetype = $extension;
                $document->document = "http://15.206.38.116/clinicalwallet/public/uploads/instructor/" . $headers['id'] . '/'.$originalName; 
                $document->save(); 
                }  
            } 
                $this->LogOutput(Response::json(array('status'=>200,'message' => 'Document Uploaded sucessfully')));
                return Response::json(array('status'=>200,'message' => 'Document Uploaded sucessfully')); 
        }
    } 

    public function DocumentList(Request $request){
        $this->LogInput($request); 
        $errors_array = array();
        $headers = apache_request_headers();
        $validator = Validator::make($request->all(), []); 
        if ( $validator->fails()){
            $this->LogOutput(Response::json(array('status'=>500,'message' => $validator->errors())));
            return Response::json(array('status'=>500,'message' => $validator->errors()));
        }else{  
            if (!isset($request->page)){
                    $request->page  = 1;
                }
                $page = $request->page * 10; 
                $document = InstructorDocuments::where('user_id',$headers['id'])->where('parent_folder_id',$request->parent_folder_id)->get(); 
                $folder = InstructorFoler::where('user_id',$headers['id'])->where('parent_folder_id',$request->parent_folder_id)->offset($page - 10)->limit($page)->orderBy('id', 'desc')->get();  
                    if(count($folder) < 10){
                        $merged = $document->merge($folder);
                        $document = $merged->all();
                    }else{
                        $document = $folder;
                    }
           // $current_folder_id = Folder::where('user_id',$headers['id'])->where('id',$request->parent_folder_id)->pluck('parent_folder_id')->first(); 
           $previous_folder = InstructorFoler::where('user_id',$headers['id'])->where('id',$request->parent_folder_id)->pluck('parent_folder_id')->first();  
	            if($document){  
		            $this->LogOutput(Response::json(array('status'=>200,'message' => 'Document list','previous_folder_id'=>$previous_folder,'data' => $document )));
		            return Response::json(array('status'=>200,'message' => 'Document list','previous_folder_id'=>$previous_folder,'data'=>$document ));
        		}else{  
		        	$this->LogOutput(Response::json(array('status'=>500,'message' => 'Error No Document found!','data' => $document)));
		            return Response::json(array('status'=>500,'message' => 'Error No Document found!','data' => $document));
	            }
        }
    } 

    public function DeleteFile(Request $request){
        $this->LogInput($request); 
        $errors_array = array();
        $headers = apache_request_headers(); 
        if($request->file_id){         
                if($request->filetype == "folder"){                 
                    $is_folder = InstructorFoler::where('user_id',$headers['id'])->where('id',$request->file_id)->where('filetype',$request->filetype)->first();
                        if($is_folder){ 
                            $folder = InstructorFoler::where('user_id',$headers['id'])->where('id',$request->file_id)->where('filetype',$request->filetype)->delete(); 
                            $file = InstructorDocuments::where('user_id',$headers['id'])->where('parent_folder_id',$request->parent_folder_id)->delete(); 
                           $document = InstructorDocuments::where('user_id',$headers['id'])->where('parent_folder_id',$request->parent_folder_id)->get();
                           $folder = InstructorFoler::where('user_id',$headers['id'])->where('parent_folder_id',$request->parent_folder_id)->get();
                           $merged = $document->merge($folder);
                           $document = $merged->all(); 
                           $this->LogOutput(Response::json(array('status'=>200,'message' => 'Folder Deleted sucessfully','data' => $document)));
                           return Response::json(array('status'=>200,'message' => 'Folder Deleted sucessfully','data' => $document));
                        }else{
                            $this->LogOutput(Response::json(array('status'=>500,'message' => 'Error No Folder found!')));
                            return Response::json(array('status'=>500,'message' => 'Error No Folder found!'));
                        }
                }else{
                    $is_file = InstructorDocuments::where('user_id',$headers['id'])->where('id',$request->file_id)->where('filetype',$request->filetype)->first();
                        if($is_file){ 
                            $file = InstructorDocuments::where('user_id',$headers['id'])->where('id',$request->file_id)->where('filetype',$request->filetype)->delete(); 
                            $document = InstructorDocuments::where('user_id',$headers['id'])->where('parent_folder_id',$request->parent_folder_id)->get();
                            $folder = InstructorFoler::where('user_id',$headers['id'])->where('parent_folder_id',$request->parent_folder_id)->get();
                            $merged = $document->merge($folder);
                            $document = $merged->all();
                            $this->LogOutput(Response::json(array('status'=>200,'message' => 'File Deleted sucessfully','data' => $document)));
                            return Response::json(array('status'=>200,'message' => 'File Deleted sucessfully','data' => $document));
                        }else{
                            $this->LogOutput(Response::json(array('status'=>500,'message' => 'Error No file found!')));
                            return Response::json(array('status'=>500,'message' => 'Error No file found!'));
                        } 
                } 
            }else{  
                $this->LogOutput(Response::json(array('status'=>500,'message' => 'Error while Deleting file!')));
                return Response::json(array('status'=>500,'message' => 'Error while Deleting file!'));
            } 
    }

}
