<?php
namespace App\Http\Controllers\Api;
use Illuminate\Support\Facades\Input;
use Illuminate\Contracts\Hashing\Hasher as HasherContract;
use Illuminate\Support\Str;
use Illuminate\Database\Query\Builder; 
use Route;
use File; 
use Response;
use App\Folder; 
use App\User; 
use App\School;
use App\Program;
use App\Instructor;
use App\PrivacyPolicy;
use App\Help;
use App\TermsOfServices;
use App\AboutUs;
use App\ContactUs;
use App\Http\Controllers\Controller; 
use Validator;
use DB;
use App\XApi;
use Hash;
use Mail; 
use App\Notifications\MailVerification;
use App\Notifications\ResendOTP; 
use Illuminate\Http\Request; 
use App\PasswordReset;
use App\Notifications\PasswordResetRequest;
use App\Notifications\PasswordResetSuccess;

class getApiController extends Controller
{
     public function __construct() {
        //API log code.
        $headers = apache_request_headers();
        $file = 'apilog.txt';
        list($controller , $action) = explode('@', Route::getCurrentRoute()->getActionName()); 
        if(file_exists($file)) {
            $data        = '';
            $info        = $headers;
            $headInfo    = http_build_query($info,'', ',');
            list($controller , $action) = explode('@', Route::getCurrentRoute()->getActionName()); 
            $requestData = http_build_query(Input::all(), '', ',');
            $date        = date('d-m-Y H:i:s');
            $data        = 'Date : '.$date.' | Controller : '.$controller.' | Action : '.$action.' | Request  Parameters :'.$requestData.' | Header Information :'.$headInfo;
            if ( 0 != filesize( $file )) {
                $data = "\r\n".$data;
            }
            $handle      = fopen($file, 'a') or die('Cannot open file:  '.$file);
            fwrite($handle, $data);
            fclose($handle);
        } 
    }

    public function Append($log_file, $value)
    {   
        File::append($log_file, $value . "\r\n");
    }

    public function LogInput(Request $request)
    {
        $log_file = storage_path() . '/logs/api' . date('Ymd') . '.log';
        $headers = apache_request_headers(); 
        $this->Append($log_file,'----------------' . debug_backtrace()[1]['function'] . ' --------------');
        $this->Append($log_file,'Request Info : ');
        $this->Append($log_file, 'Date: ' . date('Y-m-d H:i:s') . 'IP:' . $request->ip());
        $this->Append($log_file, 'User-Agent: ' . $request->header('User-Agent'));
        $this->Append($log_file, 'URL: ' . $request->url());
        $this->Append($log_file,'Input Parameters: ' .  json_encode(Input::all()));
        $this->Append($log_file,'Headers Parameters: ' .  json_encode($headers));
        $this->Append($log_file,'-----------');
        return;
    }
    public function LogOutput($output)
    {
        $log_file = storage_path() . '/logs/api' . date('Ymd') . '.log';
        $this->Append($log_file, 'Output: ');
        $this->Append($log_file,$output);
        $this->Append($log_file,'--------------------END------------------------');
        $this->Append($log_file,'');
        return;
    }

    public function MetaData (Request $request)
    { 

        $this->LogInput($request);
        $errors_array = array();
        $headers = apache_request_headers();

        //X-Api check code.
        if (isset($headers['X_Api_Key']) && $headers['X_Api_Key'] != '') {
            if(!XApi::checkXAPI($headers['X_Api_Key'])){
                return Response::json(array('status'=>500,'message' => 'Invalid X-API'));
            } 
        } else {
            return Response::json(array('status'=>500,'message' => 'X-API key not found'));
        }   
        $data = School::with('Program')->get();  

        $this->LogOutput(Response::json(array('status'=>200,'message' => 'Metadata','data'=>$data)));
                return Response::json(array('status'=>200,'message' => 'Metadata','data'=>$data)); 

    }

    public function signup(Request $request)
    { 	
        $this->LogInput($request);
        $errors_array = array();
        $headers = apache_request_headers();
        $validator = Validator::make($request->all(), [ 
        ]); 
        if ( $validator->fails() ) { 
                $this->LogOutput(Response::json(array('status'=>500,'message' => $validator->errors())));
                return Response::json(array('status'=>500,'message' => $validator->errors())); 
        } else {
        if($request->role_id == 1){ 
                 $headers = apache_request_headers(); 
                    $user = Instructor::where('role_id',1)->where('email',$request->email)->get();
                    if ( $user->isNotEmpty() )
                    {
                        $this->LogOutput(Response::json(array('status'=>500,'message' => 'Email Id already exists')));
                        return Response::json(array('status'=>500,'message' => 'Email Id already exists'));
                    }

                    if(!isset($request->email))
                    {
                         return Response::json(array('status'=>500,'message' => 'Please Enter Email Id'));
                    } 
         
                    if (isset($headers['device_token']))
                    {
                        $device=$headers['device_token'];
                    }
                    else
                    {
                        $device=null;
                    }

                     if ($request->file('image'))
                    { 
                        $image=$request->file('image');
                        $name = time().'.'.$image->getClientOriginalExtension();
                        $destinationPath = public_path('/profile/images');
                        $image->move($destinationPath, $name);
                        $image_name =   "http://15.206.38.116/clinicalwallet/public/profile/images/".$name;
                    }
                    else
                    {
                        $image_name=null;
                    } 
                    $user = new Instructor(); 
                    $user->first_name = $request->first_name;
                    $user->last_name = $request->last_name;   
                    $user->email= $request->email ;
                    $user->phone= $request->phone ;
                    $user->school= $request->school ;
                    $user->program= $request->program ;
                    $user->student_id= $request->student_id ;
                    $user->zipcode= $request->zipcode ;
                    $user->password = bcrypt($request->password);
                    $user->image = $image_name;
                    $user->user_token = substr(substr( "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ" ,mt_rand( 0 ,51 ) ,1 ) .substr( md5( time() ), 1), 0, 25); 
                    $user->device_token = $device; 
                    // $user->image = "http://15.206.38.116/habit/public/profile/images/".$name;
                    $user->role_id = "1";
                    $user->save(); 
                    $user['token'] = "12304567321425"; 
                   
                    if($user)
                    { 
                        $folder = public_path('uploads/instructor/'.$user->id.'/'); 
                        if (!File::exists($folder)) {
                                File::makeDirectory($folder, 0777, true, true);
                                $folder = new Folder(); 
                                $folder->user_id = $user->id;
                                $folder->folder_name =  $user->id; 
                                $folder->parent_folder_id = 0; 
                                $folder->save(); 
                        }   
                        $email = $request->email; 
                        Mail::send(['html'=>'auth.passwords.email_verification_instructor'],['email'=>$email,'user'=>$user],function($message) use ($email,$user){
                                    $message->to($email)->subject('Email verification');
                                    $message->from('bhargavkachhot.rs@gmail.com','Clinical Wallet');
                                });  
                    }
                    $this->LogOutput(Response::json(array('status'=>200,'message' => 'Mail Verification sent suceessfully','data'=>$user)));   
                    return Response::json(array('status'=>200,'message' => 'Mail Verification sent suceessfully','data'=>$user));
              
            }else {

                 $headers = apache_request_headers(); 
                    $user = User::where('role_id',2)->where('email',$request->email)->get();
                    if ( $user->isNotEmpty() )
                    {
                        $this->LogOutput(Response::json(array('status'=>500,'message' => 'Email Id already exists')));
                        return Response::json(array('status'=>500,'message' => 'Email Id already exists'));
                    }

                    if(!isset($request->email))
                    {
                         return Response::json(array('status'=>500,'message' => 'Please Enter Email Id'));
                    } 
         
                    if (isset($headers['device_token']))
                    {
                        $device=$headers['device_token'];
                    }
                    else
                    {
                        $device=null;
                    }

                     if ($request->file('image'))
                    { 
                        $image=$request->file('image');
                        $name = time().'.'.$image->getClientOriginalExtension();
                        $destinationPath = public_path('/profile/images');
                        $image->move($destinationPath, $name);
                        $image_name =   "http://15.206.38.116/clinicalwallet/public/profile/images/".$name;
                    }
                    else
                    {
                        $image_name=null;
                    } 
                    $user = new User(); 
                    $user->first_name = $request->first_name;
                    $user->last_name = $request->last_name;   
                    $user->email= $request->email ;
                    $user->phone= $request->phone ;
                    $user->school= $request->school ;
                    $user->program= $request->program ;
                    $user->student_id= $request->student_id ;
                    $user->zipcode= $request->zipcode ;
                    $user->password = bcrypt($request->password);
                    $user->image = $image_name;
                    $user->user_token = substr(substr( "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ" ,mt_rand( 0 ,51 ) ,1 ) .substr( md5( time() ), 1), 0, 25); 
                    $user->device_token = $device; 
                    // $user->image = "http://15.206.38.116/habit/public/profile/images/".$name;
                    $user->role_id = "2";
                    $user->save(); 
                    $user['token'] = "12304567321425"; 
                   
                    if($user)
                    { 
                        $folder = public_path('uploads/documents/'.$user->id.'/'); 
                        if (!File::exists($folder)) {
                                File::makeDirectory($folder, 0777, true, true);
                                $folder = new Folder(); 
                                $folder->user_id = $user->id;
                                $folder->folder_name =  $user->id; 
                                $folder->parent_folder_id = 0; 
                                $folder->save(); 
                        }   
                        $email = $request->email; 
                        Mail::send(['html'=>'auth.passwords.email_verification'],['email'=>$email,'user'=>$user],function($message) use ($email,$user){
                                    $message->to($email)->subject('Email verification');
                                    $message->from('bhargavkachhot.rs@gmail.com','Clinical Wallet');
                                });  
                    }
                    $this->LogOutput(Response::json(array('status'=>200,'message' => 'Mail Verification sent suceessfully','data'=>$user)));   
                    return Response::json(array('status'=>200,'message' => 'Mail Verification sent suceessfully','data'=>$user));
            } 
        }  
    }

    public function login(Request $request){
        $this->LogInput($request);
        $headers = apache_request_headers();
        $errors_array = array(); 
        $requestData = (array) json_decode(file_get_contents('php://input'), TRUE); 
    
       $validator = Validator::make($request->all(), [ 
           ]); 
   
        if ( $validator->fails() ) {
            $this->LogOutput(Response::json(array('status'=>500,'message' => $validator->errors())));
            return Response::json(array('status'=>500,'message' => $validator->errors()));
        } else {  
                if($request->role_id == 1){
                    $check_user = Instructor::where('email',$request['email'])->where('role_id',1)->get(); 
                    if($check_user->isEmpty()){
                        $this->LogOutput(Response::json(array('status'=>500,'message' => 'Email not found. Please try again')));
                        return Response::json(array('status'=>500,'message' => 'Email not found. Please try again'));
                    }
                    else{  
                        $check_user_logins =  User::IsValidCredentials($request->email,$request->password);   
                    }
                }else{
                    $check_user = User::where('email',$request['email'])->where('role_id',2)->get(); 
                    if($check_user->isEmpty()){
                        $this->LogOutput(Response::json(array('status'=>500,'message' => 'Email not found. Please try again')));
                        return Response::json(array('status'=>500,'message' => 'Email not found. Please try again'));
                    }
                    else{  
                        $check_user_logins =  User::IsValidCredentials($request->email,$request->password);   
                    }
                } 
        }

        if ($request->role_id == 1) {
                    foreach ($check_user as $key => $value) { 
                    if($check_user_logins ){ 
                        $key = env('APP_KEY');
                        $token = hash_hmac('sha256', Str::random(40), $key);
                        $userData = Instructor::where('email',$request['email'])->first(); 
                        if($userData->is_approved == 0){ 
                                $email = $request->email;  
                                $user = Instructor::where('email',$email)->first();  
                            Mail::send(['html'=>'auth.passwords.email_verification_instructor'],['email'=>$email,'user'=>$user],function($message) use ($email,$user){
                                     $message->to($email)->subject('Email verification');
                                     $message->from('bhargavkachhot.rs@gmail.com','Clinical Wallet');
                             });  
                            $this->LogOutput(Response::json(array('status'=>300,'message' => 'Please verify your Email ID')));
                            return Response::json(array('status'=>300,'message' => 'Please verify your Email ID'));
                        }
                        if ($userData == null )
                        {
                            $userData = Instructor::where('username',$request['email'])->first();
                        }
                        Instructor::where('id', $userData->id)->update(['user_token'=>$token]); 
                            $from = new \DateTime($userData->birthdate);
                            $to   = new \DateTime('today'); 
                            $arr['id'] = $userData->id;  
                            $arr['email'] = $userData->email;    
                            $arr['user_token'] = $token; 

                            if($userData->device_token ==null)
                            {
                                $arr['device_token'] ="";
                            }
                            else{
                                $arr['device_token'] = $userData->device_token;
                            } 
                            $arr['token'] = "123456844654984544";  
                            $message = 'Logged In'; 
                            $user_id = Instructor::where('email',$request['email'])->pluck('id');  
                            $update_device_token = User::where('id',$user_id)->update(['device_token' => $headers['device_token']]);
                            $data = Instructor::where('id',$user_id)->first();
                            $this->LogOutput(Response::json(array('status'=>200,'message' => $message,'data' => $data)));
                            return Response::json(array('status'=>200,'message' => $message,'data' => $data)); 
                    } else {
                        $this->LogOutput(Response::json(array('status'=>500,'message' => 'Email and Password does not match')));
                        return Response::json(array('status'=>500,'message' => 'Email and Password does not match'));
                    } 
                }         
          }else{
                        foreach ($check_user as $key => $value) { 
                        if($check_user_logins ){ 
                            $key = env('APP_KEY');
                            $token = hash_hmac('sha256', Str::random(40), $key);
                            $userData = User::where('email',$request['email'])->first(); 
                            if($userData->is_approved == 0){ 
                                    $email = $request->email;  
                                    $user = User::where('email',$email)->first();
                                Mail::send(['html'=>'auth.passwords.email_verification'],['email'=>$email,'user'=>$user],function($message) use ($email,$user){
                                         $message->to($email)->subject('Email verification');
                                         $message->from('bhargavkachhot.rs@gmail.com','Clinical Wallet');
                                 });  
                                $this->LogOutput(Response::json(array('status'=>300,'message' => 'Please verify your Email ID')));
                                return Response::json(array('status'=>300,'message' => 'Please verify your Email ID'));
                            }
                            if ($userData == null )
                            {
                                $userData = User::where('username',$request['email'])->first();
                            }
                            User::where('id', $userData->id)->update(['user_token'=>$token]); 
                                $from = new \DateTime($userData->birthdate);
                                $to   = new \DateTime('today'); 
                                $arr['id'] = $userData->id;  
                                $arr['email'] = $userData->email;    
                                $arr['user_token'] = $token; 

                                if($userData->device_token ==null)
                                {
                                    $arr['device_token'] ="";
                                }
                                else{
                                    $arr['device_token'] = $userData->device_token;
                                } 
                                $arr['token'] = "123456844654984544";  
                                $message = 'Logged In'; 
                                $user_id = User::where('email',$request['email'])->pluck('id');  
                                $update_device_token = User::where('id',$user_id)->update(['device_token' => $headers['device_token']]);
                                $data = User::where('id',$user_id)->first();
                                $this->LogOutput(Response::json(array('status'=>200,'message' => $message,'data' => $data)));
                                return Response::json(array('status'=>200,'message' => $message,'data' => $data)); 
                        } else {
                            $this->LogOutput(Response::json(array('status'=>500,'message' => 'Email and Password does not match')));
                            return Response::json(array('status'=>500,'message' => 'Email and Password does not match'));
                        } 
                    } 
          }  
    } 

    public function ChangePassword(Request $request){
      $this->LogInput($request); 
        $errors_array = array();
        $headers = apache_request_headers();
        $validator = Validator::make($request->all(), []);

        if($validator->fails()){
            $this->LogOutput(Response::json(array('status'=>500,'message' => $validator->errors())));
            return Response::json(array('status'=>500,'message' => $validator->errors())); 
        }else{ 
            if($request->password && $request->previous_password){
                if($request->role_id == 1){
                     $check_user = Instructor::where('id',$headers['id'])->pluck('password')->first(); 
                        if(Hash::check($request->previous_password,$check_user)){
                            if($check_user){ 
                                $update_password = Instructor::where('id',$headers['id'])->where('role_id','1')->update(['password' =>  bcrypt($request->password)]);  
                                $this->LogOutput(Response::json(array('status'=>200,'message' => 'Password changed sucessfully.')));
                                return Response::json(array('status'=>200,'message' => 'Password changed sucessfully.'));
                            }else{
                                $this->LogOutput(Response::json(array('status'=>500,'message' => 'Error while changeing password.')));
                                return Response::json(array('status'=>500,'message' => 'Error while changeing password.'));
                            } 
                        }else{
                            $this->LogOutput(Response::json(array('status'=>500,'message' => 'Please enter previous password correctly.')));
                            return Response::json(array('status'=>500,'message' => 'Please enter previous password correctly.'));
                        }  
                }else{ 
                    
                     $check_user = User::where('id',$headers['id'])->pluck('password')->first(); 
                        if(Hash::check($request->previous_password,$check_user)){
                            if($check_user){ 
                                $update_password = User::where('id',$headers['id'])->where('role_id','2')->update(['password' =>  bcrypt($request->password)]);  
                                $this->LogOutput(Response::json(array('status'=>200,'message' => 'Password changed sucessfully.')));
                                return Response::json(array('status'=>200,'message' => 'Password changed sucessfully.'));
                            }else{
                                $this->LogOutput(Response::json(array('status'=>500,'message' => 'Error while changeing password.')));
                                return Response::json(array('status'=>500,'message' => 'Error while changeing password.'));
                            } 
                        }else{
                            $this->LogOutput(Response::json(array('status'=>500,'message' => 'Please enter previous password correctly.')));
                            return Response::json(array('status'=>500,'message' => 'Please enter previous password correctly.'));
                        }
                     
                }        
            }else{
                $this->LogOutput(Response::json(array('status'=>500,'message' => 'please enter password.')));
                return Response::json(array('status'=>500,'message' => 'please enter password.'));
            }           
        }
    }

    public function create(Request $request)
    {
        $this->LogInput($request); 
        $errors_array = array();
        $headers = apache_request_headers(); 

        $request->validate([
            'email' => 'required|string|email',
        ]);
        if ($request->role_id == 1) {
             $user = Instructor::where('email', $request->email)->first();
        }else{
             $user = User::where('email', $request->email)->first();
        }  
        if (!$user)
        return Response::json(array('status'=>500,'message' => 'We cant find a user with that e-mail address'));
        $passwordReset = PasswordReset::updateOrCreate(
            ['email' => $user->email],
            [
                'email' => $user->email,
                'user_id'    => $user->id,
            ]
        );
        if ($user && $passwordReset)
            $user->notify(
                new PasswordResetRequest($passwordReset->user_id)
            ); 
            return Response::json(array('status'=>200,'message' => 'We have e-mailed your password reset link!')); 
    }

    public function otpVerification(Request $request){ 
         $this->LogInput($request); 
        $errors_array = array();
        $headers = apache_request_headers(); 

        if ($request->role_id == 1) {
                $check_otp =  Instructor::where('email',$request['email'])
                                    ->where('otp',$request['otp'])
                                    ->first(); 
                if($check_otp){
                    $user = Instructor::where('email',$request['email'])->update(['is_approved' => 1]);
                    return Response::json(array('status'=>200,'message' => 'OTP verified'));
                }else{  
                    if($check_otp){
                        $user = Instructor::where('email',$request['email'])->update(['is_approved' => 1]);
                        return Response::json(array('status'=>200,'message' => 'OTP verified'));
                    }else{
                         return Response::json(array('status'=>500,'message' => 'Error in OTP verification'));
                    }
                }
         }else{
                $check_otp =  User::where('email',$request['email'])
                                ->where('otp',$request['otp'])
                                ->first(); 
                if($check_otp){
                    $user = User::where('email',$request['email'])->update(['is_approved' => 1]);
                    return Response::json(array('status'=>200,'message' => 'OTP verified'));
                }else{  
                    if($check_otp){
                        $user = User::where('email',$request['email'])->update(['is_approved' => 1]);
                        return Response::json(array('status'=>200,'message' => 'OTP verified'));
                    }else{
                         return Response::json(array('status'=>500,'message' => 'Error in OTP verification'));
                    }
                }
        } 
    }

    public function PrivacyPolicy(Request $request){ 
        $this->LogInput($request); 
        $errors_array = array();
        $headers = apache_request_headers(); 

        $privacypolicy = PrivacyPolicy::pluck('text')->first();  
                    if($privacypolicy){ 
                        return view('static_pages.privacy_policy',['privacypolicy'=>$privacypolicy]);
                    }else{
                         return Response::json(array('status'=>500,'message' => 'Error'));
                    } 
    }

    public function TermsOfServices(Request $request){ 
        $this->LogInput($request); 
        $errors_array = array();
        $headers = apache_request_headers(); 

        $terms_of_services = TermsOfServices::pluck('text')->first(); 
                    if($terms_of_services){ 
                        return view('static_pages.terms_of_services',['terms_of_services'=>$terms_of_services]);
                    }else{
                         return Response::json(array('status'=>500,'message' => 'Error'));
                    } 
    }

    public function Help(Request $request){ 
        $this->LogInput($request); 
        $errors_array = array();
        $headers = apache_request_headers(); 

        $help = Help::pluck('text')->first(); 
                    if($help){ 
                        return view('static_pages.help',['help'=>$help]);
                    }else{
                         return Response::json(array('status'=>500,'message' => 'Error'));
                    } 
    }

    public function AboutUs(Request $request){ 
        $this->LogInput($request); 
        $errors_array = array();
        $headers = apache_request_headers();

        $about_us = AboutUs::pluck('text')->first(); 
                    if($about_us){ 
                        return view('static_pages.about_us',['about_us'=>$about_us]);
                    }else{
                         return Response::json(array('status'=>500,'message' => 'Error'));
                    } 
    }

    public function ContactUs(Request $request){ 
        $this->LogInput($request); 
        $errors_array = array();
        $headers = apache_request_headers();
        $validator = Validator::make($request->all(), [ 
        ]); 
        
        if($validator->fails()){
            $this->LogOutput(Response::json(array('status'=>500,'message' => $validator->errors())));
            return Response::json(array('status'=>500,'message' => $validator->errors())); 
        }else{  
            if ($request->file('image')){ 
                    $image=$request->file('image');
                    $name = time().'.'.$image->getClientOriginalExtension();
                    $destinationPath = public_path('/contact_us');
                    $image->move($destinationPath, $name);
                    $image_name =   "http://15.206.38.116/clinicalwallet/public/contact_us/".$name;
                    $user = new ContactUs();
                    $user->user_id = $headers['id'];
                    $user->image = $image_name;
                    $user->comments = $request->comments;
                    $user->save(); 

                    $email = User::where('id',$headers['id'])->where('is_approved',1)->where('role_id',2)->pluck('email')->first();
                    $user = User::where('id',$headers['id'])->where('is_approved',1)->where('role_id',2)->first();
                    $contact_us = ContactUs::where('user_id',$headers['id'])->orderBy('created_at', 'desc')->first();
                    if($user){
                        Mail::send(['html'=>'static_pages.contact_us'],['email'=>$email,'user'=>$user,'contact_us'=>$contact_us],function($message) use ($email,$user,$contact_us){
                                             $message->to('bhargavkachhot.rs@gmail.com')->subject('Comments from User');
                                             $message->from('bhargavkachhot.rs@gmail.com','Clinical Wallet');
                                     });
                    }
            }else{
                    $user = new ContactUs(); 
                    $user->user_id = $headers['id'];
                    $user->comments = $request->comments;
                    $user->save();

                    $email = User::where('id',$headers['id'])->where('is_approved',1)->where('role_id',2)->pluck('email')->first();
                    $user = User::where('id',$headers['id'])->where('is_approved',1)->where('role_id',2)->first();
                    $contact_us = ContactUs::where('user_id',$headers['id'])->orderBy('created_at', 'desc')->first();
                    if($user){
                        Mail::send(['html'=>'static_pages.contact_us'],['email'=>$email,'user'=>$user,'contact_us'=>$contact_us],function($message) use ($email,$user,$contact_us){
                                             $message->to('bhargavkachhot.rs@gmail.com')->subject('Comments from User');
                                             $message->from('bhargavkachhot.rs@gmail.com','Clinical Wallet');
                                     });
                    }
            }          
                $contact_us = ContactUs::where('user_id',$headers['id'])->get();  
                if($contact_us){  
                    return Response::json(array('status'=>200,'message' => 'Your comment submited sucessfully.'));
                }else{
                     return Response::json(array('status'=>500,'message' => 'Error'));
                }
        }     
    }

}
