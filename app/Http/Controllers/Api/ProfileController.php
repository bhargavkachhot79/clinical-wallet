<?php

namespace App\Http\Controllers\Api;
use Illuminate\Support\Facades\Input;
use Illuminate\Contracts\Hashing\Hasher as HasherContract;
use Illuminate\Support\Str;
use Illuminate\Database\Query\Builder; 
use Route;
use File; 
use Response;
use App\Document;
use App\User;
use App\Instructor;
use App\Http\Controllers\Controller; 
use Validator;
use DB;
use App\XApi;
use Hash;
use Mail;
use App\Folder; 
use App\Notifications\MailVerification;
use App\Notifications\ResendOTP; 
use Illuminate\Http\Request; 
use App\PasswordReset;
use App\Notifications\PasswordResetRequest;
use App\Notifications\PasswordResetSuccess;
use Illuminate\Support\Facades\Storage;


class ProfileController extends Controller
{
     public function __construct() {
        //API log code.
        $headers = apache_request_headers();
        $file = 'apilog.txt';
        list($controller , $action) = explode('@', Route::getCurrentRoute()->getActionName()); 
        if(file_exists($file)) {
            $data        = '';
            $info        = $headers;
            $headInfo    = http_build_query($info,'', ',');
            list($controller , $action) = explode('@', Route::getCurrentRoute()->getActionName()); 
            $requestData = http_build_query(Input::all(), '', ',');
            $date        = date('d-m-Y H:i:s');
            $data        = 'Date : '.$date.' | Controller : '.$controller.' | Action : '.$action.' | Request  Parameters :'.$requestData.' | Header Information :'.$headInfo;
            if ( 0 != filesize( $file )) {
                $data = "\r\n".$data;
            }
            $handle      = fopen($file, 'a') or die('Cannot open file:  '.$file);
            fwrite($handle, $data);
            fclose($handle);
        } 
    }

    public function Append($log_file, $value)
    {   
        File::append($log_file, $value . "\r\n");
    }

    public function LogInput(Request $request)
    {
        $log_file = storage_path() . '/logs/api' . date('Ymd') . '.log';
        $headers = apache_request_headers(); 
        $this->Append($log_file,'----------------' . debug_backtrace()[1]['function'] . ' --------------');
        $this->Append($log_file,'Request Info : ');
        $this->Append($log_file, 'Date: ' . date('Y-m-d H:i:s') . 'IP:' . $request->ip());
        $this->Append($log_file, 'User-Agent: ' . $request->header('User-Agent'));
        $this->Append($log_file, 'URL: ' . $request->url());
        $this->Append($log_file,'Input Parameters: ' .  json_encode(Input::all()));
        $this->Append($log_file,'Headers Parameters: ' .  json_encode($headers));
        $this->Append($log_file,'-----------');
        return;
    }
    public function LogOutput($output)
    {
        $log_file = storage_path() . '/logs/api' . date('Ymd') . '.log';
        $this->Append($log_file, 'Output: ');
        $this->Append($log_file,$output);
        $this->Append($log_file,'--------------------END------------------------');
        $this->Append($log_file,'');
        return;
    }

    public function UserProfile(Request $request){
        $this->LogInput($request); 
        $errors_array = array();
        $headers = apache_request_headers();
        $validator = Validator::make($request->all(), []); 
        if ($validator->fails()){
            $this->LogOutput(Response::json(array('status'=>500,'message' => $validator->errors())));
            return Response::json(array('status'=>500,'message' => $validator->errors()));
        }else{ 
            if ($request->role_id == 1) {
                  $user = Instructor::where('id',$headers['id'])->where('is_approved',1)->get();
              }else{
                  $user = User::where('id',$headers['id'])->where('is_approved',1)->get();
              }  
                if(count($user)){  
                    $this->LogOutput(Response::json(array('status'=>200,'message' => 'User Profile','data' => $user)));
                    return Response::json(array('status'=>200,'message' => 'User Profile','data'=>$user));
                }else{  
                    $this->LogOutput(Response::json(array('status'=>500,'message' => 'Error No result found!')));
                    return Response::json(array('status'=>500,'message' => 'Error No result found!'));
                }
        }
    }

    public function UpdateProfile(Request $request){
        $this->LogInput($request); 
        $errors_array = array();
        $headers = apache_request_headers(); 

        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'phone' => 'required',
        ]);
            
            if ($request->role_id == 1) {
                $user = Instructor::where('id',$headers['id'])->where('is_approved',1)->get();
                    if(count($user)){
                    if ($request->file('image')){ 
                            $image=$request->file('image');
                            $name = time().'.'.$image->getClientOriginalExtension();
                            $destinationPath = public_path('/profile/images');
                            $image->move($destinationPath, $name);
                            $image_name =   "http://15.206.38.116/clinicalwallet/public/profile/images/".$name;
                            $user = Instructor::where('id',$headers['id'])->update([                   
                                 'image' => $image_name, 
                            ]);
                    }
                        $user = Instructor::where('id',$headers['id'])->update([ 
                            'first_name'=> $request->first_name,
                            'last_name' => $request->last_name,
                            'phone' => $request->phone, 
                        ]);
                    $user = Instructor::where('id',$headers['id'])->first();    
                    $this->LogOutput(Response::json(array('status'=>200,'message' => 'Profile updated sucessfully','data' => $user)));
                    return Response::json(array('status'=>200,'message' => 'Profile updated sucessfully','data'=>$user));
                }else{  
                    $this->LogOutput(Response::json(array('status'=>500,'message' => 'Error No user found!')));
                    return Response::json(array('status'=>500,'message' => 'Error No user found!'));
                } 
            }else{
                $user = User::where('id',$headers['id'])->where('is_approved',1)->get();
                    if(count($user)){
                    if ($request->file('image')){ 
                            $image=$request->file('image');
                            $name = time().'.'.$image->getClientOriginalExtension();
                            $destinationPath = public_path('/profile/images');
                            $image->move($destinationPath, $name);
                            $image_name =   "http://15.206.38.116/clinicalwallet/public/profile/images/".$name;
                            $user = User::where('id',$headers['id'])->update([                   
                                 'image' => $image_name, 
                            ]);
                    }
                        $user = User::where('id',$headers['id'])->update([ 
                            'first_name'=> $request->first_name,
                            'last_name' => $request->last_name,
                            'phone' => $request->phone, 
                        ]);
                    $user = User::where('id',$headers['id'])->first();    
                    $this->LogOutput(Response::json(array('status'=>200,'message' => 'Profile updated sucessfully','data' => $user)));
                    return Response::json(array('status'=>200,'message' => 'Profile updated sucessfully','data'=>$user));
                }else{  
                    $this->LogOutput(Response::json(array('status'=>500,'message' => 'Error No user found!')));
                    return Response::json(array('status'=>500,'message' => 'Error No user found!'));
                }
            }
                
        
    }

    public function UserDocumentProfile(Request $request){
        $this->LogInput($request); 
        $errors_array = array();
        $headers = apache_request_headers();
        $validator = Validator::make($request->all(), []);

        if ( $validator->fails()){
            $this->LogOutput(Response::json(array('status'=>500,'message' => $validator->errors())));
            return Response::json(array('status'=>500,'message' => $validator->errors()));
        }else{ 
            if ($request->role_id == 1) {
                $user_with_profile = Instructor::with('Document')->where('id',$headers['id'])->where('is_approved',1)->get();
             }else{
                $user_with_profile = User::with('Document')->where('id',$headers['id'])->where('is_approved',1)->get();
             } 
                if(count($user_with_profile)){  
                    $this->LogOutput(Response::json(array('status'=>200,'message' => 'User Profile with Document','data' => $user_with_profile)));
                    return Response::json(array('status'=>200,'message' => 'User Profile with Document','data'=>$user_with_profile));
                }else{  
                    $this->LogOutput(Response::json(array('status'=>500,'message' => 'Error No result found!')));
                    return Response::json(array('status'=>500,'message' => 'Error No result found!'));
                }
        }
    } 
}
