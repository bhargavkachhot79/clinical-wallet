<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GBCController extends Controller
{
	
	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function Location()
    {
        return view('student.GBC.location');
    }
}
