<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InstructorDocuments extends Model
{
    protected $table = 'instructor_documents';
}
